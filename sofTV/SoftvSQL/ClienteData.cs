using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Softv.Entities;
using Softv.Providers;
using SoftvConfiguration;

namespace Softv.DAO
{
  /// <summary>
  /// Class                   : Softv.DAO.ClienteData
  /// Generated by            : Desarroll@, Class Generator (c) 2009
  /// Description             : Cliente Data Access Object
  /// File                    : ClienteDAO.cs
  /// Creation date           : 07/03/2011
  /// Creation time           : 09:48:46 a.m.
  /// </summary>
  public class ClienteData : ClienteProvider
  {
      /// <summary>
      /// </summary>
      /// <param name="Cliente">Object Cliente added to list</param>
      public override int AddCliente(ClienteEntity entity_Cliente)
      {
          int result=0;
          using(SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Cliente.ConnectionString))
          {
              SqlCommand comandoSql = new SqlCommand("Softv_AddCliente", connection);
              comandoSql.CommandType = CommandType.StoredProcedure;

              comandoSql.Parameters.Add("@CONTRATO", SqlDbType.BigInt);
              comandoSql.Parameters["@CONTRATO"].Direction = ParameterDirection.Output;

               comandoSql.Parameters.AddWithValue("@NOMBRE", String.IsNullOrEmpty(entity_Cliente.NOMBRE) ? DBNull.Value : (object)entity_Cliente.NOMBRE );
               comandoSql.Parameters.AddWithValue("@Clv_Calle", (object)entity_Cliente.Clv_Calle ?? (object)DBNull.Value);
               comandoSql.Parameters.AddWithValue("@NUMERO", String.IsNullOrEmpty(entity_Cliente.NUMERO) ? DBNull.Value : (object)entity_Cliente.NUMERO );
               comandoSql.Parameters.AddWithValue("@ENTRECALLES", String.IsNullOrEmpty(entity_Cliente.ENTRECALLES) ? DBNull.Value : (object)entity_Cliente.ENTRECALLES );
               comandoSql.Parameters.AddWithValue("@Clv_Colonia", (object)entity_Cliente.Clv_Colonia ?? (object)DBNull.Value);
               comandoSql.Parameters.AddWithValue("@CodigoPostal", String.IsNullOrEmpty(entity_Cliente.CodigoPostal) ? DBNull.Value : (object)entity_Cliente.CodigoPostal );
               comandoSql.Parameters.AddWithValue("@TELEFONO", String.IsNullOrEmpty(entity_Cliente.TELEFONO) ? DBNull.Value : (object)entity_Cliente.TELEFONO );
               comandoSql.Parameters.AddWithValue("@CELULAR", String.IsNullOrEmpty(entity_Cliente.CELULAR) ? DBNull.Value : (object)entity_Cliente.CELULAR );
               comandoSql.Parameters.AddWithValue("@DESGLOSA_Iva", (object)entity_Cliente.DESGLOSA_Iva ?? (object)DBNull.Value);
               comandoSql.Parameters.AddWithValue("@SoloInternet", (object)entity_Cliente.SoloInternet ?? (object)DBNull.Value);
               comandoSql.Parameters.AddWithValue("@eshotel", (object)entity_Cliente.eshotel ?? (object)DBNull.Value);
               comandoSql.Parameters.AddWithValue("@Clv_Ciudad", (object)entity_Cliente.Clv_Ciudad ?? (object)DBNull.Value);
               comandoSql.Parameters.AddWithValue("@Email", String.IsNullOrEmpty(entity_Cliente.Email) ? DBNull.Value : (object)entity_Cliente.Email );
               comandoSql.Parameters.AddWithValue("@clv_sector", (object)entity_Cliente.clv_sector ?? (object)DBNull.Value);
               comandoSql.Parameters.AddWithValue("@Clv_Periodo", (object)entity_Cliente.Clv_Periodo ?? (object)DBNull.Value);
               comandoSql.Parameters.AddWithValue("@PassworCambiado", (object)entity_Cliente.PassworCambiado ?? (object)DBNull.Value);
               comandoSql.Parameters.AddWithValue("@Cuenta", (object)entity_Cliente.Cuenta ?? (object)DBNull.Value);
               comandoSql.Parameters.AddWithValue("@clv_tap", String.IsNullOrEmpty(entity_Cliente.clv_tap) ? DBNull.Value : (object)entity_Cliente.clv_tap );
               comandoSql.Parameters.AddWithValue("@Salida", String.IsNullOrEmpty(entity_Cliente.Salida) ? DBNull.Value : (object)entity_Cliente.Salida );
               comandoSql.Parameters.AddWithValue("@IdParticular", (object)entity_Cliente.IdParticular ?? (object)DBNull.Value);

              try
              {
                  if (connection.State == ConnectionState.Closed)
                      connection.Open();
                  result = ExecuteNonQuery(comandoSql);
              }
              catch (Exception ex)
              {
                  throw new Exception("Error adding Cliente " + ex.Message, ex);
              }
              finally
              {
                  connection.Close();
              }
              if (result > 0)
                  result = (int)comandoSql.Parameters["@CONTRATO"].Value;
          }
          return result;

      }

      /// <summary>
      /// Deletes a Cliente
      /// </summary>
      /// <param name="System.String[]">Clienteid to delete</param>
      public override int DeleteCliente(long? CONTRATO)
      {
          int result=0;
          using(SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Cliente.ConnectionString))
          {
              SqlCommand comandoSql = new SqlCommand("Softv_DeleteCliente", connection);
              comandoSql.CommandType = CommandType.StoredProcedure;
              comandoSql.Parameters.AddWithValue("@CONTRATO", CONTRATO);

              try
              {
                  if (connection.State == ConnectionState.Closed)
                     connection.Open();
                  result = ExecuteNonQuery(comandoSql);
              }
              catch (Exception ex)
              {
                  throw new Exception("Error deleting Cliente " + ex.Message, ex);
              }
              finally
              {
                  if(connection != null)
                  connection.Close();
              }
          }
          return result;

      }

      /// <summary>
      /// Edits a Cliente
      /// </summary>
      /// <param name="Cliente">Objeto Cliente a editar</param>
      public override int EditCliente(ClienteEntity entity_Cliente)
      {
          int result=0;
          using(SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Cliente.ConnectionString))
          {
              SqlCommand comandoSql = new SqlCommand("Softv_EditCliente", connection);
              comandoSql.CommandType = CommandType.StoredProcedure;

               comandoSql.Parameters.AddWithValue("@CONTRATO", (object)entity_Cliente.CONTRATO ?? (object)DBNull.Value);
               comandoSql.Parameters.AddWithValue("@NOMBRE", String.IsNullOrEmpty(entity_Cliente.NOMBRE) ? DBNull.Value : (object)entity_Cliente.NOMBRE );
               comandoSql.Parameters.AddWithValue("@Clv_Calle", (object)entity_Cliente.Clv_Calle ?? (object)DBNull.Value);
               comandoSql.Parameters.AddWithValue("@NUMERO", String.IsNullOrEmpty(entity_Cliente.NUMERO) ? DBNull.Value : (object)entity_Cliente.NUMERO );
               comandoSql.Parameters.AddWithValue("@ENTRECALLES", String.IsNullOrEmpty(entity_Cliente.ENTRECALLES) ? DBNull.Value : (object)entity_Cliente.ENTRECALLES );
               comandoSql.Parameters.AddWithValue("@Clv_Colonia", (object)entity_Cliente.Clv_Colonia ?? (object)DBNull.Value);
               comandoSql.Parameters.AddWithValue("@CodigoPostal", String.IsNullOrEmpty(entity_Cliente.CodigoPostal) ? DBNull.Value : (object)entity_Cliente.CodigoPostal );
               comandoSql.Parameters.AddWithValue("@TELEFONO", String.IsNullOrEmpty(entity_Cliente.TELEFONO) ? DBNull.Value : (object)entity_Cliente.TELEFONO );
               comandoSql.Parameters.AddWithValue("@CELULAR", String.IsNullOrEmpty(entity_Cliente.CELULAR) ? DBNull.Value : (object)entity_Cliente.CELULAR );
               comandoSql.Parameters.AddWithValue("@DESGLOSA_Iva", (object)entity_Cliente.DESGLOSA_Iva ?? (object)DBNull.Value);
               comandoSql.Parameters.AddWithValue("@SoloInternet", (object)entity_Cliente.SoloInternet ?? (object)DBNull.Value);
               comandoSql.Parameters.AddWithValue("@eshotel", (object)entity_Cliente.eshotel ?? (object)DBNull.Value);
               comandoSql.Parameters.AddWithValue("@Clv_Ciudad", (object)entity_Cliente.Clv_Ciudad ?? (object)DBNull.Value);
               comandoSql.Parameters.AddWithValue("@Email", String.IsNullOrEmpty(entity_Cliente.Email) ? DBNull.Value : (object)entity_Cliente.Email );
               comandoSql.Parameters.AddWithValue("@clv_sector", (object)entity_Cliente.clv_sector ?? (object)DBNull.Value);
               comandoSql.Parameters.AddWithValue("@Clv_Periodo", (object)entity_Cliente.Clv_Periodo ?? (object)DBNull.Value);
               comandoSql.Parameters.AddWithValue("@PassworCambiado", (object)entity_Cliente.PassworCambiado ?? (object)DBNull.Value);
               comandoSql.Parameters.AddWithValue("@Cuenta", (object)entity_Cliente.Cuenta ?? (object)DBNull.Value);
               comandoSql.Parameters.AddWithValue("@clv_tap", String.IsNullOrEmpty(entity_Cliente.clv_tap) ? DBNull.Value : (object)entity_Cliente.clv_tap );
               comandoSql.Parameters.AddWithValue("@Salida", String.IsNullOrEmpty(entity_Cliente.Salida) ? DBNull.Value : (object)entity_Cliente.Salida );
               comandoSql.Parameters.AddWithValue("@IdParticular", (object)entity_Cliente.IdParticular ?? (object)DBNull.Value);

              try
              {
                  if (connection.State == ConnectionState.Closed)
                      connection.Open();
                  result = ExecuteNonQuery(comandoSql);
              }
              catch (Exception ex)
              {
                  throw new Exception("Error updating Cliente " + ex.Message, ex);
              }
              finally
              {
                  if(connection != null)
                      connection.Close();
              }
          }
          return result;
      }

      /// <summary>
      /// Gets all Cliente
      /// </summary>
      public override List<ClienteEntity> GetCliente()
      {
          List<ClienteEntity> ClienteList = new List<ClienteEntity>();
          using(SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Cliente.ConnectionString))
          {
              SqlCommand comandoSql = new SqlCommand("Softv_GetCliente", connection);
              comandoSql.CommandType = CommandType.StoredProcedure;

              IDataReader rd = null;
              try
              {
                  if (connection.State == ConnectionState.Closed)
                  connection.Open();
                  rd = ExecuteReader(comandoSql);

                  while (rd.Read())
                  {
                      ClienteList.Add(GetClienteFromReader(rd));
                  }
              }
              catch (Exception ex)
              {
                  throw new Exception("Error getting data Cliente "  + ex.Message, ex);
              }
              finally
              {
                  if(connection!=null)
                      connection.Close();
                  if(rd != null)
                      rd.Close();
              }
          }
          return ClienteList;
      }

      /// <summary>
      /// Gets Cliente by System.String[]
      /// </summary>
      /// <param name="System.String[]">Id del Cliente a consultar</param>
      public override ClienteEntity GetClienteById(long? CONTRATO)
      {
          using(SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Cliente.ConnectionString))
          {
              SqlCommand comandoSql = new SqlCommand("Softv_GetClienteById", connection);
              ClienteEntity entity_Cliente = null;
              comandoSql.CommandType = CommandType.StoredProcedure;

              comandoSql.Parameters.AddWithValue("@CONTRATO", CONTRATO);

              IDataReader rd = null;
              try
              {
                  if (connection.State == ConnectionState.Closed)
                      connection.Open();
                  rd = ExecuteReader(comandoSql, CommandBehavior.SingleRow);
                  if (rd.Read())
                      entity_Cliente = GetClienteFromReader(rd);
              }
              catch (Exception ex)
              {
                  throw new Exception("Error getting data Cliente "  + ex.Message, ex);
              }
              finally
              {
                  if(connection!=null)
                      connection.Close();
                  if(rd != null)
                      rd.Close();
              }
              return entity_Cliente;
          }
      }
  }
}
