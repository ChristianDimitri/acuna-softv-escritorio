using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Softv.Entities;
using Softv.Providers;
using SoftvConfiguration;

namespace Softv.DAO
{
  /// <summary>
  /// Class                   : Softv.DAO.CalleData
  /// Generated by            : Desarroll@, Class Generator (c) 2009
  /// Description             : Calle Data Access Object
  /// File                    : CalleDAO.cs
  /// Creation date           : 07/03/2011
  /// Creation time           : 11:16:22 a.m.
  /// </summary>
  public class CalleData : CalleProvider
  {
      /// <summary>
      /// </summary>
      /// <param name="Calle">Object Calle added to list</param>
      public override int AddCalle(CalleEntity entity_Calle)
      {
          int result=0;
          using(SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Calle.ConnectionString))
          {
              SqlCommand comandoSql = new SqlCommand("Softv_AddCalle", connection);
              comandoSql.CommandType = CommandType.StoredProcedure;

              comandoSql.Parameters.Add("@Clv_Calle", SqlDbType.Int);
              comandoSql.Parameters["@Clv_Calle"].Direction = ParameterDirection.Output;

               comandoSql.Parameters.AddWithValue("@NOMBRE", String.IsNullOrEmpty(entity_Calle.NOMBRE) ? DBNull.Value : (object)entity_Calle.NOMBRE );

              try
              {
                  if (connection.State == ConnectionState.Closed)
                      connection.Open();
                  result = ExecuteNonQuery(comandoSql);
              }
              catch (Exception ex)
              {
                  throw new Exception("Error adding Calle " + ex.Message, ex);
              }
              finally
              {
                  connection.Close();
              }
              if (result > 0)
                  result = (int)comandoSql.Parameters["@Clv_Calle"].Value;
          }
          return result;

      }

      /// <summary>
      /// Deletes a Calle
      /// </summary>
      /// <param name="System.String[]">Calleid to delete</param>
      public override int DeleteCalle(int? Clv_Calle)
      {
          int result=0;
          using(SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Calle.ConnectionString))
          {
              SqlCommand comandoSql = new SqlCommand("Softv_DeleteCalle", connection);
              comandoSql.CommandType = CommandType.StoredProcedure;
              comandoSql.Parameters.AddWithValue("@Clv_Calle", Clv_Calle);

              try
              {
                  if (connection.State == ConnectionState.Closed)
                     connection.Open();
                  result = ExecuteNonQuery(comandoSql);
              }
              catch (Exception ex)
              {
                  throw new Exception("Error deleting Calle " + ex.Message, ex);
              }
              finally
              {
                  if(connection != null)
                  connection.Close();
              }
          }
          return result;

      }

      /// <summary>
      /// Edits a Calle
      /// </summary>
      /// <param name="Calle">Objeto Calle a editar</param>
      public override int EditCalle(CalleEntity entity_Calle)
      {
          int result=0;
          using(SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Calle.ConnectionString))
          {
              SqlCommand comandoSql = new SqlCommand("Softv_EditCalle", connection);
              comandoSql.CommandType = CommandType.StoredProcedure;

               comandoSql.Parameters.AddWithValue("@Clv_Calle", (object)entity_Calle.Clv_Calle ?? (object)DBNull.Value);
               comandoSql.Parameters.AddWithValue("@NOMBRE", String.IsNullOrEmpty(entity_Calle.NOMBRE) ? DBNull.Value : (object)entity_Calle.NOMBRE );

              try
              {
                  if (connection.State == ConnectionState.Closed)
                      connection.Open();
                  result = ExecuteNonQuery(comandoSql);
              }
              catch (Exception ex)
              {
                  throw new Exception("Error updating Calle " + ex.Message, ex);
              }
              finally
              {
                  if(connection != null)
                      connection.Close();
              }
          }
          return result;
      }

      /// <summary>
      /// Gets all Calle
      /// </summary>
      public override List<CalleEntity> GetCalle()
      {
          List<CalleEntity> CalleList = new List<CalleEntity>();
          using(SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Calle.ConnectionString))
          {
              SqlCommand comandoSql = new SqlCommand("Softv_GetCalle", connection);
              comandoSql.CommandType = CommandType.StoredProcedure;

              IDataReader rd = null;
              try
              {
                  if (connection.State == ConnectionState.Closed)
                  connection.Open();
                  rd = ExecuteReader(comandoSql);

                  while (rd.Read())
                  {
                      CalleList.Add(GetCalleFromReader(rd));
                  }
              }
              catch (Exception ex)
              {
                  throw new Exception("Error getting data Calle "  + ex.Message, ex);
              }
              finally
              {
                  if(connection!=null)
                      connection.Close();
                  if(rd != null)
                      rd.Close();
              }
          }
          return CalleList;
      }

      /// <summary>
      /// Gets Calle by System.String[]
      /// </summary>
      /// <param name="System.String[]">Id del Calle a consultar</param>
      public override CalleEntity GetCalleById(int? Clv_Calle)
      {
          using(SqlConnection connection = new SqlConnection(SoftvSettings.Settings.Calle.ConnectionString))
          {
              SqlCommand comandoSql = new SqlCommand("Softv_GetCalleById", connection);
              CalleEntity entity_Calle = null;
              comandoSql.CommandType = CommandType.StoredProcedure;

              comandoSql.Parameters.AddWithValue("@Clv_Calle", Clv_Calle);

              IDataReader rd = null;
              try
              {
                  if (connection.State == ConnectionState.Closed)
                      connection.Open();
                  rd = ExecuteReader(comandoSql, CommandBehavior.SingleRow);
                  if (rd.Read())
                      entity_Calle = GetCalleFromReader(rd);
              }
              catch (Exception ex)
              {
                  throw new Exception("Error getting data Calle "  + ex.Message, ex);
              }
              finally
              {
                  if(connection!=null)
                      connection.Close();
                  if(rd != null)
                      rd.Close();
              }
              return entity_Calle;
          }
      }
  }
}
