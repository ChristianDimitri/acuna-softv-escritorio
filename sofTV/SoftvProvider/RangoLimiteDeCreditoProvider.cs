using System;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Runtime.Remoting;
using Softv.Entities;
using SoftvConfiguration;

namespace Softv.Providers
{
  /// <summary>
  /// Class                   : Softv.Providers.RangoLimiteDeCreditoProvider
  /// Generated by            : Desarroll@, Class Generator (c) 2009
  /// Description             : RangoLimiteDeCredito Provider
  /// File                    : RangoLimiteDeCreditoProvider.cs
  /// Creation date           : 09/08/2010
  /// Creation time           : 12:58:40 p.m.
  /// </summary>
  public abstract class RangoLimiteDeCreditoProvider : Globals.DataAccess
  {
      /// <summary>
      /// La instancia del RangoLimiteDeCredito de servicios de bd
      /// </summary>
      private static RangoLimiteDeCreditoProvider _Instance = null;

      private static ObjectHandle obj;
      /// <summary>
      /// Generates a RangoLimiteDeCredito instance
      /// </summary>
      public static RangoLimiteDeCreditoProvider Instance
      {
          get
          {
              if (_Instance == null)
              {
                obj = Activator.CreateInstance(
                   SoftvSettings.Settings.RangoLimiteDeCredito.Assembly,
                   SoftvSettings.Settings.RangoLimiteDeCredito.DataClass);
              _Instance = (RangoLimiteDeCreditoProvider)obj.Unwrap();
              }
              return _Instance;
          }
      }

      /// <summary>
      /// Provider's default constructor
      /// </summary>
      public RangoLimiteDeCreditoProvider()
      {
      }
      /// <summary>
      /// Abstract method to add RangoLimiteDeCredito
      /// </summary>
      /// <param name="RangoLimiteDeCredito"></param>
      /// <returns></returns>
      public abstract int AddRangoLimiteDeCredito(RangoLimiteDeCreditoEntity entity_RangoLimiteDeCredito);

      /// <summary>
      /// Abstract method to delete RangoLimiteDeCredito
      /// </summary>
      public abstract int DeleteRangoLimiteDeCredito(int? IdRango);

      /// <summary>
      /// Abstract method to update RangoLimiteDeCredito
      /// </summary>
      public abstract int EditRangoLimiteDeCredito(RangoLimiteDeCreditoEntity entity_RangoLimiteDeCredito);

      /// <summary>
      /// Abstract method to get all RangoLimiteDeCredito
      /// </summary>
      public abstract List<RangoLimiteDeCreditoEntity> GetRangoLimiteDeCredito();

      /// <summary>
      /// Abstract method to get by id
      /// </summary>
      public abstract RangoLimiteDeCreditoEntity GetRangoLimiteDeCreditoById(int? IdRango);

      /// <summary>
      /// Converts data from reader to entity
      /// </summary>
      protected virtual RangoLimiteDeCreditoEntity GetRangoLimiteDeCreditoFromReader(IDataReader reader)
      {
          RangoLimiteDeCreditoEntity entity_RangoLimiteDeCredito = null;
          try
          {
              entity_RangoLimiteDeCredito = new RangoLimiteDeCreditoEntity();
              entity_RangoLimiteDeCredito.IdRango = reader["IdRango"] ==  DBNull.Value ? null : ((int?)(reader["IdRango"]));
              entity_RangoLimiteDeCredito.RangoInicial = reader["RangoInicial"] ==  DBNull.Value ? null : ((int?)(reader["RangoInicial"]));
              entity_RangoLimiteDeCredito.RangoFin = reader["RangoFin"] ==  DBNull.Value ? null : ((int?)(reader["RangoFin"]));
          }
          catch (Exception ex)
          {
              throw new Exception("Error converting data to entity", ex);
          }
          return entity_RangoLimiteDeCredito;
      }
  }
}
