<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmMarcaciones
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim Clv_MarcacionLabel As System.Windows.Forms.Label
        Dim NumeracionLabel As System.Windows.Forms.Label
        Dim DescripcionLabel As System.Windows.Forms.Label
        Dim CostoLabel As System.Windows.Forms.Label
        Dim EventosLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmMarcaciones))
        Me.DataSetLidia2 = New sofTV.DataSetLidia2
        Me.Consulta_MarcacionesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Consulta_MarcacionesTableAdapter = New sofTV.DataSetLidia2TableAdapters.Consulta_MarcacionesTableAdapter
        Me.Consulta_MarcacionesBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton
        Me.Consulta_MarcacionesBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton
        Me.Clv_MarcacionTextBox = New System.Windows.Forms.TextBox
        Me.NumeracionTextBox = New System.Windows.Forms.TextBox
        Me.DescripcionTextBox = New System.Windows.Forms.TextBox
        Me.CostoTextBox = New System.Windows.Forms.TextBox
        Me.EventosTextBox = New System.Windows.Forms.TextBox
        Me.Button1 = New System.Windows.Forms.Button
        Clv_MarcacionLabel = New System.Windows.Forms.Label
        NumeracionLabel = New System.Windows.Forms.Label
        DescripcionLabel = New System.Windows.Forms.Label
        CostoLabel = New System.Windows.Forms.Label
        EventosLabel = New System.Windows.Forms.Label
        CType(Me.DataSetLidia2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Consulta_MarcacionesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Consulta_MarcacionesBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Consulta_MarcacionesBindingNavigator.SuspendLayout()
        Me.SuspendLayout()
        '
        'Clv_MarcacionLabel
        '
        Clv_MarcacionLabel.AutoSize = True
        Clv_MarcacionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_MarcacionLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Clv_MarcacionLabel.Location = New System.Drawing.Point(33, 54)
        Clv_MarcacionLabel.Name = "Clv_MarcacionLabel"
        Clv_MarcacionLabel.Size = New System.Drawing.Size(141, 15)
        Clv_MarcacionLabel.TabIndex = 1
        Clv_MarcacionLabel.Text = "Clave de Marcación :"
        '
        'NumeracionLabel
        '
        NumeracionLabel.AutoSize = True
        NumeracionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NumeracionLabel.ForeColor = System.Drawing.Color.LightSlateGray
        NumeracionLabel.Location = New System.Drawing.Point(81, 92)
        NumeracionLabel.Name = "NumeracionLabel"
        NumeracionLabel.Size = New System.Drawing.Size(93, 15)
        NumeracionLabel.TabIndex = 3
        NumeracionLabel.Text = "Numeracion :"
        '
        'DescripcionLabel
        '
        DescripcionLabel.AutoSize = True
        DescripcionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DescripcionLabel.ForeColor = System.Drawing.Color.LightSlateGray
        DescripcionLabel.Location = New System.Drawing.Point(81, 135)
        DescripcionLabel.Name = "DescripcionLabel"
        DescripcionLabel.Size = New System.Drawing.Size(91, 15)
        DescripcionLabel.TabIndex = 5
        DescripcionLabel.Text = "Descripcion :"
        '
        'CostoLabel
        '
        CostoLabel.AutoSize = True
        CostoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CostoLabel.ForeColor = System.Drawing.Color.LightSlateGray
        CostoLabel.Location = New System.Drawing.Point(121, 221)
        CostoLabel.Name = "CostoLabel"
        CostoLabel.Size = New System.Drawing.Size(51, 15)
        CostoLabel.TabIndex = 7
        CostoLabel.Text = "Costo :"
        '
        'EventosLabel
        '
        EventosLabel.AutoSize = True
        EventosLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        EventosLabel.ForeColor = System.Drawing.Color.LightSlateGray
        EventosLabel.Location = New System.Drawing.Point(107, 268)
        EventosLabel.Name = "EventosLabel"
        EventosLabel.Size = New System.Drawing.Size(65, 15)
        EventosLabel.TabIndex = 9
        EventosLabel.Text = "Eventos :"
        EventosLabel.Visible = False
        '
        'DataSetLidia2
        '
        Me.DataSetLidia2.DataSetName = "DataSetLidia2"
        Me.DataSetLidia2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Consulta_MarcacionesBindingSource
        '
        Me.Consulta_MarcacionesBindingSource.DataMember = "Consulta_Marcaciones"
        Me.Consulta_MarcacionesBindingSource.DataSource = Me.DataSetLidia2
        '
        'Consulta_MarcacionesTableAdapter
        '
        Me.Consulta_MarcacionesTableAdapter.ClearBeforeFill = True
        '
        'Consulta_MarcacionesBindingNavigator
        '
        Me.Consulta_MarcacionesBindingNavigator.AddNewItem = Nothing
        Me.Consulta_MarcacionesBindingNavigator.BindingSource = Me.Consulta_MarcacionesBindingSource
        Me.Consulta_MarcacionesBindingNavigator.CountItem = Nothing
        Me.Consulta_MarcacionesBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.Consulta_MarcacionesBindingNavigator.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Consulta_MarcacionesBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorDeleteItem, Me.Consulta_MarcacionesBindingNavigatorSaveItem})
        Me.Consulta_MarcacionesBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.Consulta_MarcacionesBindingNavigator.MoveFirstItem = Nothing
        Me.Consulta_MarcacionesBindingNavigator.MoveLastItem = Nothing
        Me.Consulta_MarcacionesBindingNavigator.MoveNextItem = Nothing
        Me.Consulta_MarcacionesBindingNavigator.MovePreviousItem = Nothing
        Me.Consulta_MarcacionesBindingNavigator.Name = "Consulta_MarcacionesBindingNavigator"
        Me.Consulta_MarcacionesBindingNavigator.PositionItem = Nothing
        Me.Consulta_MarcacionesBindingNavigator.Size = New System.Drawing.Size(710, 25)
        Me.Consulta_MarcacionesBindingNavigator.TabIndex = 0
        Me.Consulta_MarcacionesBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(77, 22)
        Me.BindingNavigatorDeleteItem.Text = "Eliminar"
        '
        'Consulta_MarcacionesBindingNavigatorSaveItem
        '
        Me.Consulta_MarcacionesBindingNavigatorSaveItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.Consulta_MarcacionesBindingNavigatorSaveItem.Image = CType(resources.GetObject("Consulta_MarcacionesBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.Consulta_MarcacionesBindingNavigatorSaveItem.Name = "Consulta_MarcacionesBindingNavigatorSaveItem"
        Me.Consulta_MarcacionesBindingNavigatorSaveItem.Size = New System.Drawing.Size(121, 22)
        Me.Consulta_MarcacionesBindingNavigatorSaveItem.Text = "Guardar datos"
        '
        'Clv_MarcacionTextBox
        '
        Me.Clv_MarcacionTextBox.BackColor = System.Drawing.Color.White
        Me.Clv_MarcacionTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_MarcacionesBindingSource, "Clv_Marcacion", True))
        Me.Clv_MarcacionTextBox.Location = New System.Drawing.Point(180, 53)
        Me.Clv_MarcacionTextBox.Name = "Clv_MarcacionTextBox"
        Me.Clv_MarcacionTextBox.ReadOnly = True
        Me.Clv_MarcacionTextBox.Size = New System.Drawing.Size(86, 20)
        Me.Clv_MarcacionTextBox.TabIndex = 2
        '
        'NumeracionTextBox
        '
        Me.NumeracionTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_MarcacionesBindingSource, "Numeracion", True))
        Me.NumeracionTextBox.Location = New System.Drawing.Point(180, 91)
        Me.NumeracionTextBox.Name = "NumeracionTextBox"
        Me.NumeracionTextBox.Size = New System.Drawing.Size(187, 20)
        Me.NumeracionTextBox.TabIndex = 4
        '
        'DescripcionTextBox
        '
        Me.DescripcionTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_MarcacionesBindingSource, "Descripcion", True))
        Me.DescripcionTextBox.Location = New System.Drawing.Point(180, 134)
        Me.DescripcionTextBox.Multiline = True
        Me.DescripcionTextBox.Name = "DescripcionTextBox"
        Me.DescripcionTextBox.Size = New System.Drawing.Size(398, 68)
        Me.DescripcionTextBox.TabIndex = 6
        '
        'CostoTextBox
        '
        Me.CostoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_MarcacionesBindingSource, "Costo", True))
        Me.CostoTextBox.Location = New System.Drawing.Point(180, 220)
        Me.CostoTextBox.Name = "CostoTextBox"
        Me.CostoTextBox.Size = New System.Drawing.Size(100, 20)
        Me.CostoTextBox.TabIndex = 8
        '
        'EventosTextBox
        '
        Me.EventosTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_MarcacionesBindingSource, "Eventos", True))
        Me.EventosTextBox.Location = New System.Drawing.Point(180, 267)
        Me.EventosTextBox.Name = "EventosTextBox"
        Me.EventosTextBox.Size = New System.Drawing.Size(100, 20)
        Me.EventosTextBox.TabIndex = 10
        Me.EventosTextBox.Visible = False
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(553, 302)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(145, 33)
        Me.Button1.TabIndex = 12
        Me.Button1.Text = "&Salir"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'FrmMarcaciones
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(710, 348)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Clv_MarcacionLabel)
        Me.Controls.Add(Me.Clv_MarcacionTextBox)
        Me.Controls.Add(NumeracionLabel)
        Me.Controls.Add(Me.NumeracionTextBox)
        Me.Controls.Add(DescripcionLabel)
        Me.Controls.Add(Me.DescripcionTextBox)
        Me.Controls.Add(CostoLabel)
        Me.Controls.Add(Me.CostoTextBox)
        Me.Controls.Add(EventosLabel)
        Me.Controls.Add(Me.EventosTextBox)
        Me.Controls.Add(Me.Consulta_MarcacionesBindingNavigator)
        Me.MaximizeBox = False
        Me.Name = "FrmMarcaciones"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Catálogo de Marcaciones Especiales"
        CType(Me.DataSetLidia2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Consulta_MarcacionesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Consulta_MarcacionesBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Consulta_MarcacionesBindingNavigator.ResumeLayout(False)
        Me.Consulta_MarcacionesBindingNavigator.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DataSetLidia2 As sofTV.DataSetLidia2
    Friend WithEvents Consulta_MarcacionesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Consulta_MarcacionesTableAdapter As sofTV.DataSetLidia2TableAdapters.Consulta_MarcacionesTableAdapter
    Friend WithEvents Consulta_MarcacionesBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents Consulta_MarcacionesBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents Clv_MarcacionTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NumeracionTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DescripcionTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CostoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents EventosTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
End Class
