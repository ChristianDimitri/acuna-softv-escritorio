﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmBloqueoClientes
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cbBloquear = New System.Windows.Forms.CheckBox()
        Me.label3 = New System.Windows.Forms.Label()
        Me.label2 = New System.Windows.Forms.Label()
        Me.label1 = New System.Windows.Forms.Label()
        Me.tbObservaciones = New System.Windows.Forms.TextBox()
        Me.tbFecha = New System.Windows.Forms.TextBox()
        Me.tbContrato = New System.Windows.Forms.TextBox()
        Me.tsbCancelar = New System.Windows.Forms.Button()
        Me.tsbGuardar = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'cbBloquear
        '
        Me.cbBloquear.AutoSize = True
        Me.cbBloquear.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbBloquear.Location = New System.Drawing.Point(143, 244)
        Me.cbBloquear.Name = "cbBloquear"
        Me.cbBloquear.Size = New System.Drawing.Size(94, 22)
        Me.cbBloquear.TabIndex = 14
        Me.cbBloquear.Text = "Bloquear"
        Me.cbBloquear.UseVisualStyleBackColor = True
        '
        'label3
        '
        Me.label3.AutoSize = True
        Me.label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label3.Location = New System.Drawing.Point(32, 116)
        Me.label3.Name = "label3"
        Me.label3.Size = New System.Drawing.Size(105, 15)
        Me.label3.TabIndex = 13
        Me.label3.Text = "Observaciones:"
        '
        'label2
        '
        Me.label2.AutoSize = True
        Me.label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label2.Location = New System.Drawing.Point(30, 90)
        Me.label2.Name = "label2"
        Me.label2.Size = New System.Drawing.Size(107, 15)
        Me.label2.TabIndex = 12
        Me.label2.Text = "Fecha Bloqueo:"
        '
        'label1
        '
        Me.label1.AutoSize = True
        Me.label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label1.Location = New System.Drawing.Point(72, 64)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(65, 15)
        Me.label1.TabIndex = 11
        Me.label1.Text = "Contrato:"
        '
        'tbObservaciones
        '
        Me.tbObservaciones.Location = New System.Drawing.Point(143, 111)
        Me.tbObservaciones.Multiline = True
        Me.tbObservaciones.Name = "tbObservaciones"
        Me.tbObservaciones.Size = New System.Drawing.Size(347, 127)
        Me.tbObservaciones.TabIndex = 10
        '
        'tbFecha
        '
        Me.tbFecha.BackColor = System.Drawing.SystemColors.Window
        Me.tbFecha.Location = New System.Drawing.Point(143, 85)
        Me.tbFecha.Name = "tbFecha"
        Me.tbFecha.ReadOnly = True
        Me.tbFecha.Size = New System.Drawing.Size(156, 20)
        Me.tbFecha.TabIndex = 9
        '
        'tbContrato
        '
        Me.tbContrato.BackColor = System.Drawing.SystemColors.Window
        Me.tbContrato.Location = New System.Drawing.Point(143, 59)
        Me.tbContrato.Name = "tbContrato"
        Me.tbContrato.ReadOnly = True
        Me.tbContrato.Size = New System.Drawing.Size(156, 20)
        Me.tbContrato.TabIndex = 8
        '
        'tsbCancelar
        '
        Me.tsbCancelar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.tsbCancelar.Location = New System.Drawing.Point(440, 1)
        Me.tsbCancelar.Name = "tsbCancelar"
        Me.tsbCancelar.Size = New System.Drawing.Size(87, 22)
        Me.tsbCancelar.TabIndex = 15
        Me.tsbCancelar.Text = "CANCELAR"
        Me.tsbCancelar.UseVisualStyleBackColor = True
        '
        'tsbGuardar
        '
        Me.tsbGuardar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.tsbGuardar.Location = New System.Drawing.Point(360, 1)
        Me.tsbGuardar.Name = "tsbGuardar"
        Me.tsbGuardar.Size = New System.Drawing.Size(84, 22)
        Me.tsbGuardar.TabIndex = 16
        Me.tsbGuardar.Text = "GUARDAR"
        Me.tsbGuardar.UseVisualStyleBackColor = True
        '
        'FrmBloqueoClientes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(527, 277)
        Me.Controls.Add(Me.tsbGuardar)
        Me.Controls.Add(Me.tsbCancelar)
        Me.Controls.Add(Me.cbBloquear)
        Me.Controls.Add(Me.label3)
        Me.Controls.Add(Me.label2)
        Me.Controls.Add(Me.label1)
        Me.Controls.Add(Me.tbObservaciones)
        Me.Controls.Add(Me.tbFecha)
        Me.Controls.Add(Me.tbContrato)
        Me.MaximizeBox = False
        Me.Name = "FrmBloqueoClientes"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Bloqueo de Clientes"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents cbBloquear As System.Windows.Forms.CheckBox
    Private WithEvents label3 As System.Windows.Forms.Label
    Private WithEvents label2 As System.Windows.Forms.Label
    Private WithEvents label1 As System.Windows.Forms.Label
    Private WithEvents tbObservaciones As System.Windows.Forms.TextBox
    Private WithEvents tbFecha As System.Windows.Forms.TextBox
    Private WithEvents tbContrato As System.Windows.Forms.TextBox
    Friend WithEvents tsbCancelar As System.Windows.Forms.Button
    Friend WithEvents tsbGuardar As System.Windows.Forms.Button
End Class
