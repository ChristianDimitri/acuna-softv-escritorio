﻿Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient
Imports System.Text

Public Class FrmOrdSer
    Private customersByCityReport As ReportDocument
    Private LocFecEje As Boolean = False
    Private LocTec As Boolean = False
    Private LocDet As Boolean = False
    Private Cadena As String
    Private Imprime As Integer = 0
    Dim eRes As Integer = 0
    Dim eMsg As String = Nothing
    Dim bloq As Integer = 0
    Dim dime As Integer
    Dim valor As Integer = Nothing

    Private Sub BUSCA(ByVal CLAVE As Integer)
        Try

            'If IsNumeric(gloClave) = True And IsNumeric(GloClv_TipSer) = True Then
            If IsNumeric(gloClave) = True Then
                Dim CON As New SqlConnection(MiConexion)
                CON.Open()
                Me.CONORDSERTableAdapter.Connection = CON
                Me.CONORDSERTableAdapter.Fill(Me.NewSofTvDataSet.CONORDSER, gloClave, 0)
                CON.Close()
                CREAARBOL()
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub
    Private Sub Borra_Cambio_Domicilio_Orden_si_no_guardo()
        Dim Con45 As New SqlConnection(MiConexion)
        Dim cmd As New SqlClient.SqlCommand()
        Try
            cmd = New SqlClient.SqlCommand()
            Con45.Open()
            With cmd
                .CommandText = "Borra_Cambio_Domicilio_Orden_si_no_guardo"
                .CommandTimeout = 0
                .Connection = Con45
                .CommandType = CommandType.StoredProcedure

                Dim prm As New SqlParameter("@clv_orden", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = gloClv_Orden
                .Parameters.Add(prm)

                Dim i As Integer = cmd.ExecuteNonQuery()
            End With
            Con45.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub Borra_Camdo_si_no_guardo_orden()
        Dim con50 As New SqlConnection(MiConexion)
        Dim cmd As New SqlClient.SqlCommand()
        Try
            cmd = New SqlClient.SqlCommand()
            con50.Open()
            With cmd
                .CommandText = "Borra_Camdo_si_no_guardo_orden"
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Connection = con50

                Dim prm As New SqlParameter("@clv_orden", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = gloClave
                .Parameters.Add(prm)

                Dim i As Integer = cmd.ExecuteNonQuery()

            End With
            con50.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub FrmOrdSer_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        Dim CON As New SqlConnection(MiConexion)
        'dim cmd as New SqlClient 
        If GLOCONTRATOSEL > 0 Then
            Me.ContratoTextBox.Text = GLOCONTRATOSEL
            GLOCONTRATOSEL = 0
            GloClv_TipSer = 0
        End If
        'If GloBndTipSer = True Then
        '    GloBndTipSer = False
        '    Me.ComboBox5.SelectedValue = GloClv_TipSer
        '    Me.ComboBox5.Text = GloNom_TipSer
        '    Me.ComboBox5.FindString(GloNom_TipSer)
        '    Me.ComboBox5.Text = GloNom_TipSer
        '    Me.TextBox2.Text = GloNom_TipSer
        'End If
        If GloBndTrabajo = True Then
            GloBndTrabajo = False
            GloClv_TipSer = 0
            If bndCAMDO = True Then
                bndCAMDO = False
                Borra_Cambio_Domicilio_Orden_si_no_guardo()
            End If
            CON.Open()
            Me.BUSCADetOrdSerTableAdapter.Connection = CON
            Me.BUSCADetOrdSerTableAdapter.Fill(Me.NewSofTvDataSet.BUSCADetOrdSer, New System.Nullable(Of Long)(CType(gloClv_Orden, Long)))
            CON.Close()
        End If
        If GloBloqueaDetalle = True Then
            GloBloqueaDetalle = False
            Me.BUSCADetOrdSerDataGridView.Enabled = True
        End If

        If eStatusOrdSer = "P" And eActTecnico = False Then
            Me.Tecnico.Enabled = False
        Else
            Me.Tecnico.Enabled = True
        End If
        'Guarda el Motivo por el cual ha sido dado de baja un servicio.
        'Eric--------------------------------------------------------------------
        If GloClv_MotCan > 0 Then

            CON.Open()
            Me.InsertMotCanServTableAdapter.Connection = CON
            Me.InsertMotCanServTableAdapter.Fill(Me.DataSetEric.InsertMotCanServ, Me.Clv_OrdenTextBox.Text, GloClv_MotCan)
            CON.Close()
            GloClv_MotCan = 0

            GuardaRelOrdenUsuario()
            CON.Open()
            Me.Validate()
            Me.CONORDSERBindingSource.EndEdit()
            Me.CONORDSERTableAdapter.Connection = CON
            Me.CONORDSERTableAdapter.Update(Me.NewSofTvDataSet.CONORDSER)
            Me.PREEJECUTAOrdSerTableAdapter.Connection = CON
            Me.PREEJECUTAOrdSerTableAdapter.Fill(Me.NewSofTvDataSet.PREEJECUTAOrdSer, New System.Nullable(Of Long)(CType(Me.Clv_OrdenTextBox.Text, Long)))
            CON.Close()
            MsgBox(mensaje5)

            'Eric----------------------------------------------
            Dim CONERIC As New SqlConnection(MiConexion)
            Dim eRes As Long = 0
            Dim eMsg As String = Nothing
            CONERIC.Open()
            Me.ChecaOrdSerRetiroTableAdapter.Connection = CONERIC
            Me.ChecaOrdSerRetiroTableAdapter.Fill(Me.DataSetEric.ChecaOrdSerRetiro, CType(Me.Clv_OrdenTextBox.Text, Long), eRes, eMsg)
            CONERIC.Close()
            If eRes > 0 And IdSistema = "SA" Or IdSistema = "VA" Then
                ImprimeOrdSerRetiro(eRes)
            End If
            '------------------------------------------------------

            GloBnd = True
            GloGuardo = False
            If opcion = "N" Then
                CON.Open()
                Me.Imprime_OrdenTableAdapter.Connection = CON
                Me.Imprime_OrdenTableAdapter.Fill(Me.ProcedimientosArnoldo2.Imprime_Orden, Me.Clv_OrdenTextBox.Text, Imprime)
                CON.Close()
                If Imprime = 0 Then
                    ConfigureCrystalReports(0, "")
                ElseIf Imprime = 1 Then
                    MsgBox("La orden es de proceso Automático No se Imprimio", MsgBoxStyle.Information)
                End If
            End If
            CON.Close()
            Me.Close()
        End If
        If bloq = 1 Then
            bloq = 0
            eGloContrato = Me.ContratoTextBox.Text
            FrmBloqueo.Show()
            Me.Panel1.Enabled = False
            Me.Panel3.Enabled = False
            Me.Panel6.Enabled = False
            Me.Panel7.Enabled = False
            Me.Panel8.Enabled = False
        End If
        '----------------------------------------------------------------------------------------
    End Sub

    Private Sub BUSCACLIENTES(ByVal OP As Integer)
        Try
            Dim CON As New SqlConnection(MiConexion)

            If IsNumeric(Me.ContratoTextBox.Text) = True Then
                CON.Open()
                Me.BUSCLIPORCONTRATOTableAdapter.Connection = CON
                Me.BUSCLIPORCONTRATOTableAdapter.Fill(Me.NewSofTvDataSet.BUSCLIPORCONTRATO, New System.Nullable(Of Long)(CType(Me.ContratoTextBox.Text, Long)), (CType("", String)), (CType("", String)), (CType("", String)), (CType("", String)), New System.Nullable(Of Integer)(CType(0, Integer)), 0)
                Me.BuscaBloqueadoTableAdapter.Connection = CON
                Me.BuscaBloqueadoTableAdapter.Fill(Me.DataSetLidia.BuscaBloqueado, Me.ContratoTextBox.Text, NUM, num2)
                CON.Close()
                CREAARBOL()
                If num2 = 1 Then
                    eGloContrato = Me.ContratoTextBox.Text
                    bloq = 1
                End If
            End If

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub CREAARBOL()

        Try
            Dim CON As New SqlConnection(MiConexion)

            Dim I As Integer = 0
            Dim X As Integer = 0
            Dim Y As Integer = 0
            Dim epasa As Boolean = True
            ' Assumes that customerConnection is a valid SqlConnection object.
            ' Assumes that orderConnection is a valid OleDbConnection object.
            'Dim custAdapter As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter( _
            '  "SELECT * FROM dbo.Customers", customerConnection)''

            'Dim customerOrders As DataSet = New DataSet()
            'custAdapter.Fill(customerOrders, "Customers")
            ' 
            'Dim pRow, cRow As DataRow
            'For Each pRow In customerOrders.Tables("Customers").Rows
            ' msgbox(pRow("CustomerID").ToString())
            'Next
            CON.Open()
            If IsNumeric(Me.ContratoTextBox.Text) = True Then
                Me.DameSerDELCliTableAdapter.Connection = CON
                Me.DameSerDELCliTableAdapter.Fill(Me.NewSofTvDataSet.dameSerDELCli, New System.Nullable(Of Long)(CType(Me.ContratoTextBox.Text, Long)))
            Else
                Me.DameSerDELCliTableAdapter.Connection = CON
                Me.DameSerDELCliTableAdapter.Fill(Me.NewSofTvDataSet.dameSerDELCli, New System.Nullable(Of Long)(CType(0, Long)))
            End If
            CON.Close()
            Dim pasa As Boolean = False
            Dim Net As Boolean = False
            Dim dig As Boolean = False
            Dim jNet As Integer = -1
            Dim PasaJNet As Boolean = False
            Dim jDig As Integer = -1
            Dim FilaRow As DataRow
            'Me.TextBox1.Text = ""
            Me.TreeView1.Nodes.Clear()
            For Each FilaRow In Me.NewSofTvDataSet.dameSerDELCli.Rows

                'MsgBox(Trim(FilaRow(1).ToString()) & " " & Trim(FilaRow(0).ToString()))
                X = 0
                'If Len(Trim(Me.TextBox1.Text)) = 0 Then
                'Me.TextBox1.Text = Trim(FilaRow("Servicio").ToString())
                'Else
                'Me.TextBox1.Text = Me.TextBox1.Text & " , " & Trim(FilaRow("Servicio").ToString())
                'End If
                'MsgBox(Mid(FilaRow("Servicio").ToString(), 1, 19))
                If Mid(FilaRow("Servicio").ToString(), 1, 3) = "---" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    Net = False
                    dig = False
                    jNet = -1
                    jDig = -1
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 15) = "Servicio Basico" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    jNet = -1
                    jDig = -1
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 31) = "Servicios de Televisión Digital" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    jNet = -1
                    jDig = -1
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 21) = "Servicios de Internet" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    jNet = -1
                    jDig = -1
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 22) = "Servicios de Teléfonia" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    jNet = -1
                    jDig = -1
                Else
                    If Mid(FilaRow("Servicio").ToString(), 1, 14) = "Mac Cablemodem" Then
                        Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        jNet = jNet + 1
                        pasa = False
                        Net = True
                    ElseIf Mid(FilaRow("Servicio").ToString(), 1, 15) = "Aparato Digital" Then
                        Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        jDig = jDig + 1
                        pasa = False
                        dig = True
                    Else
                        If Net = True Then
                            Me.TreeView1.Nodes(I - 1).Nodes(jNet).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Gray
                        ElseIf dig = True Then
                            Me.TreeView1.Nodes(I - 1).Nodes(jDig).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Gray
                        Else
                            If epasa = True Then
                                Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                                pasa = False
                                epasa = False
                            Else
                                Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Gray
                                epasa = False
                                pasa = False
                            End If

                        End If
                    End If
                End If
                If pasa = True Then I = I + 1
            Next

            'Me.TreeView1.Nodes(0).ExpandAll()
            For Y = 0 To (I - 1)
                Me.TreeView1.Nodes(Y).ExpandAll()
            Next
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try


    End Sub

    Private Sub CREAARBOL11()

        Try
            Dim I As Integer = 0
            Dim X As Integer = 0
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            ' Assumes that customerConnection is a valid SqlConnection object.
            ' Assumes that orderConnection is a valid OleDbConnection object.
            'Dim custAdapter As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter( _
            '  "SELECT * FROM dbo.Customers", customerConnection)''

            'Dim customerOrders As DataSet = New DataSet()
            'custAdapter.Fill(customerOrders, "Customers")
            ' 
            'Dim pRow, cRow As DataRow
            'For Each pRow In customerOrders.Tables("Customers").Rows
            ' Console.WriteLine(pRow("CustomerID").ToString())
            'Next

            If IsNumeric(Me.ContratoTextBox.Text) = True Then
                Me.DameSerDELCliTableAdapter.Connection = CON
                Me.DameSerDELCliTableAdapter.Fill(Me.NewSofTvDataSet.dameSerDELCli, New System.Nullable(Of Long)(CType(Me.ContratoTextBox.Text, Long)))
            Else
                Me.DameSerDELCliTableAdapter.Connection = CON
                Me.DameSerDELCliTableAdapter.Fill(Me.NewSofTvDataSet.dameSerDELCli, New System.Nullable(Of Long)(CType(0, Long)))
            End If


            Dim FilaRow As DataRow
            'Me.TextBox1.Text = ""
            Me.TreeView1.Nodes.Clear()
            For Each FilaRow In Me.NewSofTvDataSet.dameSerDELCli.Rows

                'MsgBox(Trim(FilaRow(1).ToString()) & " " & Trim(FilaRow(0).ToString()))
                X = 0
                'If Len(Trim(Me.TextBox1.Text)) = 0 Then
                'Me.TextBox1.Text = Trim(FilaRow("Servicio").ToString())
                'Else
                'Me.TextBox1.Text = Me.TextBox1.Text & " , " & Trim(FilaRow("Servicio").ToString())
                'End If
                Me.TreeView1.Nodes.Add(Trim(FilaRow("Servicio").ToString()))
                I += 1
            Next
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub ACTIVA(ByVal BND As Boolean)
        'Me.ComboBox4.Enabled = BND
        Me.BindingNavigatorDeleteItem.Enabled = BND
        RadioButton2.Enabled = BND
        RadioButton3.Enabled = BND
        Me.Tecnico.Enabled = BND
        Me.Fec_EjeTextBox.Enabled = BND
        Me.Visita1TextBox.Enabled = BND
        Me.Visita2TextBox.Enabled = BND
        'SolucionTextBox.Enabled = BND
        Panel2.Enabled = BND
        If BND = True Then
            Me.RadioButton1.Enabled = False
            Me.ContratoTextBox.Enabled = False
            Me.Button1.Enabled = False
            'Me.SplitContainer1.Enabled = False
            'Me.ComboBox3.Enabled = False
            'Me.ProblemaTextBox.Enabled = False
        End If
    End Sub

    Private Sub FrmOrdSer_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed

    End Sub

    Private Sub FrmOrdSer_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Dim LocContrato1 As Long = 0
        If IsNumeric(Me.ContratoTextBox.Text) = False Then LocContrato1 = 0 Else LocContrato1 = Me.ContratoTextBox.Text
        If IsNumeric(Me.Clv_OrdenTextBox.Text) = True Then
            Try
                If dameStatusOrdenQueja(CInt(Me.Clv_OrdenTextBox.Text), "O") = "P" And opcion = "M" Then
                    softv_BorraDescarga(CInt(Me.Clv_OrdenTextBox.Text), "O")
                End If
                Dim CON As New SqlConnection(MiConexion)
                Dim error2 As Integer = Nothing

                LocNo_Bitacora = 0
                CON.Open()
                Me.Valida_DetOrdenTableAdapter.Connection = CON
                Me.Valida_DetOrdenTableAdapter.Fill(Me.NewSofTvDataSet.Valida_DetOrden, New System.Nullable(Of Long)(CType(Me.Clv_OrdenTextBox.Text, Long)))
                CON.Close()
                If IsNumeric(Me.ValidacionTextBox.Text) = False Then Me.ValidacionTextBox.Text = 0
                Me.valida()
                'If Me.CONTADORTextBox.Text = 4 Then
                ' GloGuardo = True
                ' End If
                If GloGuardo = True And Me.ValidacionTextBox.Text > 0 Then
                    Dim RESP As MsgBoxResult = MsgBoxResult.Yes
                    If opcion = "C" Then
                        Exit Sub
                    End If
                    If opcion = "N" Then
                        RESP = MsgBox("Desea Guardar la Orden que Genero", MsgBoxStyle.YesNo)
                        If RESP = MsgBoxResult.No Then
                            If IsNumeric(Me.ContratoTextBox.Text) = False Then LocContrato1 = 0 Else LocContrato1 = Me.ContratoTextBox.Text
                            bitsist(GloUsuario, CLng(LocContrato1), LocGloSistema, "Ordenes Servicio", "", "Se Acceso a la Pantalla de Captura de Ordenes de Servicio y NO se Grabo la Orden " + Me.Clv_OrdenTextBox.Text, "Se Acceso a la Pantalla de Captura de Ordenes de Servicio y NO se Grabo la Orden " + Me.Clv_OrdenTextBox.Text, "SA")
                            GloGuardo = False
                            'GloClv_TipSer = Me.ComboBox5.SelectedValue
                            GloClv_TipSer = 0
                            'GloNom_TipSer = Me.ComboBox5.Text
                            GloNom_TipSer = ""
                            Borra_Camdo_si_no_guardo_orden()
                            CON.Open()
                            Me.CONORDSERTableAdapter.Connection = CON
                            Me.CONORDSERTableAdapter.Delete(gloClave, 0)
                            CON.Close()
                        Else
                            error2 = Checa_si_tiene_camdo(gloClave)
                            If IsNumeric(Me.ContratoTextBox.Text) = False Then LocContrato1 = 0 Else LocContrato1 = Me.ContratoTextBox.Text
                            bitsist(GloUsuario, CLng(LocContrato1), LocGloSistema, "Ordenes Servicio", "", "Se Acceso a la Pantalla de Captura de Ordenes de Servicio y SI se Grabo la Orden " + Me.Clv_OrdenTextBox.Text, "Se Acceso a la Pantalla de Captura de Ordenes de Servicio y SI se Grabo la Orden " + Me.Clv_OrdenTextBox.Text, "SA")
                            If error2 > 0 Then
                                GloBnd = True
                                MsgBox("Se Tiene Que Capturar el Nuevo Domicilio", MsgBoxStyle.Information)
                                CON.Open()
                                Me.CONORDSERTableAdapter.Connection = CON
                                Me.CONORDSERTableAdapter.Delete(gloClave, 0)
                                CON.Close()
                                Exit Sub

                            End If
                        End If
                    ElseIf Me.StatusTextBox.Text = "E" And Me.CONTADORTextBox.Text = 4 Then
                        RESP = MsgBox("La Orden ya tiene todos los datos para Ejecutarse ¿ Desea Salir sin Grabar ? ", MsgBoxStyle.YesNo)
                        If RESP = MsgBoxResult.No Then
                            If IsNumeric(Me.ContratoTextBox.Text) = False Then LocContrato1 = 0 Else LocContrato1 = Me.ContratoTextBox.Text
                            bitsist(GloUsuario, CLng(LocContrato1), LocGloSistema, "Ordenes Servicio", "", "Se Acceso a la Pantalla de Captura de Ordenes de Servicio y se salio sin Grabar " + Me.Clv_OrdenTextBox.Text, "Se Acceso a la Pantalla de Captura de Ordenes de Servicio y se salio sin Grabar " + Me.Clv_OrdenTextBox.Text, "SA")
                            e.Cancel = True
                            Exit Sub
                        End If
                    End If

                    GloBnd = True
                ElseIf GloGuardo = True And Me.ValidacionTextBox.Text = 0 Then
                    If IsNumeric(Me.ContratoTextBox.Text) = False Then LocContrato1 = 0 Else LocContrato1 = Me.ContratoTextBox.Text
                    bitsist(GloUsuario, CLng(LocContrato1), LocGloSistema, "Ordenes Servicio", "Se Acceso a la Pantalla de Captura de Ordenes de Servicio y no se Guardado la Orden " + Me.Clv_OrdenTextBox.Text, " ", "Se Acceso a la Pantalla de Captura de Ordenes de Servicio y no se Guardado la Orden " + Me.Clv_OrdenTextBox.Text, LocClv_Ciudad)
                    GloBnd = True
                    GloGuardo = False
                    'GloClv_TipSer = Me.ComboBox5.SelectedValue
                    'GloNom_TipSer = Me.ComboBox5.Text
                    GloClv_TipSer = 0
                    GloNom_TipSer = ""
                    CON.Open()
                    Me.CONORDSERTableAdapter.Connection = CON
                    Me.CONORDSERTableAdapter.Delete(gloClave, GloClv_TipSer)
                    CON.Close()
                End If

            Catch ex As System.Exception
                System.Windows.Forms.MessageBox.Show(ex.Message)
            End Try
        End If
    End Sub

    Private Sub FrmOrdSer_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Leave

    End Sub


    Private Sub FrmOrdSer_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            colorea(Me, Me.Name)
            Dim CON As New SqlConnection(MiConexion)
            Dim valor As Integer = 0
            Dim LocContrato1 As Long = 0
            If IsNumeric(ContratoTextBox.Text) = True Then LocContrato1 = ContratoTextBox.Text



            'No paresca "Es Hotel"
            If IdSistema = "LO" Or IdSistema = "YU" Then
                Me.Label13.Visible = False
                Me.ESHOTELCheckBox.Visible = False
            End If

            CON.Open()
            'GloClv_TipSer = 1000
            'TODO: esta línea de código carga datos en la tabla 'DataSetarnoldo.Dame_fecha_hora_serv' Puede moverla o quitarla según sea necesario.
            Me.Dame_fecha_hora_servTableAdapter.Connection = CON
            Me.Dame_fecha_hora_servTableAdapter.Fill(Me.DataSetarnoldo.Dame_fecha_hora_serv)
            'TODO: esta línea de código carga datos en la tabla 'DataSetarnoldo.Dame_fecha_hora_serv' Puede moverla o quitarla según sea necesario.
            Locclv_folio = 0
            LocNo_Bitacora = 0
            LocValida1 = False
            Bloquea = False

            'TODO: esta línea de código carga datos en la tabla 'NewSofTvDataSet.MuestraTipSerPrincipal' Puede moverla o quitarla según sea necesario.
            Me.DameClv_Session_TecnicosTableAdapter.Connection = CON
            Me.DameClv_Session_TecnicosTableAdapter.Fill(Me.DataSetarnoldo.DameClv_Session_Tecnicos, clv_sessionTecnico)
            Me.MuestraTipSerPrincipalTableAdapter.Connection = CON
            Me.MuestraTipSerPrincipalTableAdapter.Fill(Me.NewSofTvDataSet.MuestraTipSerPrincipal)
            'TODO: esta línea de código carga datos en la tabla 'NewSofTvDataSet.MUESTRATECNICOS' Puede moverla o quitarla según sea necesario.
            'Me.MUESTRATECNICOSTableAdapter.Connection = CON
            'Me.MUESTRATECNICOSTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRATECNICOS)
            'Me.Muestra_Tecnicos_AlmacenTableAdapter.Connection = CON
            'Me.Muestra_Tecnicos_AlmacenTableAdapter.Fill(Me.DataSetarnoldo.Muestra_Tecnicos_Almacen, 0)
            CON.Close()

            If opcion = "N" Then
                MuestraRelOrdenesTecnicos(0)
                Me.Button3.Visible = False
                Me.CONORDSERBindingSource.AddNew()
                GloControlaReloj = 0
                Me.StatusTextBox.Text = "P"
                'FrmSelTipServicio.Show()
                Me.Fecha_SoliciutudMaskedTextBox.Text = Now
                ACTIVA(False)
                Panel1.Enabled = True
                Panel7.Enabled = True
                'Lo pUso Eric
                Panel6.Enabled = False
                Me.Panel8.Enabled = False
                If IsNumeric(Me.ContratoTextBox.Text) = False Then LocContrato1 = 0 Else LocContrato1 = Me.ContratoTextBox.Text
                bitsist(GloUsuario, CLng(LocContrato1), LocGloSistema, "Ordenes Servicio", "Se Acceso a la Pantalla de Captura de Ordenes de Servicio para Agregar una Nueva Orden ", " ", "Se Acceso a la Pantalla de Captura de Ordenes de Servicio para Agregar una Nueva Orden", LocClv_Ciudad)
            ElseIf opcion = "C" Then
                If IsNumeric(gloClave) = True Then
                    MuestraRelOrdenesTecnicos(gloClave)
                    GloControlaReloj = 0
                    'Panel1.Enabled = False
                    Panel6.Enabled = False
                    Panel7.Enabled = False
                    Me.Panel3.Enabled = False
                    Bloquea = True
                    BUSCA(gloClave)
                    CREAARBOL()
                    Me.Button1.Enabled = False
                    Me.Panel2.Enabled = False
                    Me.Fecha_SoliciutudMaskedTextBox.Enabled = False
                    Me.CONORDSERBindingNavigator.Enabled = False
                    Me.ContratoTextBox.ReadOnly = True
                    Me.FolioTextBox.ReadOnly = True
                    Me.Fec_EjeTextBox.ReadOnly = True
                    'Me.GroupBox1.Enabled = True
                    ' Me.TreeView1.Enabled = True
                    Me.Label2.Visible = True 'Etiqueta Visible
                    Me.Label2.Text = "Se generó el número de bitácora: " & Cadena 'Etiqueta Text concatenar 
                    CON.Open()
                    Me.Muestra_no_ordenTableAdapter.Connection = CON
                    Me.Muestra_no_ordenTableAdapter.Fill(Me.DataSetarnoldo.Muestra_no_orden, CInt(gloClave))
                    Me.BUSCADetOrdSerTableAdapter.Connection = CON
                    Me.BUSCADetOrdSerTableAdapter.Fill(Me.NewSofTvDataSet.BUSCADetOrdSer, New System.Nullable(Of Long)(CType(gloClave, Long)))
                    'Me.MUESTRATRABAJOSTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRATRABAJOS, New System.Nullable(Of Integer)(CType(GloClv_TipSer, Integer)))
                    'Eric
                    Me.ValidaSiEsAcometidaTableAdapter.Connection = CON
                    Me.ValidaSiEsAcometidaTableAdapter.Fill(Me.DataSetEric.ValidaSiEsAcometida, CInt(gloClave), eResAco)
                    CON.Close()
                    If (eResAco = 1 And IdSistema = "TO") Or (eResAco = 1 And IdSistema = "SA") Or (eResAco = 1 And IdSistema = "VA") Then
                        CON.Open()
                        Me.ConRelCtePlacaTableAdapter.Connection = CON
                        Me.ConRelCtePlacaTableAdapter.Fill(Me.DataSetEric.ConRelCtePlaca, Me.ContratoTextBox.Text)
                        CON.Close()
                        Me.Label4.Visible = True
                        Me.PlacaTextBox.Visible = True
                    Else
                        Me.Label4.Visible = False
                        Me.PlacaTextBox.Visible = False
                    End If

                    'CONSULTA LA RELACION QUE HAY ENTRE LA ORDEN Y EL USUARIO
                    CON.Open()
                    Me.Consulta_RelOrdenUsuarioTableAdapter.Connection = CON
                    Me.Consulta_RelOrdenUsuarioTableAdapter.Fill(Me.DataSetEric2.Consulta_RelOrdenUsuario, gloClave)
                    CON.Close()
                    Me.Label7.Visible = True
                    Me.GeneroLabel1.Visible = True
                    If Me.StatusTextBox.Text = "E" Then
                        Me.Label12.Visible = True
                        Me.EjecutoLabel1.Visible = True
                    End If
                    If IsNumeric(Me.ContratoTextBox.Text) = False Then LocContrato1 = 0 Else LocContrato1 = Me.ContratoTextBox.Text
                    bitsist(GloUsuario, CLng(LocContrato1), LocGloSistema, "Ordenes Servicio", "Se Acceso a la Pantalla de Captura de Ordenes de Servicio para Consultar la Orden : " + gloClave.ToString, " ", "Se Acceso a la Pantalla de Captura de Ordenes de Servicio para Consultar la Orden : " + gloClave.ToString, LocClv_Ciudad)
                End If
            ElseIf opcion = "M" Then
                If IsNumeric(gloClave) = True Then
                    MuestraRelOrdenesTecnicos(0)
                    Me.Panel3.Enabled = False
                    ACTIVA(True)
                    Panel1.Enabled = True
                    Panel6.Enabled = True
                    Panel7.Enabled = True
                    BUSCA(gloClave)
                    CREAARBOL()
                    CON.Open()
                    Me.BUSCADetOrdSerTableAdapter.Connection = CON
                    Me.BUSCADetOrdSerTableAdapter.Fill(Me.NewSofTvDataSet.BUSCADetOrdSer, New System.Nullable(Of Long)(CType(gloClave, Long)))
                    Me.MUESTRATRABAJOSTableAdapter.Connection = CON
                    'Me.MUESTRATRABAJOSTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRATRABAJOS, New System.Nullable(Of Integer)(CType(GloClv_TipSer, Integer)))
                    Me.MUESTRATRABAJOSTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRATRABAJOS, 0)
                    'eric
                    Me.ValidaSiEsAcometidaTableAdapter.Connection = CON
                    Me.ValidaSiEsAcometidaTableAdapter.Fill(Me.DataSetEric.ValidaSiEsAcometida, Me.Clv_OrdenTextBox.Text, eResAco)
                    CON.Close()
                    If (eResAco = 1 And IdSistema = "TO") Or (eResAco = 1 And IdSistema = "SA") Or (eResAco = 1 And IdSistema = "VA") Then
                        CON.Open()
                        Me.ConRelCtePlacaTableAdapter.Connection = CON
                        Me.ConRelCtePlacaTableAdapter.Fill(Me.DataSetEric.ConRelCtePlaca, Me.ContratoTextBox.Text)
                        CON.Close()
                        Me.Label4.Visible = True
                        Me.PlacaTextBox.Visible = True
                    Else
                        Me.Label4.Visible = False
                        Me.PlacaTextBox.Visible = False
                    End If

                    If Me.StatusTextBox.Text = "E" Then
                        If IsNumeric(Me.ContratoTextBox.Text) = False Then LocContrato1 = 0 Else LocContrato1 = Me.ContratoTextBox.Text
                        bitsist(GloUsuario, CLng(LocContrato1), LocGloSistema, "Ordenes Servicio", "Se Acceso a la Pantalla de Captura de Ordenes de Servicio para Modificar la Orden : " + gloClave.ToString, " ", "Se Acceso a la Pantalla de Captura de Ordenes de Servicio para Modificar la Orden : " + gloClave.ToString, LocClv_Ciudad)
                        Me.Label2.Visible = True 'Etiqueta Visible
                        Me.Label2.Text = "Se generó el número de bitácora: " & Cadena 'Etiqueta Text concatenar 
                        CON.Open()
                        Me.Muestra_no_ordenTableAdapter.Connection = CON
                        Me.Muestra_no_ordenTableAdapter.Fill(Me.DataSetarnoldo.Muestra_no_orden, CInt(gloClave))
                        Panel1.Enabled = False
                        Panel6.Enabled = False
                        Panel7.Enabled = False
                        Bloquea = True
                        'CONSULTA LA RELACION QUE HAY ENTRE LA ORDEN Y EL USUARIO
                        Me.Consulta_RelOrdenUsuarioTableAdapter.Connection = CON
                        Me.Consulta_RelOrdenUsuarioTableAdapter.Fill(Me.DataSetEric2.Consulta_RelOrdenUsuario, gloClave)
                        CON.Close()
                        Me.Label7.Visible = True
                        Me.GeneroLabel1.Visible = True
                        If Me.StatusTextBox.Text = "E" Then
                            Me.Label12.Visible = True
                            Me.EjecutoLabel1.Visible = True
                        End If

                    ElseIf Me.StatusTextBox.Text = "P" Then
                        If IsNumeric(Me.ContratoTextBox.Text) = False Then LocContrato1 = 0 Else LocContrato1 = Me.ContratoTextBox.Text
                        bitsist(GloUsuario, CLng(LocContrato1), LocGloSistema, "Ordenes Servicio", "Se Acceso a la Pantalla de Captura de Ordenes de Servicio para Ejecutar la Orden : " + gloClave.ToString, " ", "Se Acceso a la Pantalla de Captura de Ordenes de Servicio para Ejecutar la Orden : " + gloClave.ToString, LocClv_Ciudad)
                        CON.Open()
                        Me.ValidarNuevoTableAdapter.Connection = CON
                        Me.ValidarNuevoTableAdapter.Fill(Me.DataSetarnoldo.ValidarNuevo, gloClave, 0, valor)
                        CON.Close()
                        If valor <= 0 Then
                            Panel1.Enabled = True
                            Panel6.Enabled = True
                            Panel7.Enabled = True
                            Bloquea = False
                            Timer1.Enabled = True
                            GloControlaReloj = 1
                        Else
                            Timer1.Enabled = False
                            GloControlaReloj = 0
                            Panel1.Enabled = False
                            Panel6.Enabled = False
                            Panel7.Enabled = False
                            Me.Panel8.Enabled = False
                            Bloquea = True
                            MsgBox("La orden no se puede ejecutar de forma manual ya que este tipo de orden de servicio al cliente se procesa de forma automatica ", MsgBoxStyle.Information)
                        End If

                        Me.StatusTextBox.Text = "E"
                        Me.Fec_EjeTextBox.Enabled = True
                        Me.Visita1TextBox.Enabled = False
                        Me.Visita2TextBox.Enabled = False
                        Me.TextBox1.Visible = False
                        Me.Fec_EjeTextBox.Focus()

                        'lineas de eric

                        If eStatusOrdSer = "P" Then
                            CON.Open()
                            Me.ValidaSiEsAcometidaTableAdapter.Connection = CON
                            Me.ValidaSiEsAcometidaTableAdapter.Fill(Me.DataSetEric.ValidaSiEsAcometida, Me.Clv_OrdenTextBox.Text, eResAco)
                            CON.Close()
                            If (eResAco = 1 And IdSistema = "TO") Or (eResAco = 1 And IdSistema = "SA") Or (eResAco = 1 And IdSistema = "VA") Then
                                Me.Label4.Visible = True
                                Me.PlacaTextBox.Visible = True
                            Else
                                Me.Label4.Visible = False
                                Me.PlacaTextBox.Visible = False
                            End If
                        End If

                        ''Me.Label4.Visible = True
                        ''Me.Label4.Text = "Ejecución : "
                        ''If IsDate(Me.Fecha_SoliciutudMaskedTextBox.Text) = True Then
                        '' Me.Fecha_EjecucionMaskedTextBox.MinDate = Me.Fecha_SoliciutudMaskedTextBox.Text
                        ''Me.DameUltimo_dia_del_MesTableAdapter.Fill(Me.NewSofTvDataSet.DameUltimo_dia_del_Mes, New System.Nullable(Of Integer)(CType(Month(Me.Fecha_SoliciutudMaskedTextBox.Text), Integer)), New System.Nullable(Of Integer)(CType(Year(Me.Fecha_SoliciutudMaskedTextBox.Text), Integer)))
                        ''Me.Fecha_EjecucionMaskedTextBox.MaxDate = FechaDateTimePicker.Value
                        ''End If
                    ElseIf Me.StatusTextBox.Text = "V" Then
                        If IsDate(Me.Visita1TextBox1.Text) = True Then
                            Me.Visita1TextBox.Enabled = False
                        End If
                        If IsDate(Me.Visita2TextBox.Text) = True Then
                            Me.Visita2TextBox.Enabled = False
                        End If

                    End If
                End If

            End If
            If IsNumeric(ContratoTextBox.Text) = True Then LocContrato1 = ContratoTextBox.Text
            'Me.CONDetOrdSerTableAdapter.Fill(Me.NewSofTvDataSet.CONDetOrdSer, New System.Nullable(Of Long)(CType(Me.Clv_OrdenTextBox.Text, Long)))
            If opcion = "M" Or opcion = "C" Then
                Dim cone As New SqlClient.SqlConnection(MiConexion)
                cone.Open()
                NUM = 0
                num2 = 0
                Me.BuscaBloqueadoTableAdapter.Connection = cone
                Me.BuscaBloqueadoTableAdapter.Fill(Me.DataSetLidia.BuscaBloqueado, loccontratoordenes, CInt(NUM), CInt(num2))
                cone.Close()
                If num2 = 1 Then
                    'eGloContrato = Contrato
                    bloq = 1
                End If
                ' Me.ContratoTextBox.Text = loccontratoordenes
                ChecaRelOrdenUsuario(gloClave)
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        'Dim CON As New SqlConnection(MiConexion)
        'CON.Open()
        'Me.BorraMotivoCanServTableAdapter.Connection = CON
        'Me.BorraMotivoCanServTableAdapter.Fill(Me.DataSetEric.BorraMotivoCanServ, Me.Clv_OrdenTextBox.Text, 0, 0, 0, 1)
        'CON.Close()
        Me.CONORDSERBindingSource.CancelEdit()
        GloBnd = True
        Elimina_Rel_Material_cliente()
        Me.Close()
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        Dim LocContrato1 As Long = 0
        If IsNumeric(Me.ContratoTextBox.Text) = False Then LocContrato1 = 0 Else LocContrato1 = Me.ContratoTextBox.Text
        bitsist(GloUsuario, CLng(LocContrato1), LocGloSistema, "Ordenes Servicio", "Se Acceso a la Pantalla de Captura de Ordenes de Servicio y se Borro la Orden " + Me.Clv_OrdenTextBox.Text, " ", "Se Acceso a la Pantalla de Captura de Ordenes de Servicio y  se  Borro la Orden " + Me.Clv_OrdenTextBox.Text, LocClv_Ciudad)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.CONORDSERTableAdapter.Connection = CON
        Me.CONORDSERTableAdapter.Delete(gloClave, 0)
        CON.Close()
        MsgBox(mensaje6)
        GloBnd = True
        Elimina_Rel_Material_cliente()
        Me.Close()
    End Sub
    Private Sub Dime_Si_Graba()
        Dim conlidia As New SqlClient.SqlConnection(MiConexion)
        Dim comando As New SqlCommand
        Dim reader As SqlDataReader
        If IsDate(Me.Visita2TextBox.Text) = True Then
            conlidia.Open()
            With comando
                .Connection = conlidia
                .CommandText = "Exec Dimesigrabaord " & CStr(gloClave) & "," & "0" & ",'" & CStr(Me.Visita2TextBox.Text) & "'"
                .CommandType = CommandType.Text
                .CommandTimeout = 0
                reader = comando.ExecuteReader()
                Using reader
                    While reader.Read
                        ' Process SprocResults datareader here.
                        Me.TextBox4.Text = reader.GetValue(0)
                    End While
                End Using
            End With
            conlidia.Close()
            dime = Me.TextBox4.Text
        Else
            dime = 0
        End If
    End Sub
    Private Function Checa_si_tiene_camdo(ByVal clv_orden As Long) As Integer
        Dim con60 As New SqlClient.SqlConnection(MiConexion)
        Dim cmd As New SqlClient.SqlCommand()
        Dim error1 As Integer = Nothing
        Try
            con60.Open()
            cmd = New SqlClient.SqlCommand()
            With cmd
                .CommandText = "Checa_si_tiene_camdo"
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Connection = con60

                Dim prm As New SqlParameter("@clv_orden", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = clv_orden
                .Parameters.Add(prm)

                Dim prm1 As New SqlParameter("@error", SqlDbType.Int)
                prm1.Direction = ParameterDirection.Output
                prm1.Value = 0
                .Parameters.Add(prm1)

                Dim i As Integer = cmd.ExecuteNonQuery()

                error1 = prm1.Value

            End With
            con60.Close()
            Checa_si_tiene_camdo = error1
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Function

    Private Sub CONORDSERBindingNavigatorSaveItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONORDSERBindingNavigatorSaveItem.Click
        Dim CON As New SqlConnection(MiConexion)
        Dim error2 As Integer = Nothing
        'CON.Open()
        If opcion <> "N" Then
            If Me.RadioButton3.Checked = True Then
                'CON.Open()
                If Me.Visita1TextBox.Text.Trim.Length > 0 Then
                    Dime_Si_Graba()
                ElseIf Me.Visita2TextBox.Text.Trim.Length > 0 Then
                    'Me.DimesigrabaordTableAdapter.Connection = CON
                    'Me.DimesigrabaordTableAdapter.Fill(Me.DataSetLidia.dimesigrabaord, gloClave, 0, Me.Visita2TextBox.Text)
                    Dime_Si_Graba()
                End If
                ' CON.Close()
            Else
                'CON.Open()
                'Me.DimesigrabaordTableAdapter.Connection = CON
                'Me.DimesigrabaordTableAdapter.Fill(Me.DataSetLidia.dimesigrabaord, gloClave, 0, Me.Fec_EjeTextBox.Text)
                Dime_Si_Graba()
                ' CON.Close()
            End If
        End If
        If dime = "2" Or dime = "0" Or opcion = "N" Then
            Try
                If IsNumeric(Me.ContratoTextBox.Text) = True Then
                    'STORED PROCEDURE Q VERIFICA SI TIENE MAS SERVICIOS EL CLIENTE
                    CON.Open()
                    Me.Valida_DetOrdenTableAdapter.Connection = CON
                    Me.Valida_DetOrdenTableAdapter.Fill(Me.NewSofTvDataSet.Valida_DetOrden, New System.Nullable(Of Long)(CType(Me.Clv_OrdenTextBox.Text, Long)))
                    CON.Close()
                    If Me.ValidacionTextBox.Text = 0 Then
                        MsgBox("Se Requiere tener datos en el Detalle de la Orden")
                        Exit Sub
                    End If

                    error2 = Checa_si_tiene_camdo(CLng(Me.Clv_OrdenTextBox.Text))

                    If error2 > 0 Then
                        MsgBox("Se Requiere Que Capture El Nuevo Domicilio", MsgBoxStyle.Information)
                        Exit Sub
                    End If


                    'AQUI VA TU CODIGO ERIC 2 DE ABRIL DE 2008
                    'If Me.StatusTextBox.Text = "P" And GLOTRABAJO = "CCABM" Then
                    ' If Len(Trim(Tecnico.Text)) = 0 Or IsNumeric(Tecnico.SelectedValue) = False Then
                    'MsgBox("Se requiere que Seleccione el Técnico por favor", MsgBoxStyle.Information)
                    'Exit Sub
                    'End If
                    If Me.StatusTextBox.Text <> "V" Then
                        Me.valida()
                        If Me.RadioButton1.Checked = False Then
                            If Me.CONTADORTextBox.Text = 1 Then
                                'If GloClv_TipSer = 2 Then
                                MsgBox("Se Requiere que Asigne el Cablemodem", MsgBoxStyle.Information)
                                Exit Sub
                            ElseIf Me.CONTADORTextBox.Text = 2 Then
                                'ElseIf GloClv_TipSer = 3 Then
                                MsgBox("Se Requiere que Asigne el Aparato ", MsgBoxStyle.Information)
                                Exit Sub
                                'End If
                            ElseIf Me.CONTADORTextBox.Text = 3 Then
                                MsgBox("Se Requiere Que Asigne el ATA", MsgBoxStyle.Information)
                                Exit Sub
                            End If
                        End If
                    End If
                    If Me.StatusTextBox.Text = "E" Then
                        Dim Pasa As Integer = 0
                        If IsDate(Mid(Me.Fec_EjeTextBox.Text, 1, 10)) = True Then
                            Dim Fecha As Date = Mid(Me.Fec_EjeTextBox.Text, 1, 10)
                            If DateValue(Fecha) >= DateValue(Me.Fecha_SoliciutudMaskedTextBox.Value) And DateValue(Fecha) <= DateValue(Me.FechaDateTimePicker.Value) Then
                                Pasa = 1
                            Else
                                MsgBox("La Fecha de la Ejecución no puede ser Menor a la Fecha de Solicitud ni Mayor a la Fecha Actual ", MsgBoxStyle.Information)
                                Me.Fec_EjeTextBox.Clear()
                                Exit Sub
                            End If
                        Else
                            MsgBox("La Fecha de Ejecución es Invalida")
                            Me.Fec_EjeTextBox.Clear()
                            Exit Sub
                        End If
                        If IsDate(Me.Fec_EjeTextBox.Text) = False Then
                            MsgBox("Se Requiere que Capture la Fecha y la Hora de Ejecución de Forma Correcta por Favor", MsgBoxStyle.Information)
                            Exit Sub
                        End If
                        If Len(Trim(Tecnico.Text)) = 0 Or IsNumeric(Tecnico.SelectedValue) = False Then
                            MsgBox("Se requiere que Seleccione el Técnico por favor", MsgBoxStyle.Information)
                            Exit Sub
                        End If
                    ElseIf Me.StatusTextBox.Text = "V" Then
                        'Visita1
                        Dim Pasa As Integer = 0
                        If IsDate(Mid(Me.Visita1TextBox.Text, 1, 10)) = True Then
                            Dim Fecha As Date = Mid(Me.Visita1TextBox.Text, 1, 10)
                            If DateValue(Fecha) >= DateValue(Me.Fecha_SoliciutudMaskedTextBox.Value) And DateValue(Fecha) <= DateValue(Me.FechaDateTimePicker.Value) Then
                                Pasa = 1
                            Else
                                MsgBox("La Fecha de la Visita no puede ser Menor a la Fecha de Solicitud ni Mayor a la Fecha Actual ", MsgBoxStyle.Information)
                                Me.Visita1TextBox.Clear()
                                Exit Sub
                            End If
                        Else
                            MsgBox("La Fecha de Ejecución es Invalida")
                            Me.Visita1TextBox.Clear()
                            Exit Sub
                        End If
                        If IsDate(Me.Visita1TextBox.Text) = False Then
                            MsgBox("Se requiere que capture la fecha y la hora de Visita de forma correcta por favor", MsgBoxStyle.Information)
                            Exit Sub
                        End If
                        'Visita1                    
                        'Visita2
                        If IsDate(Mid(Me.Visita2TextBox.Text, 1, 10)) = True Then
                            Dim Fecha As Date = Mid(Me.Visita2TextBox.Text, 1, 10)
                            If DateValue(Fecha) >= DateValue(Me.Fecha_SoliciutudMaskedTextBox.Value) And DateValue(Fecha) <= DateValue(Me.FechaDateTimePicker.Value) Then
                                Pasa = 1
                            Else
                                MsgBox("La Fecha de la Visita no puede ser Menor a la Fecha de Solicitud ni mayor a la Fecha Actual ", MsgBoxStyle.Information)
                                Me.Visita2TextBox.Clear()
                                Exit Sub
                            End If
                        Else
                            'MsgBox("La Fecha de Ejecución es Invalida")
                            Me.Visita2TextBox.Clear()
                        End If
                        If IsDate(Me.Visita2TextBox.Text) = False Then
                            'MsgBox("Se requiere que capture la fecha y la hora de Visita de forma correcta por favor", MsgBoxStyle.Information)
                            Me.Visita2TextBox.Clear()
                        End If
                        'Visita1                    
                    End If


                    If LocValida1 = True Then
                        CON.Open()
                        'Me.Dame_FolioTableAdapter.Connection = CON
                        'Me.Dame_FolioTableAdapter.Fill(Me.DataSetarnoldo.Dame_Folio, gloClave, 1, Locclv_folio)
                        'Me.Inserta_Bitacora_tecTableAdapter.Connection = CON
                        'Me.Inserta_Bitacora_tecTableAdapter.Fill(Me.DataSetarnoldo.Inserta_Bitacora_tec, clv_sessionTecnico, CLng(gloClave), Locclv_folio, 1, Locclv_tec, GloUsuario, "P", "", LocNo_Bitacora)
                        Locclv_folio = Dame_Folio(gloClave, 1, 0)
                        Inserta_Bitacora_tec_2(clv_sessionTecnico, CLng(gloClave), Locclv_folio, 1, Locclv_tec, GloClvUsuario, "P", "", Locclv_Alm)
                        Me.Inserta_Rel_Bitacora_OrdenTableAdapter.Connection = CON
                        Me.Inserta_Rel_Bitacora_OrdenTableAdapter.Fill(Me.DataSetarnoldo.Inserta_Rel_Bitacora_Orden, LocNo_Bitacora, CLng(gloClave))
                        Me.Inserta_RelCobraDescTableAdapter.Connection = CON
                        Me.Inserta_RelCobraDescTableAdapter.Fill(Me.ProcedimientosArnoldo2.Inserta_RelCobraDesc, LocNo_Bitacora, "O")
                        CON.Close()
                        LocValida1 = False
                    End If

                    If bndCCABM = True Then
                        CON.Open()
                        Me.Cambia_Tipo_cablemodemTableAdapter.Connection = CON
                        Me.Cambia_Tipo_cablemodemTableAdapter.Fill(Me.ProcedimientosArnoldo2.Cambia_Tipo_cablemodem, CLng(Me.Clv_OrdenTextBox.Text), LoctipoCablemdm)
                        Me.Guarda_Comentario_CCABMTableAdapter.Connection = CON
                        Me.Guarda_Comentario_CCABMTableAdapter.Fill(Me.ProcedimientosArnoldo2.Guarda_Comentario_CCABM, CLng(Me.Clv_OrdenTextBox.Text), LoctipoCablemdm)
                        CON.Close()
                    End If

                    'Eric
                    eRes = 0
                    eMsg = ""
                    CON.Open()
                    Me.ChecaMotivoCanServTableAdapter.Connection = CON
                    Me.ChecaMotivoCanServTableAdapter.Fill(Me.DataSetEric.ChecaMotivoCanServ, Me.Clv_OrdenTextBox.Text, eRes, eMsg)
                    CON.Close()
                    If eRes = 1 Then

                        'If opcion = "M" And GloClv_TipSer = 1 Then
                        '    GloClv_MotCan = 0
                        '    FrmMotCan.Show()
                        '    Exit Sub
                        'End If

                        'If (opcion = "N" And GloClv_TipSer = 2) Or (opcion = "N" And GloClv_TipSer = 3) Then
                        '    GloClv_MotCan = 0
                        '    FrmMotCan.Show()
                        '    Exit Sub
                        'End If

                        If (opcion = "N") Then
                            GloClv_MotCan = 0
                            FrmMotCan.Show()
                            Exit Sub
                        End If


                    End If
                    '---------------------------------------

                    'Eric
                    If (eResAco = 1 And opcion <> "N" And IdSistema = "TO") Or (eResAco = 1 And opcion <> "N" And IdSistema = "SA") Or (eResAco = 1 And opcion <> "N" And IdSistema = "VA") Then
                        If Me.PlacaTextBox.Text.Length > 0 Then
                            CON.Open()
                            Me.ConRelCtePlacaTableAdapter.Connection = CON
                            Me.ConRelCtePlacaTableAdapter.Insert(Me.ContratoTextBox.Text, Me.PlacaTextBox.Text, Me.Clv_OrdenTextBox.Text)
                            CON.Close()
                            GuardaRelOrdenUsuario()
                            CON.Open()
                            Me.Validate()
                            Me.CONORDSERBindingSource.EndEdit()
                            Me.CONORDSERTableAdapter.Connection = CON
                            Me.CONORDSERTableAdapter.Update(Me.NewSofTvDataSet.CONORDSER)
                            Me.PREEJECUTAOrdSerTableAdapter.Connection = CON
                            Me.PREEJECUTAOrdSerTableAdapter.Fill(Me.NewSofTvDataSet.PREEJECUTAOrdSer, New System.Nullable(Of Long)(CType(Me.Clv_OrdenTextBox.Text, Long)))
                            CON.Close()
                            MsgBox(mensaje5)
                            GloBnd = True
                            GloGuardo = False
                            If opcion = "N" And IdSistema <> "VA" Then
                                CON.Open()
                                Me.Imprime_OrdenTableAdapter.Connection = CON
                                Me.Imprime_OrdenTableAdapter.Fill(Me.ProcedimientosArnoldo2.Imprime_Orden, Me.Clv_OrdenTextBox.Text, Imprime)
                                CON.Close()
                                If Imprime = 0 Then
                                    ConfigureCrystalReports(0, "")
                                ElseIf Imprime = 1 Then
                                    MsgBox("La orden es de proceso Automático No se Imprimio", MsgBoxStyle.Information)
                                End If
                            End If

                            Me.Close()
                        Else
                            MsgBox("Captura # de Placa.", , "Atención")
                        End If

                    Else
                        GuardaRelOrdenUsuario()
                        CON.Open()
                        Me.Validate()
                        Me.CONORDSERBindingSource.EndEdit()
                        Me.CONORDSERTableAdapter.Connection = CON
                        Me.CONORDSERTableAdapter.Update(Me.NewSofTvDataSet.CONORDSER)
                        Me.PREEJECUTAOrdSerTableAdapter.Connection = CON
                        Me.PREEJECUTAOrdSerTableAdapter.Fill(Me.NewSofTvDataSet.PREEJECUTAOrdSer, New System.Nullable(Of Long)(CType(Me.Clv_OrdenTextBox.Text, Long)))
                        CON.Close()
                        MsgBox(mensaje5)

                        'Eric----------------------------------------------
                        Dim CONERIC As New SqlConnection(MiConexion)
                        Dim eRes As Long = 0
                        Dim eMsg As String = Nothing


                        CONERIC.Open()
                        Me.ChecaOrdSerRetiroTableAdapter.Connection = CONERIC
                        Me.ChecaOrdSerRetiroTableAdapter.Fill(Me.DataSetEric.ChecaOrdSerRetiro, CType(Me.Clv_OrdenTextBox.Text, Long), eRes, eMsg)
                        CONERIC.Close()
                        If (eRes > 0 And IdSistema = "SA") Or (eRes > 0 And IdSistema = "VA") Then
                            ImprimeOrdSerRetiro(eRes)
                        End If

                        '------------------------------------------------------

                        GloBnd = True
                        GloGuardo = False
                        If opcion = "N" Then
                            CON.Open()
                            Me.Imprime_OrdenTableAdapter.Connection = CON
                            Me.Imprime_OrdenTableAdapter.Fill(Me.ProcedimientosArnoldo2.Imprime_Orden, Me.Clv_OrdenTextBox.Text, Imprime)
                            CON.Close()
                            If Imprime = 0 And IdSistema <> "VA" Then
                                ConfigureCrystalReports(0, "")
                            ElseIf Imprime = 1 Then
                                MsgBox("La orden es de proceso Automático No se Imprimio", MsgBoxStyle.Information)
                            End If
                        End If
                        Me.Close()
                    End If



                Else
                    MsgBox(mensaje7)
                End If

                'CON.Close()
            Catch ex As System.Exception
                System.Windows.Forms.MessageBox.Show(ex.Message)
            End Try
        Else
            MsgBox("No Se Puede Grabar,la Fecha de Ejecución No puede ser de Meses Anteriores ", MsgBoxStyle.Information)
            Me.TextBox4.Clear()
        End If
    End Sub



    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        If dameStatusOrdenQueja(CInt(Me.Clv_OrdenTextBox.Text), "O") = "P" And opcion = "M" Then
            Dim res = MsgBox("¿Deseas salir sin guardar la Descarga de Material?", MsgBoxStyle.YesNo)
            If res = MsgBoxResult.Yes Then
                softv_BorraDescarga(CInt(Me.Clv_OrdenTextBox.Text), "O")
            Else
                Exit Sub
            End If
        End If

        Me.CONORDSERBindingSource.CancelEdit()
        GloBnd = True

        If IsNumeric(Me.Clv_OrdenTextBox.Text) = True Then
            If opcion = "N" Then
                Dim CON As New SqlConnection(MiConexion)
                CON.Open()
                Me.Grabar_det_ordenTableAdapter.Connection = CON
                Me.Grabar_det_ordenTableAdapter.Fill(Me.DataSetarnoldo.grabar_det_orden, CInt(Me.Clv_OrdenTextBox.Text))
                CON.Close()
            End If

        End If
        Me.Close()

    End Sub



    Private Sub RadioButton2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton2.CheckedChanged
        If Me.RadioButton2.Checked = True Then
            Me.StatusTextBox.Text = "E"
            GloControlaReloj = 1
            Me.Fec_EjeTextBox.Enabled = True
            Me.Visita1TextBox.Enabled = False
            Me.Visita2TextBox.Enabled = False
            Me.Fec_EjeTextBox.Focus()
            '           Me.TextBox1.Visible = False
            ''Me.Label4.Visible = True
            ''Me.Label4.Text = "Ejecución : "
        End If

        'If Me.StatusTextBox.Text <> "E" Then
        '    Me.StatusTextBox.Text = "E"
        '    Me.Fecha_EjecucionMaskedTextBox.Visible = True
        '    Me.Label4.Visible = True
        '    Me.Label4.Text = "Ejecución : "
        '    If IsDate(Me.Fecha_SoliciutudMaskedTextBox.Text) = True Then
        '        Me.Fecha_EjecucionMaskedTextBox.MinDate = Me.Fecha_SoliciutudMaskedTextBox.Text
        '        Me.DameUltimo_dia_del_MesTableAdapter.Fill(Me.NewSofTvDataSet.DameUltimo_dia_del_Mes, New System.Nullable(Of Integer)(CType(Month(Me.Fecha_SoliciutudMaskedTextBox.Text), Integer)), New System.Nullable(Of Integer)(CType(Year(Me.Fecha_SoliciutudMaskedTextBox.Text), Integer)))
        '        Me.Fecha_EjecucionMaskedTextBox.MaxDate = FechaDateTimePicker.Value
        '    End If
        'ElseIf Me.StatusTextBox.Text = "E" Then
        '    Me.Fecha_EjecucionMaskedTextBox.Visible = True
        '    Me.Label4.Visible = True
        '    Me.Label4.Text = "Ejecución : "
        'End If
    End Sub

    Private Sub RadioButton3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton3.CheckedChanged
        If Me.RadioButton3.Checked = True Then
            GloControlaReloj = 0
            Me.Panel5.BackColor = Color.WhiteSmoke
            Me.Panel6.BackColor = Color.WhiteSmoke
            Me.StatusTextBox.Text = "V"
            '    Me.TextBox1.Visible = False
            Me.Fec_EjeTextBox.Enabled = False
            Me.Visita1TextBox.Enabled = True
            Me.Visita2TextBox.Enabled = True
            Me.Visita1TextBox.Focus()
            ''Me.Label4.Visible = True
            ''Me.Label4.Text = "Visita : "
        End If
        'Me.StatusTextBox.Text = "V"
        'If Me.StatusTextBox.Text <> "V" Then
        '    Me.Fecha_EjecucionMaskedTextBox.Visible = True
        '    Me.Label4.Visible = True
        '    Me.StatusTextBox.Text = "V"
        '    Me.Label4.Text = "Visita : "
        '    If IsDate(Me.Fecha_SoliciutudMaskedTextBox.Text) = True Then
        '        Me.Fecha_EjecucionMaskedTextBox.MinDate = Me.Fecha_SoliciutudMaskedTextBox.Text
        '        Me.DameUltimo_dia_del_MesTableAdapter.Fill(Me.NewSofTvDataSet.DameUltimo_dia_del_Mes, New System.Nullable(Of Integer)(CType(Month(Me.Fecha_SoliciutudMaskedTextBox.Text), Integer)), New System.Nullable(Of Integer)(CType(Year(Me.Fecha_SoliciutudMaskedTextBox.Text), Integer)))
        '        Me.Fecha_EjecucionMaskedTextBox.MaxDate = FechaDateTimePicker.Value
        '    End If
        'ElseIf Me.StatusTextBox.Text = "V" Then
        '    Me.Fecha_EjecucionMaskedTextBox.Visible = True
        '   
        '    Me.Label4.Visible = True
        '    Me.Label4.Text = "Visita : "
        'End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        GLOCONTRATOSEL = 0
        'op = 3
        GloClv_TipSer = 0
        FrmSelCliente.Show()
    End Sub

    Private Sub ConfigureCrystalReports(ByVal op As String, ByVal Titulo As String)
        Try
            Dim CON As New SqlConnection(MiConexion)


            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            Dim Op1 As String = "0", Op2 As String = "0", Op3 As String = "0", Op4 As String = "0", Op5 As String = "0"
            Dim StatusPen As String = "0", StatusEje As String = "0", StatusVis As String = "0"
            Dim Fec1Ini As String = "01/01/1900", Fec1Fin As String = "01/01/1900", Fec2Ini As String = "01/01/1900", Fec2Fin As String = "01/01/1900"
            Dim Num1 As String = 0, Num2 As String = 0
            Dim nclv_trabajo As String = "0"
            Dim nClv_colonia As String = "0"
            Dim Impresora As String = Nothing
            Dim a As Integer = 0



            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")

            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim mySelectFormula As String = Titulo
            Dim OpOrdenar As String = "0"


            Dim reportPath As String = Nothing

            If IdSistema = "AG" Then

                reportPath = RutaReportes + "\ReporteFormatoOrdenesServBueno.rpt"
            ElseIf IdSistema = "TO" Then
                reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoCabStar.rpt"
            ElseIf IdSistema = "SA" Then
                reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoTvRey.rpt"
            ElseIf IdSistema = "VA" Then
                reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoCosmo.rpt"
            ElseIf IdSistema = "LO" Or IdSistema = "YU" Then
                reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoLogitel.rpt"
            End If


            customersByCityReport.Load(reportPath)
            SetDBLogonForReport(connectionInfo, customersByCityReport)

            '@Clv_TipSer int
            customersByCityReport.SetParameterValue(0, 0)
            ',@op1 smallint
            customersByCityReport.SetParameterValue(1, 1)
            ',@op2 smallint
            customersByCityReport.SetParameterValue(2, 0)
            ',@op3 smallint
            customersByCityReport.SetParameterValue(3, 0)
            ',@op4 smallint,
            customersByCityReport.SetParameterValue(4, 0)
            '@op5 smallint
            customersByCityReport.SetParameterValue(5, 0)
            ',@StatusPen bit
            customersByCityReport.SetParameterValue(6, 0)
            ',@StatusEje bit
            customersByCityReport.SetParameterValue(7, 0)
            ',@StatusVis bit,
            customersByCityReport.SetParameterValue(8, 0)
            '@Clv_OrdenIni bigint
            customersByCityReport.SetParameterValue(9, CLng(Me.Clv_OrdenTextBox.Text))
            ',@Clv_OrdenFin bigint
            customersByCityReport.SetParameterValue(10, CLng(Me.Clv_OrdenTextBox.Text))
            ',@Fec1Ini Datetime
            customersByCityReport.SetParameterValue(11, "01/01/1900")
            ',@Fec1Fin Datetime,
            customersByCityReport.SetParameterValue(12, "01/01/1900")
            '@Fec2Ini Datetime
            customersByCityReport.SetParameterValue(13, "01/01/1900")
            ',@Fec2Fin Datetime
            customersByCityReport.SetParameterValue(14, "01/01/1900")
            ',@Clv_Trabajo int
            customersByCityReport.SetParameterValue(15, 0)
            ',@Clv_Colonia int
            customersByCityReport.SetParameterValue(16, 0)
            ',@OpOrden int
            customersByCityReport.SetParameterValue(17, OpOrdenar)





            mySelectFormula = "Orden " & GloNom_TipSer
            customersByCityReport.DataDefinition.FormulaFields("Queja").Text = "'" & mySelectFormula & "'"
            CON.Open()
            Me.Dame_Impresora_OrdenesTableAdapter.Connection = CON
            Me.Dame_Impresora_OrdenesTableAdapter.Fill(Me.DataSetarnoldo.Dame_Impresora_Ordenes, Impresora, a)
            CON.Close()
            If a = 1 Then
                MsgBox("No se tiene asignada una Impresora de Ordenes de Servicio", MsgBoxStyle.Information)
                Exit Sub
            Else
                customersByCityReport.PrintOptions.PrinterName = Impresora
                customersByCityReport.PrintToPrinter(1, True, 1, 1)
            End If
            '--SetDBLogonForReport(connectionInfo)

            customersByCityReport = Nothing
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Try
            Dim myTables As Tables = myReportDocument.Database.Tables
            Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
            For Each myTable In myTables
                Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
                myTableLogonInfo.ConnectionInfo = myConnectionInfo
                myTable.ApplyLogOnInfo(myTableLogonInfo)
                myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
            Next
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        Me.Clv_TecnicoTextBox.Text = Me.ComboBox1.SelectedValue
    End Sub

    Private Sub ContratoTextBox_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles ContratoTextBox.GotFocus
        Me.ContratoTextBox.SelectionStart = 0
        Me.ContratoTextBox.SelectionLength = Len(Me.ContratoTextBox.Text)
    End Sub

    Private Sub ContratoTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles ContratoTextBox.KeyPress
        e.KeyChar = Chr(ValidaKey(ContratoTextBox, Asc(LCase(e.KeyChar)), "N"))
    End Sub

    Private Sub ContratoTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ContratoTextBox.TextChanged
        If IsNumeric(Me.ContratoTextBox.Text) = True Then
            GloContratoord = CLng(Me.ContratoTextBox.Text)
        End If
        Me.BUSCACLIENTES(0)

    End Sub



    Private Sub Clv_OrdenTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_OrdenTextBox.TextChanged
        gloClave = Me.Clv_OrdenTextBox.Text
        If IsNumeric(Me.Clv_OrdenTextBox.Text) = True Then
            dimebitacora()
        End If
    End Sub


    Private Sub StatusTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles StatusTextBox.TextChanged
        If Me.StatusTextBox.Text = "P" Then
            If Me.RadioButton1.Checked = False Then Me.RadioButton1.Checked = True
        ElseIf Me.StatusTextBox.Text = "E" Then
            If Me.RadioButton2.Checked = False Then
                Me.RadioButton2.Checked = True
                Me.TextBox1.Visible = False
                'Me.Fecha_EjecucionMaskedTextBox.Visible = True
                ''Me.Label4.Visible = True
                ''Me.Label4.Text = "Ejecución : "
            End If
        ElseIf Me.StatusTextBox.Text = "V" Then
            If Me.RadioButton3.Checked = False Then
                Me.RadioButton3.Checked = True
                Me.TextBox1.Visible = False

                'Me.Fecha_EjecucionMaskedTextBox.Visible = True
                '' Me.Label4.Visible = True
                '' Me.Label4.Text = "Visita : "
            End If

        End If
    End Sub



    Private Sub Fecha_SoliciutudMaskedTextBox_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Fecha_SoliciutudMaskedTextBox.ValueChanged
        If Me.StatusTextBox.Text = "P" Then
            'If IsDate(Me.Fecha_SoliciutudMaskedTextBox.Text) = True Then
            ' Me.Fecha_EjecucionMaskedTextBox.MinDate = Me.Fecha_SoliciutudMaskedTextBox.Text
            ' Me.DameUltimo_dia_del_MesTableAdapter.Fill(Me.NewSofTvDataSet.DameUltimo_dia_del_Mes, New System.Nullable(Of Integer)(CType(Month(Me.Fecha_SoliciutudMaskedTextBox.Text), Integer)), New System.Nullable(Of Integer)(CType(Year(Me.Fecha_SoliciutudMaskedTextBox.Text), Integer)))
            ' Me.Fecha_EjecucionMaskedTextBox.MaxDate = FechaDateTimePicker.Value
            'End If
        End If
    End Sub

    Private Sub ComboBox5_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox5.SelectedIndexChanged
        'If Me.ComboBox5.SelectedValue <> Nothing Then
        '    GloClv_TipSer = Me.ComboBox5.SelectedValue
        '    Me.TextBox2.Text = Me.ComboBox5.Text
        '    GloNom_TipSer = Me.ComboBox5.Text
        'End If
    End Sub


    Private Sub Fec_EjeTextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Fec_EjeTextBox1.TextChanged
        Me.Fec_EjeTextBox.Text = Me.Fec_EjeTextBox1.Text
    End Sub

    Private Sub Visita1TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Visita1TextBox1.TextChanged
        Me.Visita1TextBox.Text = Me.Visita1TextBox1.Text
    End Sub

    Private Sub Visita2TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Visita2TextBox1.TextChanged
        Me.Visita2TextBox.Text = Me.Visita2TextBox1.Text
    End Sub

    Private Sub Fec_EjeTextBox_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles Fec_EjeTextBox.GotFocus
        Fec_EjeTextBox.SelectionStart = 0
        Fec_EjeTextBox.SelectionLength = Len(Fec_EjeTextBox.Text)
    End Sub



    Private Sub Fec_EjeTextBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Fec_EjeTextBox.TextChanged
        If IsDate(Mid(Me.Fec_EjeTextBox.Text, 1, 10)) = True Then
            Dim Fecha As Date = Mid(Me.Fec_EjeTextBox.Text, 1, 10)
            If DateValue(Fecha) = DateValue("01/01/1900") Then
                Me.Fec_EjeTextBox.Clear()
            End If
        End If
        If IsDate(Me.Fec_EjeTextBox.Text) = True Then
            Me.Fec_EjeTextBox1.Text = Me.Fec_EjeTextBox.Text
            Panel5.BackColor = Color.WhiteSmoke
            If Len(Trim(Me.Fec_EjeTextBox1.Text)) = 10 Then
                LocFecEje = True
            End If
        Else
            LocFecEje = False
        End If
    End Sub

    Private Sub Visita1TextBox_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles Visita1TextBox.GotFocus
        Visita1TextBox.SelectionStart = 0
        Visita1TextBox.SelectionLength = Len(Visita1TextBox.Text)
    End Sub


    Private Sub Visita1TextBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Visita1TextBox.TextChanged
        If IsDate(Mid(Me.Visita1TextBox.Text, 1, 10)) = True Then
            Dim Fecha As Date = Mid(Me.Visita1TextBox.Text, 1, 10)
            If DateValue(Fecha) = DateValue("01/01/1900") Then
                Me.Visita1TextBox.Clear()
            End If
        End If
        If IsDate(Me.Visita1TextBox.Text) = True Then
            Me.Visita1TextBox1.Text = Me.Visita1TextBox.Text
        End If
    End Sub

    Private Sub Visita2TextBox_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles Visita2TextBox.GotFocus
        Visita2TextBox.SelectionStart = 0
        Visita2TextBox.SelectionLength = Len(Visita2TextBox.Text)
    End Sub


    Private Sub Visita2TextBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Visita2TextBox.TextChanged
        If IsDate(Mid(Me.Visita2TextBox.Text, 1, 10)) = True Then
            Dim Fecha As Date = Mid(Me.Visita2TextBox.Text, 1, 10)
            If DateValue(Fecha) = DateValue("01/01/1900") Then
                Me.Visita2TextBox.Clear()
            End If
        End If
        If IsDate(Me.Visita2TextBox.Text) = True Then
            Me.Visita2TextBox1.Text = Me.Visita2TextBox.Text
        End If
    End Sub

    Private Sub Button9_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click

        Dim clavedetordSer As Long = 0
        If Me.StatusTextBox.Text = "P" Then
            If IsNumeric(Me.ContratoTextBox.Text) = False Then
                MsgBox(mensaje7)
                Exit Sub
            End If
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.Validate()
            Me.CONORDSERBindingSource.EndEdit()
            Me.CONORDSERTableAdapter.Connection = CON
            Me.CONORDSERTableAdapter.Update(Me.NewSofTvDataSet.CONORDSER)

            'Este no Me.CONDetOrdSerTableAdapter.Insert(Me.Clv_OrdenTextBox.Text, Me.Clv_TrabajoTextBox.Text, Me.ObsTextBox1.Text, Me.SeRealizaCheckBox.Checked, clavedetordSer)
            gloClv_Orden = Me.Clv_OrdenTextBox.Text
            Contrato = Me.ContratoTextBox.Text
            CON.Close()
            FrmDetOrSer.Show()

        Else
            MsgBox("Solo se puede agregar Servicios al Cliente cuando esta con Status de Pendiente", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub Button2_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If IsNumeric(Me.Clv_OrdenTextBox.Text) = True Then
            Valida_DetOrdenDelete(CInt(Me.Clv_OrdenTextBox.Text), 1)

            If valor > 0 Then
                If IsNumeric(BUSCADetOrdSerDataGridView.SelectedCells(0).Value) = True Then
                    Me.ClaveTextBox.Text = BUSCADetOrdSerDataGridView.SelectedCells(0).Value
                End If
                If IsNumeric(Me.ClaveTextBox.Text) = True Then
                    Dim CON As New SqlConnection(MiConexion)
                    CON.Open()
                    Me.BorraMotivoCanServTableAdapter.Connection = CON
                    Me.BorraMotivoCanServTableAdapter.Fill(Me.DataSetEric.BorraMotivoCanServ, Me.Clv_OrdenTextBox.Text, 0, Me.ClaveTextBox.Text, 0, 2)
                    Me.CONDetOrdSerTableAdapter.Connection = CON
                    Me.CONDetOrdSerTableAdapter.Delete(Me.ClaveTextBox.Text)
                    Me.BUSCADetOrdSerTableAdapter.Connection = CON
                    Me.BUSCADetOrdSerTableAdapter.Fill(Me.NewSofTvDataSet.BUSCADetOrdSer, New System.Nullable(Of Long)(CType(Me.Clv_OrdenTextBox.Text, Long)))

                    CON.Close()
                    Me.ClaveTextBox.Text = 0
                End If
            End If
        End If
    End Sub
    Private Sub dame_clv_tipser(ByVal clv_txt As String)
        Dim CON As New SqlConnection(MiConexion)
        Dim cmd As New SqlClient.SqlCommand
        cmd = New SqlClient.SqlCommand()
        Try
            If GloClv_TipSer > 0 Then
                GloClv_TipSer = 0
            End If
            CON.Open()
            With cmd
                .CommandText = "Dame_tipo_servicio_trabajo"
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Connection = CON

                Dim prm As New SqlParameter("@clv_txt", SqlDbType.VarChar, 10)
                Dim prm1 As New SqlParameter("@clv_tipser", SqlDbType.Int)

                prm.Direction = ParameterDirection.Input
                prm1.Direction = ParameterDirection.Output

                prm.Value = clv_txt
                prm1.Value = 0

                .Parameters.Add(prm)
                .Parameters.Add(prm1)

                Dim i As Integer = cmd.ExecuteNonQuery()

                GloClv_TipSer = prm1.Value
            End With
            CON.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub BUSCADetOrdSerDataGridView_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles BUSCADetOrdSerDataGridView.CellClick

        Dim Pasa As Integer = 0
        If GloControlaReloj = 0 Then
            Pasa = 0
        Else
            If LocTec = True And LocFecEje = True Then
                Pasa = 0
            Else
                Pasa = 1
            End If
        End If

        Locclv_tec = Me.Tecnico.SelectedValue
        ''dame_clv_tipser(

        If Pasa = 0 Then
            If IsNumeric(BUSCADetOrdSerDataGridView.SelectedCells(0).Value) = True Then
                GloDetClave = BUSCADetOrdSerDataGridView.SelectedCells(0).Value
                Contrato = Me.ContratoTextBox.Text
                gloClv_Orden = Me.Clv_OrdenTextBox.Text
                GLOTRABAJO = RTrim(LTrim(Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 6)))
                dame_clv_tipser(GLOTRABAJO)
                If Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 5) = "CAMDO" Or Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 5) = "CADIG" Or Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 5) = "CANET" Then
                    Me.BUSCADetOrdSerDataGridView.Enabled = False
                    FrmCAMDO.Show()
                ElseIf Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 5) = "CONEX" Then

                    Me.BUSCADetOrdSerDataGridView.Enabled = False
                    FrmCONEX.Show()
                ElseIf Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 5) = "CEXTE" Then

                    Me.BUSCADetOrdSerDataGridView.Enabled = False
                    FrmCEXTE.Show()
                ElseIf Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 5) = "CANEX" Then

                    Me.BUSCADetOrdSerDataGridView.Enabled = False
                    FrmCANEX.Show()
                ElseIf Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 5) = "BCABM" Then

                    Me.BUSCADetOrdSerDataGridView.Enabled = False
                    FrmRelCablemodemClientes.Show()
                ElseIf Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 5) = "BAPAR" Then
                    Me.BUSCADetOrdSerDataGridView.Enabled = False
                    FrmRelCablemodemClientesDigital.Show()
                ElseIf Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 5) = "ICABM" Then
                    If Me.StatusTextBox.Text = "P" Then
                        Me.BUSCADetOrdSerDataGridView.Enabled = False
                        'FrmRelCablemodemClientes.Show()
                        FrmICABMAsigna.Show()
                    ElseIf Me.StatusTextBox.Text = "E" Then
                        Me.BUSCADetOrdSerDataGridView.Enabled = False
                        'GloClv_TipSer = 2
                        FrmICABMAsigna.Show()
                    End If
                ElseIf Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 6) = "ICABMT" Then
                    If Me.StatusTextBox.Text = "P" Then
                        Me.BUSCADetOrdSerDataGridView.Enabled = False
                        'FrmRelCablemodemClientes.Show()
                        FrmICABMAsigna.Show()
                    ElseIf Me.StatusTextBox.Text = "E" Then
                        Me.BUSCADetOrdSerDataGridView.Enabled = False
                        'GloClv_TipSer = 2
                        FrmICABMAsigna.Show()
                    End If
                ElseIf Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 5) = "IAPAR" Then
                    If Me.StatusTextBox.Text = "P" Then
                        Me.BUSCADetOrdSerDataGridView.Enabled = False
                        'FrmRelCablemodemClientesDigital.Show()
                        FrmIAPARAsigna.Show()
                    ElseIf Me.StatusTextBox.Text = "E" Then
                        Me.BUSCADetOrdSerDataGridView.Enabled = False
                        'GloClv_TipSer = 3
                        FrmIAPARAsigna.Show()
                    End If
                ElseIf Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 5) = "CCABM" Then
                    If Me.StatusTextBox.Text = "P" Then
                        MsgBox("Sólo se puede Asignar el Aparato hasta que la Orden sea Ejecutada", , "Atención")
                    ElseIf Me.StatusTextBox.Text = "E" Then
                        Me.BUSCADetOrdSerDataGridView.Enabled = False
                        FrmICABMAsigna.Show()
                    End If
                ElseIf Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 5) = "CAPAR" Then
                    If Me.StatusTextBox.Text = "P" Then
                        MsgBox("Sólo se puede Asignar el Aparato hasta que la Orden sea Ejecutada", , "Atención")
                    ElseIf Me.StatusTextBox.Text = "E" Then
                        Me.BUSCADetOrdSerDataGridView.Enabled = False
                        FrmIAPARAsigna.Show()
                    End If
                ElseIf (Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 5) = "IPAQU" Or Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 5) = "BPAQU" Or Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 5) = "DPAQU" Or Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 5) = "RPAQU") Then
                    'If Me.StatusTextBox.Text = "E" Then
                    Me.BUSCADetOrdSerDataGridView.Enabled = False
                    FrmrRelPaquetesdelCliente.Show()
                    'End If
                ElseIf (Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 6)) = "IPAQUT" Then
                    'If Me.StatusTextBox.Text = "E" Then
                    Me.BUSCADetOrdSerDataGridView.Enabled = False
                    FrmrRelPaquetesdelCliente.Show()
                    'End If
                ElseIf (Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 5) = "IPAQD" Or Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 5) = "BPAQD" Or Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 5) = "DPAQD" Or Mid(Trim(BUSCADetOrdSerDataGridView.SelectedCells(3).Value), 1, 5) = "RPAQD") Then
                    'If Me.StatusTextBox.Text = "E" Then
                    Me.BUSCADetOrdSerDataGridView.Enabled = False
                    FrmrRelPaquetesdelClienteDigital.Show()
                    'End If
                End If
            End If
        Else
            MsgBox("Primero Capture los Datos Solicitados ", MsgBoxStyle.Information)
        End If
    End Sub




    Private Sub valida()
        Dim OP As Integer = 0
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            'If GloClv_TipSer = 2 Then
            '    OP = 0
            'ElseIf GloClv_TipSer = 3 Then
            '    OP = 1
            'End If
            OP = 0
            Me.ValidaTrabajosTableAdapter.Connection = CON
            Me.ValidaTrabajosTableAdapter.Fill(Me.NewSofTvDataSet.ValidaTrabajos, New System.Nullable(Of Long)(CType(Me.Clv_OrdenTextBox.Text, Long)), New System.Nullable(Of Integer)(CType(OP, Integer)))
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        If GloControlaReloj = 1 Then
            If LocFecEje = False Then
                If Me.Panel5.BackColor = Color.WhiteSmoke Then
                    Me.Panel5.BackColor = Color.Gold
                    If Me.Fec_EjeTextBox.Focused = False Then
                        Me.Fec_EjeTextBox.Focus()
                    End If
                Else
                    Me.Panel5.BackColor = Color.WhiteSmoke
                End If
            ElseIf LocTec = False Then
                If Me.Panel6.BackColor = Color.WhiteSmoke Then
                    Me.Panel6.BackColor = Color.Gold
                    If Me.Tecnico.Focused = False Then
                        Me.Tecnico.Focus()
                    End If
                Else
                    Me.Panel6.BackColor = Color.WhiteSmoke
                End If
            End If
        End If
    End Sub

    Private Sub Tecnico_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Tecnico.SelectedIndexChanged
        If Len(Trim(Tecnico.Text)) > 0 And IsNumeric(Tecnico.SelectedValue) = True Then
            LocTec = True
            Panel6.BackColor = Color.WhiteSmoke
        Else
            LocTec = False
        End If
    End Sub

    Private Sub Tecnico_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Tecnico.TextChanged
        If Len(Trim(Tecnico.Text)) > 0 And IsNumeric(Tecnico.SelectedValue) = True Then
            LocTec = True
            Panel6.BackColor = Color.WhiteSmoke
        Else
            LocTec = False
        End If
    End Sub


    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If IsNumeric(Me.Tecnico.SelectedValue) = True Then
            Locclv_tec = Me.Tecnico.SelectedValue
            gLOVERgUARDA = 0
            If Me.CONORDSERBindingNavigator.Enabled = False Then
                gLOVERgUARDA = 1
            End If
            gloClv_Orden = Me.Clv_OrdenTextBox.Text
            Contrato = Me.ContratoTextBox.Text
            'Me.DameClv_Session_TecnicosTableAdapter.Fill(Me.DataSetarnoldo.DameClv_Session_Tecnicos, clv_sessionTecnico)
            Dim frm As New SoftvNew.FrmDescargaMaterialTec()
            frm.Clv_Orden = gloClv_Orden
            frm.IdTecnico = Locclv_tec
            frm.tipoDescarga = "O"
            frm.ShowDialog()
            'FrmDescargaMaterialTec.Show()
        Else
            MsgBox("Seleccione el Tecnico por favor")
        End If
    End Sub



    Private Sub dimebitacora()
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Label3.Visible = True
            Me.FolioTextBox.Visible = True
            Me.DimeSiTieneunaBitacoraTableAdapter.Connection = CON
            Me.DimeSiTieneunaBitacoraTableAdapter.Fill(Me.DataSetEdgarRev2.DimeSiTieneunaBitacora, New System.Nullable(Of Long)(CType(Me.Clv_OrdenTextBox.Text, Long)))
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub ESHOTELLabel1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub ImprimeOrdSerRetiro(ByVal ClvOrden As Long)

        Try
            Dim CON As New SqlConnection(MiConexion)

            Dim Titulo As String = Nothing
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            Dim Op1 As String = "0", Op2 As String = "0", Op3 As String = "0", Op4 As String = "0", Op5 As String = "0"
            Dim StatusPen As String = "0", StatusEje As String = "0", StatusVis As String = "0"
            Dim Fec1Ini As String = "01/01/1900", Fec1Fin As String = "01/01/1900", Fec2Ini As String = "01/01/1900", Fec2Fin As String = "01/01/1900"
            Dim Num1 As String = 0, Num2 As String = 0
            Dim nclv_trabajo As String = "0"
            Dim nClv_colonia As String = "0"
            Dim Impresora As String = Nothing
            Dim a As Integer = 0



            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")

            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim mySelectFormula As String = Titulo
            Dim OpOrdenar As String = "0"


            Dim reportPath As String = Nothing

            If IdSistema = "AG" Then

                reportPath = RutaReportes + "\ReporteFormatoOrdenesServBueno.rpt"
            ElseIf IdSistema = "TO" Then
                reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoCabStar.rpt"
            ElseIf IdSistema = "SA" Then
                reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoTvRey.rpt"
            ElseIf IdSistema = "VA" Then
                reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoCosmo.rpt"
            ElseIf IdSistema = "LO" Or IdSistema = "YU" Then
                reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoLogitel.rpt"
            End If


            customersByCityReport.Load(reportPath)
            SetDBLogonForReport(connectionInfo, customersByCityReport)

            '@Clv_TipSer int
            customersByCityReport.SetParameterValue(0, 0)
            ',@op1 smallint
            customersByCityReport.SetParameterValue(1, 1)
            ',@op2 smallint
            customersByCityReport.SetParameterValue(2, 0)
            ',@op3 smallint
            customersByCityReport.SetParameterValue(3, 0)
            ',@op4 smallint,
            customersByCityReport.SetParameterValue(4, 0)
            '@op5 smallint
            customersByCityReport.SetParameterValue(5, 0)
            ',@StatusPen bit
            customersByCityReport.SetParameterValue(6, 1)
            ',@StatusEje bit
            customersByCityReport.SetParameterValue(7, 0)
            ',@StatusVis bit,
            customersByCityReport.SetParameterValue(8, 0)
            '@Clv_OrdenIni bigint
            customersByCityReport.SetParameterValue(9, ClvOrden)
            ',@Clv_OrdenFin bigint
            customersByCityReport.SetParameterValue(10, ClvOrden)
            ',@Fec1Ini Datetime
            customersByCityReport.SetParameterValue(11, "01/01/1900")
            ',@Fec1Fin Datetime,
            customersByCityReport.SetParameterValue(12, "01/01/1900")
            '@Fec2Ini Datetime
            customersByCityReport.SetParameterValue(13, "01/01/1900")
            ',@Fec2Fin Datetime
            customersByCityReport.SetParameterValue(14, "01/01/1900")
            ',@Clv_Trabajo int
            customersByCityReport.SetParameterValue(15, 0)
            ',@Clv_Colonia int
            customersByCityReport.SetParameterValue(16, 0)
            ',@OpOrden int
            customersByCityReport.SetParameterValue(17, OpOrdenar)





            mySelectFormula = "Orden " & GloNom_TipSer
            customersByCityReport.DataDefinition.FormulaFields("Queja").Text = "'" & mySelectFormula & "'"
            CON.Open()
            Me.Dame_Impresora_OrdenesTableAdapter.Connection = CON
            Me.Dame_Impresora_OrdenesTableAdapter.Fill(Me.DataSetarnoldo.Dame_Impresora_Ordenes, Impresora, a)
            CON.Close()
            If a = 1 Then
                MsgBox("No se tiene asignada una Impresora de Ordenes de Servicio", MsgBoxStyle.Information)
                Exit Sub
            Else
                customersByCityReport.PrintOptions.PrinterName = Impresora
                customersByCityReport.PrintToPrinter(1, True, 1, 1)
            End If
            '--SetDBLogonForReport(connectionInfo)

            customersByCityReport = Nothing
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub GuardaRelOrdenUsuario()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.NueRelOrdenUsuarioTableAdapter.Connection = CON
        Me.NueRelOrdenUsuarioTableAdapter.Fill(Me.DataSetEric.NueRelOrdenUsuario, CLng(Me.Clv_OrdenTextBox.Text), GloClvUsuario, Me.StatusTextBox.Text)
        CON.Close()
    End Sub

    Private Sub Panel1_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Panel1.Paint

    End Sub




    Private Sub BUSCADetOrdSerDataGridView_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles BUSCADetOrdSerDataGridView.CellContentClick

    End Sub

    Private Sub ChecaRelOrdenUsuario(ByVal Clv_Orden As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ChecaRelOrdenUsuario", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_Orden", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_Orden
        comando.Parameters.Add(parametro)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            conexion.Close()
            conexion.Dispose()
        Catch ex As Exception
            conexion.Close()
            conexion.Dispose()
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub
    Private Sub Elimina_Rel_Material_cliente()

        Dim con As New SqlConnection(MiConexion)
        Dim com As New SqlCommand("Elimina_Rel_Material_cliente", con)
        com.CommandType = CommandType.StoredProcedure

        Dim par1 As New SqlParameter("@orden", SqlDbType.BigInt)
        par1.Direction = ParameterDirection.Input
        par1.Value = gloClv_Orden
        com.Parameters.Add(par1)

        Try
            con.Open()
            com.ExecuteNonQuery()

        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            con.Dispose()
            con.Close()
        End Try

    End Sub
    Private Sub MuestraRelOrdenesTecnicos(ByVal Clv_Orden As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC MuestraRelOrdenesTecnicos ")
        strSQL.Append(CStr(Clv_Orden))

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            conexion.Open()
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            Me.Tecnico.DataSource = bindingSource
        Catch ex As Exception
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub
    Private Sub Inserta_Bitacora_Tecnico()
        Dim con As New SqlConnection(MiConexion)
        Dim com As New SqlCommand("Inserta_Bitacora_Tecnico", con)
        com.CommandType = CommandType.StoredProcedure

        Dim par1 As New SqlParameter("@CLV_SESSION", SqlDbType.BigInt)
        par1.Direction = ParameterDirection.Input
        par1.Value = clv_sessionTecnico
        com.Parameters.Add(par1)

        Dim par4 As New SqlParameter("@CLV_TECNICO", SqlDbType.Int)
        par4.Direction = ParameterDirection.Input
        par4.Value = Locclv_tec
        com.Parameters.Add(par4)

        Try
            con.Open()
            com.ExecuteNonQuery()

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        Finally
            con.Close()
            con.Dispose()

        End Try
    End Sub
    Public Sub Inserta_Rel_Bitacora_Orden()
        Dim con As New SqlConnection(MiConexion)
        Dim com As New SqlCommand("Inserta_Rel_Bitacora_Orden", con)
        com.CommandType = CommandType.StoredProcedure

        Dim par2 As New SqlParameter("@clv_orden", SqlDbType.Int)
        par2.Direction = ParameterDirection.Input
        par2.Value = CLng(gloClave)
        com.Parameters.Add(par2)

        Dim par3 As New SqlParameter("@tipo", SqlDbType.VarChar, 1)
        par3.Direction = ParameterDirection.Input
        par3.Value = "O"
        com.Parameters.Add(par3)

        Dim par4 As New SqlParameter("@contrato", SqlDbType.Int)
        par4.Direction = ParameterDirection.Input
        par4.Value = Contrato
        com.Parameters.Add(par4)

        Try
            con.Open()
            com.ExecuteNonQuery()

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        Finally
            con.Close()
            con.Dispose()

        End Try
    End Sub
    Private Sub MODORDSER(ByVal Clv_Orden As Long, ByVal Clv_TipSer As Integer, ByVal Contrato As Long, ByVal Fec_Sol As String, ByVal Fec_Eje As String, ByVal Visita1 As String, ByVal Visita2 As String, ByVal Status As String, ByVal Clv_Tecnico As Integer, ByVal IMPRESA As Boolean, ByVal Clv_FACTURA As Long, ByVal Obs As String, ByVal ListadeArticulos As String)

        Dim con As New SqlConnection(MiConexion)
        Dim com As New SqlCommand("MODORDSER", con)
        com.CommandType = CommandType.StoredProcedure

        Dim par1 As New SqlParameter("@clv_orden", SqlDbType.BigInt)
        Dim par2 As New SqlParameter("@Clv_TipSer", SqlDbType.Int)
        Dim par3 As New SqlParameter("@Contrato", SqlDbType.BigInt)
        Dim par4 As New SqlParameter("@Fec_Sol", SqlDbType.DateTime)
        Dim par5 As New SqlParameter("@Fec_Eje", SqlDbType.DateTime)
        Dim par6 As New SqlParameter("@Visita1", SqlDbType.DateTime)
        Dim par7 As New SqlParameter("@Visita2", SqlDbType.DateTime)
        Dim par8 As New SqlParameter("@Status", SqlDbType.VarChar, 1)
        Dim par9 As New SqlParameter("@Clv_Tecnico", SqlDbType.Int)
        Dim par10 As New SqlParameter("@IMPRESA", SqlDbType.Bit)
        Dim par11 As New SqlParameter("@Clv_FACTURA", SqlDbType.BigInt)
        Dim par12 As New SqlParameter("@Obs", SqlDbType.VarChar, 255)
        Dim par13 As New SqlParameter("@ListadeArticulos", SqlDbType.VarChar, 1)

        par1.Direction = ParameterDirection.Input
        par2.Direction = ParameterDirection.Input
        par3.Direction = ParameterDirection.Input
        par4.Direction = ParameterDirection.Input
        par5.Direction = ParameterDirection.Input
        par6.Direction = ParameterDirection.Input
        par7.Direction = ParameterDirection.Input
        par8.Direction = ParameterDirection.Input
        par9.Direction = ParameterDirection.Input
        par10.Direction = ParameterDirection.Input
        par11.Direction = ParameterDirection.Input
        par12.Direction = ParameterDirection.Input
        par13.Direction = ParameterDirection.Input

        par1.Value = Clv_Orden
        par2.Value = Clv_TipSer
        par3.Value = Contrato
        par4.Value = Fec_Sol
        par5.Value = Fec_Eje
        par6.Value = Visita1
        par7.Value = Visita2
        par8.Value = Status
        par9.Value = Clv_Tecnico
        par10.Value = IMPRESA
        par11.Value = Clv_FACTURA
        par12.Value = Obs
        par13.Value = ListadeArticulos

        com.Parameters.Add(par1)
        com.Parameters.Add(par2)
        com.Parameters.Add(par3)
        com.Parameters.Add(par4)
        com.Parameters.Add(par5)
        com.Parameters.Add(par6)
        com.Parameters.Add(par7)
        com.Parameters.Add(par8)
        com.Parameters.Add(par9)
        com.Parameters.Add(par10)
        com.Parameters.Add(par11)
        com.Parameters.Add(par12)
        com.Parameters.Add(par13)


        Try
            con.Open()
            com.ExecuteNonQuery()

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        Finally
            con.Close()
            con.Dispose()

        End Try

    End Sub

    ''''*********-------------    FIN   -----------****************
    Private Sub guardaBitacoraCANUM(ByVal contrato As Long, ByVal tel_old As Integer, ByVal tel_nes As Integer, ByVal OP As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("guardaBitacoraCANUM", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = contrato
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@tel_old", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = tel_old
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@tel_new", SqlDbType.Int)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = tel_nes
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@OP", SqlDbType.Int)
        parametro4.Direction = ParameterDirection.Input
        parametro4.Value = OP
        comando.Parameters.Add(parametro4)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    'Private Sub Almacen_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    If Len(Trim(Almacen.Text)) > 0 And IsNumeric(Almacen.SelectedValue) = True Then
    '        LocAlm = True
    '        Panel9.BackColor = Color.WhiteSmoke
    '    Else
    '        LocAlm = False
    '    End If
    'End Sub

    'Private Sub Almacen_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    If Len(Trim(Almacen.Text)) > 0 And IsNumeric(Almacen.SelectedValue) = True Then
    '        LocAlm = True
    '        Panel9.BackColor = Color.WhiteSmoke
    '    Else
    '        LocAlm = False
    '    End If
    'End Sub

    Private Sub Inserta_Bitacora_tec_2(ByVal Clv_Session As Long, ByVal Clv_Orden As Long, ByRef clv_folio As Integer, ByVal clv_categoria As Integer, ByVal clv_tecnico As Integer, ByVal clv_usuario As String, ByVal Status As String, ByVal obs As String, ByVal idAlmacen As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("Inserta_Bitacora_tec_2", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_Session
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Clv_Orden", SqlDbType.BigInt)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Clv_Orden
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@clv_folio", SqlDbType.Int)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = clv_folio
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@clv_categoria", SqlDbType.Int)
        parametro4.Direction = ParameterDirection.Input
        parametro4.Value = clv_categoria
        comando.Parameters.Add(parametro4)

        Dim parametro5 As New SqlParameter("@clv_tecnico", SqlDbType.Int)
        parametro5.Direction = ParameterDirection.Input
        parametro5.Value = clv_tecnico
        comando.Parameters.Add(parametro5)

        Dim parametro6 As New SqlParameter("@clv_usuario", SqlDbType.VarChar, 50)
        parametro6.Direction = ParameterDirection.Input
        parametro6.Value = clv_usuario
        comando.Parameters.Add(parametro6)

        Dim parametro7 As New SqlParameter("@Status", SqlDbType.VarChar, 50)
        parametro7.Direction = ParameterDirection.Input
        parametro7.Value = Status
        comando.Parameters.Add(parametro7)

        Dim parametro8 As New SqlParameter("@obs", SqlDbType.VarChar, 200)
        parametro8.Direction = ParameterDirection.Input
        parametro8.Value = obs
        comando.Parameters.Add(parametro8)

        Dim parametro9 As New SqlParameter("@no_bitacora", SqlDbType.BigInt)
        parametro9.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro9)

        Dim parametro10 As New SqlParameter("@idAlmacen", SqlDbType.Int)
        parametro10.Direction = ParameterDirection.Input
        parametro10.Value = idAlmacen
        comando.Parameters.Add(parametro10)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            LocNo_Bitacora = CLng(parametro9.Value)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub
    Private Sub BORRAMETRAJE(ByVal CLV_ORDEN As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("BORRAMETRAJE", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@CLV_ORDEN", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = CLV_ORDEN
        comando.Parameters.Add(parametro)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub Valida_DetOrdenDelete(ByVal CLV_ORDEN As Long, ByVal OP As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("Valida_DetOrdenDelete", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@CLV_ORDEN", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = CLV_ORDEN
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@OP", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = OP
        comando.Parameters.Add(parametro2)

        Dim parametro1 As New SqlParameter("@error", SqlDbType.Int)
        parametro1.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro1)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            valor = CInt(parametro1.Value)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub btnHistorial_Click(sender As System.Object, e As System.EventArgs) Handles btnHistorial.Click
        FrmHistorialDescargas.clvOrdenHistorialDescarga = CLng(Me.Clv_OrdenTextBox.Text)
        FrmHistorialDescargas.tipoDescargaHirstorialDescarga = "O"
        FrmHistorialDescargas.Show()
    End Sub
End Class