﻿Imports System.Data.SqlClient

Public Class FrmFechasEjecOrdDESCO

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If esEjecucionMasiva = True Then
            esEjecucionMasiva = False
            GloFecha_Fin = Me.DateTimePicker2.Text
            nombreColonia = CStr(ComboBox1.Text)
            fechaEjecucion = Me.DateTimePicker3.Text
            clv_Tecnico = CInt(ComboBox3.Text)
            FrmMiMenu.ejecucionMasivaOrdenes()
            noFecha = False
            Me.Close()
        End If
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        esEjecucionMasiva = False
        noFecha = False
        Me.Close()
    End Sub

    Private Sub FrmFechasEjecOrdDESCO_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated

    End Sub

    Private Sub FrmFechasEjecOrdDESCO_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.DateTimePicker2.MaxDate = Today
        proyectos(ComboBox2, "SELECT Nombre FROM AlmacenWebDB.dbo.tbl_tecnicos ORDER BY Nombre ASC")
    End Sub

    Public Sub proyectos(ByVal ComboBox As ComboBox, ByVal sql As String)
        Dim cn As New SqlConnection(MiConexion)
        Try
            cn.Open()
            Dim cmd As New SqlCommand(sql, cn)
            Dim da As New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds)
            ComboBox.DataSource = ds.Tables(0)
            ComboBox.DisplayMember = ds.Tables(0).Columns(0).Caption.ToString
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString, "ERROR AL CARGAR LOS DATOS", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally
            If cn.State = ConnectionState.Open Then
                cn.Close()
            End If
        End Try
    End Sub

    Private Sub ComboBox2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox2.SelectedIndexChanged
        proyectos(ComboBox3, "select Clave from AlmacenWebDB.dbo.tbl_tecnicos where Nombre='" + CStr(ComboBox2.Text) + "'")
    End Sub

    Private Sub DateTimePicker3_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker3.ValueChanged

    End Sub

    Private Sub DateTimePicker2_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker2.ValueChanged
        Me.DateTimePicker3.MinDate = Me.DateTimePicker2.Value
        Me.DateTimePicker3.MaxDate = Today
        noFecha = True
        GloFecha_Fin = Me.DateTimePicker2.Text
        llenaCombo()
    End Sub

    Public Sub llenaCombo()
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("dameColoniasOrdenes", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par As New SqlParameter("@fecha", SqlDbType.DateTime)
        par.Direction = ParameterDirection.Input
        par.Value = GloFecha_Fin
        comando.Parameters.Add(par)
        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

        ComboBox1.Text = ""

        proyectos(ComboBox1, "SELECT colonia FROM coloniasOrdenes")
    End Sub

End Class