﻿Public Class FrmPregunta
    Public locPreguntaContrato As Integer

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs)
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        BndReactivacionRes = 1
        glocontratoreactivacion = 0
        Me.DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub FrmPregunta_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        CobraABajasClientesTV(locPreguntaContrato)

    End Sub

    Private Sub Button2_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim accesoReportes As New FrmAccesoReportes
        Me.DialogResult = Windows.Forms.DialogResult.OK

        BndReactivacion = True
        BndReactivacionRes = 0
        glocontratoreactivacion = 0
        If accesoReportes.ShowDialog = Windows.Forms.DialogResult.Cancel Then
            Me.DialogResult = Windows.Forms.DialogResult.Cancel
            BndReactivacion = True
            BndReactivacionRes = 0
            glocontratoreactivacion = 0
        End If

        Me.Close()
    End Sub




    Private Sub CobraABajasClientesTV(ByVal CONTRATO As Integer)

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CONTRATO", SqlDbType.Int, CONTRATO)
        BaseII.CreateMyParameter("@IMPORTEFORZOSO", System.Data.ParameterDirection.Output, SqlDbType.Money)
        BaseII.CreateMyParameter("@RES", System.Data.ParameterDirection.Output, SqlDbType.Int)
        BaseII.CreateMyParameter("@MSJ", System.Data.ParameterDirection.Output, SqlDbType.VarChar, 150)
        BaseII.ProcedimientoOutPut("CobraABajasClientesTV")

        Dim IMPORTE As String = ""
        Dim MENSAJE As String = ""
        IMPORTE = BaseII.dicoPar("@IMPORTEFORZOSO").ToString
        MENSAJE = BaseII.dicoPar("@MSJ").ToString

        TextBox2.Text = "Se hará un cargo de $ " + IMPORTE + " por concepto de Cobro de Adeudo. " + MENSAJE



    End Sub

End Class