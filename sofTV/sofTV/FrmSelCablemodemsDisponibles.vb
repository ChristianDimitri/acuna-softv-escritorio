Imports System.Data.SqlClient

Public Class FrmSelCablemodemsDisponibles

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        GloBndClv_CablemodemSel = False
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If IsNumeric(Me.ComboBox2.SelectedValue) = True Then
            GloClv_CablemodemSel = Me.ComboBox2.SelectedValue
            GloMacCablemodemSel = Me.ComboBox2.Text
            GloBndClv_CablemodemSel = True
        Else
            GloClv_CablemodemSel = 0
            GloMacCablemodemSel = ""
            GloBndClv_CablemodemSel = False
            If IdSistema <> "LO" And IdSistema <> "YU" Then
                MsgBox("No se Seleccionado un Cablemodem", MsgBoxStyle.Information)
            ElseIf IdSistema = "LO" Or IdSistema = "YU" Then
                MsgBox("No se Seleccionado un ATA", MsgBoxStyle.Information)
            End If
        End If
            Me.Close()
    End Sub

    Private Sub FrmSelCablemodemsDisponibles_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        colorea(Me, Me.Name)
        If GloClv_TipSer = 2 Then
            Me.MUESTRACABLEMODEMS_disponibles_2TableAdapter.Connection = CON
            Me.MUESTRACABLEMODEMS_disponibles_2TableAdapter.Fill(Me.DataSetEdgarRev2.MUESTRACABLEMODEMS_disponibles_2, 0, Locclv_tec)
        ElseIf GloClv_TipSer = 3 Then
            Me.MUESTRACABLEMODEMS_disponibles_2TableAdapter.Connection = CON
            Me.MUESTRACABLEMODEMS_disponibles_2TableAdapter.Fill(Me.DataSetEdgarRev2.MUESTRACABLEMODEMS_disponibles_2, 3, Locclv_tec)
        End If
        CON.Close()
    End Sub

    Private Sub ComboBox2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox2.SelectedIndexChanged
        If IsNumeric(Me.ComboBox2.SelectedValue) = True Then
            GloClv_CablemodemSel = Me.ComboBox2.SelectedValue
            GloMacCablemodemSel = Me.ComboBox2.Text
        End If
    End Sub


    Private Sub Label5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label5.Click

    End Sub
End Class