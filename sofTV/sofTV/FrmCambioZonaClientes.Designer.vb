﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmCambioZonaClientes
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.DatosGroupBox = New System.Windows.Forms.GroupBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.ColoniaCombo = New System.Windows.Forms.ComboBox()
        Me.CalleCombo = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.NombreText = New System.Windows.Forms.TextBox()
        Me.ContratoText = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Activo1Check = New System.Windows.Forms.CheckBox()
        Me.Zonas1Grid = New System.Windows.Forms.DataGridView()
        Me.Contrato = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Nombre = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Clv_Periodo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Zonas2Grid = New System.Windows.Forms.DataGridView()
        Me.Cont = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Nom = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Zon = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Activo2Check = New System.Windows.Forms.CheckBox()
        Me.ExitButton = New System.Windows.Forms.Button()
        Me.SearchButton = New System.Windows.Forms.Button()
        Me.FechasGroupBox = New System.Windows.Forms.GroupBox()
        Me.ActivoBajaCheck = New System.Windows.Forms.CheckBox()
        Me.ActivoDescCheck = New System.Windows.Forms.CheckBox()
        Me.ActivoInstCheck = New System.Windows.Forms.CheckBox()
        Me.ActivoContCheck = New System.Windows.Forms.CheckBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Baja2Picker = New System.Windows.Forms.DateTimePicker()
        Me.Baja1Picker = New System.Windows.Forms.DateTimePicker()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Desc2Picker = New System.Windows.Forms.DateTimePicker()
        Me.Desc1Picker = New System.Windows.Forms.DateTimePicker()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Inst2Picker = New System.Windows.Forms.DateTimePicker()
        Me.Inst1Picker = New System.Windows.Forms.DateTimePicker()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Cont2Picker = New System.Windows.Forms.DateTimePicker()
        Me.Cont1Picker = New System.Windows.Forms.DateTimePicker()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.ServicioGroupBox = New System.Windows.Forms.GroupBox()
        Me.DescTempCheck = New System.Windows.Forms.CheckBox()
        Me.FueraAreaCheck = New System.Windows.Forms.CheckBox()
        Me.SuspendidosCheck = New System.Windows.Forms.CheckBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TipoCobroCombo = New System.Windows.Forms.ComboBox()
        Me.ZonaCombo = New System.Windows.Forms.ComboBox()
        Me.TipSerCombo = New System.Windows.Forms.ComboBox()
        Me.ContratadosCheck = New System.Windows.Forms.CheckBox()
        Me.InstaladosCheck = New System.Windows.Forms.CheckBox()
        Me.BajaCheck = New System.Windows.Forms.CheckBox()
        Me.DesconectadosCheck = New System.Windows.Forms.CheckBox()
        Me.Activo3Check = New System.Windows.Forms.CheckBox()
        Me.AddOneButton = New System.Windows.Forms.Button()
        Me.AddAllButton = New System.Windows.Forms.Button()
        Me.DelOneButton = New System.Windows.Forms.Button()
        Me.DelAllButton = New System.Windows.Forms.Button()
        Me.CambiaZonaCombo = New System.Windows.Forms.ComboBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.AfectaButton = New System.Windows.Forms.Button()
        Me.ImpresionButton = New System.Windows.Forms.Button()
        Me.DatosGroupBox.SuspendLayout()
        CType(Me.Zonas1Grid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Zonas2Grid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.FechasGroupBox.SuspendLayout()
        Me.ServicioGroupBox.SuspendLayout()
        Me.SuspendLayout()
        '
        'DatosGroupBox
        '
        Me.DatosGroupBox.Controls.Add(Me.Label6)
        Me.DatosGroupBox.Controls.Add(Me.ColoniaCombo)
        Me.DatosGroupBox.Controls.Add(Me.CalleCombo)
        Me.DatosGroupBox.Controls.Add(Me.Label5)
        Me.DatosGroupBox.Controls.Add(Me.Label2)
        Me.DatosGroupBox.Controls.Add(Me.NombreText)
        Me.DatosGroupBox.Controls.Add(Me.ContratoText)
        Me.DatosGroupBox.Controls.Add(Me.Label1)
        Me.DatosGroupBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DatosGroupBox.Location = New System.Drawing.Point(4, 11)
        Me.DatosGroupBox.Name = "DatosGroupBox"
        Me.DatosGroupBox.Size = New System.Drawing.Size(283, 223)
        Me.DatosGroupBox.TabIndex = 0
        Me.DatosGroupBox.TabStop = False
        Me.DatosGroupBox.Text = "Buscar Cliente Por :"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(6, 168)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(69, 16)
        Me.Label6.TabIndex = 8
        Me.Label6.Text = "Colonia :"
        '
        'ColoniaCombo
        '
        Me.ColoniaCombo.DisplayMember = "Concepto"
        Me.ColoniaCombo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ColoniaCombo.FormattingEnabled = True
        Me.ColoniaCombo.Location = New System.Drawing.Point(6, 187)
        Me.ColoniaCombo.Name = "ColoniaCombo"
        Me.ColoniaCombo.Size = New System.Drawing.Size(271, 24)
        Me.ColoniaCombo.TabIndex = 7
        Me.ColoniaCombo.ValueMember = "Clv_Colonia"
        '
        'CalleCombo
        '
        Me.CalleCombo.DisplayMember = "Concepto"
        Me.CalleCombo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CalleCombo.FormattingEnabled = True
        Me.CalleCombo.Location = New System.Drawing.Point(6, 137)
        Me.CalleCombo.Name = "CalleCombo"
        Me.CalleCombo.Size = New System.Drawing.Size(271, 24)
        Me.CalleCombo.TabIndex = 6
        Me.CalleCombo.ValueMember = "Clv_Calle"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(6, 118)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(52, 16)
        Me.Label5.TabIndex = 5
        Me.Label5.Text = "Calle :"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(6, 70)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(71, 16)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Nombre :"
        '
        'NombreText
        '
        Me.NombreText.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NombreText.Location = New System.Drawing.Point(6, 89)
        Me.NombreText.Name = "NombreText"
        Me.NombreText.Size = New System.Drawing.Size(271, 22)
        Me.NombreText.TabIndex = 4
        '
        'ContratoText
        '
        Me.ContratoText.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ContratoText.Location = New System.Drawing.Point(6, 41)
        Me.ContratoText.Name = "ContratoText"
        Me.ContratoText.Size = New System.Drawing.Size(91, 22)
        Me.ContratoText.TabIndex = 4
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(6, 22)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(74, 16)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Contrato :"
        '
        'Activo1Check
        '
        Me.Activo1Check.AutoSize = True
        Me.Activo1Check.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Activo1Check.Location = New System.Drawing.Point(223, 3)
        Me.Activo1Check.Name = "Activo1Check"
        Me.Activo1Check.Size = New System.Drawing.Size(62, 17)
        Me.Activo1Check.TabIndex = 9
        Me.Activo1Check.Text = "Activo"
        Me.Activo1Check.UseVisualStyleBackColor = True
        '
        'Zonas1Grid
        '
        Me.Zonas1Grid.AllowUserToAddRows = False
        Me.Zonas1Grid.AllowUserToDeleteRows = False
        Me.Zonas1Grid.BackgroundColor = System.Drawing.Color.White
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Zonas1Grid.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle7
        Me.Zonas1Grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.Zonas1Grid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Contrato, Me.Nombre, Me.Clv_Periodo})
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Zonas1Grid.DefaultCellStyle = DataGridViewCellStyle8
        Me.Zonas1Grid.Location = New System.Drawing.Point(298, 12)
        Me.Zonas1Grid.Name = "Zonas1Grid"
        Me.Zonas1Grid.ReadOnly = True
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Zonas1Grid.RowHeadersDefaultCellStyle = DataGridViewCellStyle9
        Me.Zonas1Grid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.Zonas1Grid.Size = New System.Drawing.Size(594, 326)
        Me.Zonas1Grid.TabIndex = 1
        '
        'Contrato
        '
        Me.Contrato.DataPropertyName = "Contrato"
        Me.Contrato.HeaderText = "Contrato"
        Me.Contrato.Name = "Contrato"
        Me.Contrato.ReadOnly = True
        '
        'Nombre
        '
        Me.Nombre.DataPropertyName = "Nombre"
        Me.Nombre.HeaderText = "Nombre"
        Me.Nombre.Name = "Nombre"
        Me.Nombre.ReadOnly = True
        Me.Nombre.Width = 350
        '
        'Clv_Periodo
        '
        Me.Clv_Periodo.DataPropertyName = "Descripcion"
        Me.Clv_Periodo.HeaderText = "Zona"
        Me.Clv_Periodo.Name = "Clv_Periodo"
        Me.Clv_Periodo.ReadOnly = True
        '
        'Zonas2Grid
        '
        Me.Zonas2Grid.AllowUserToAddRows = False
        Me.Zonas2Grid.AllowUserToDeleteRows = False
        Me.Zonas2Grid.BackgroundColor = System.Drawing.Color.White
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Zonas2Grid.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle10
        Me.Zonas2Grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.Zonas2Grid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Cont, Me.Nom, Me.Zon})
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Zonas2Grid.DefaultCellStyle = DataGridViewCellStyle11
        Me.Zonas2Grid.Location = New System.Drawing.Point(298, 404)
        Me.Zonas2Grid.Name = "Zonas2Grid"
        Me.Zonas2Grid.ReadOnly = True
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Zonas2Grid.RowHeadersDefaultCellStyle = DataGridViewCellStyle12
        Me.Zonas2Grid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.Zonas2Grid.Size = New System.Drawing.Size(594, 326)
        Me.Zonas2Grid.TabIndex = 2
        '
        'Cont
        '
        Me.Cont.DataPropertyName = "Contrato"
        Me.Cont.HeaderText = "Contrato"
        Me.Cont.Name = "Cont"
        Me.Cont.ReadOnly = True
        '
        'Nom
        '
        Me.Nom.DataPropertyName = "Nombre"
        Me.Nom.HeaderText = "Nombre"
        Me.Nom.Name = "Nom"
        Me.Nom.ReadOnly = True
        Me.Nom.Width = 350
        '
        'Zon
        '
        Me.Zon.DataPropertyName = "Zona"
        Me.Zon.HeaderText = "Zona"
        Me.Zon.Name = "Zon"
        Me.Zon.ReadOnly = True
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.ImpresionButton)
        Me.Panel1.Controls.Add(Me.Activo2Check)
        Me.Panel1.Controls.Add(Me.Activo1Check)
        Me.Panel1.Controls.Add(Me.SearchButton)
        Me.Panel1.Controls.Add(Me.FechasGroupBox)
        Me.Panel1.Controls.Add(Me.ServicioGroupBox)
        Me.Panel1.Controls.Add(Me.DatosGroupBox)
        Me.Panel1.Controls.Add(Me.Activo3Check)
        Me.Panel1.Location = New System.Drawing.Point(2, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(290, 766)
        Me.Panel1.TabIndex = 5
        '
        'Activo2Check
        '
        Me.Activo2Check.AutoSize = True
        Me.Activo2Check.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Activo2Check.Location = New System.Drawing.Point(223, 239)
        Me.Activo2Check.Name = "Activo2Check"
        Me.Activo2Check.Size = New System.Drawing.Size(62, 17)
        Me.Activo2Check.TabIndex = 6
        Me.Activo2Check.Text = "Activo"
        Me.Activo2Check.UseVisualStyleBackColor = True
        '
        'ExitButton
        '
        Me.ExitButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ExitButton.Location = New System.Drawing.Point(754, 735)
        Me.ExitButton.Name = "ExitButton"
        Me.ExitButton.Size = New System.Drawing.Size(136, 29)
        Me.ExitButton.TabIndex = 9
        Me.ExitButton.Text = "&Salir"
        Me.ExitButton.UseVisualStyleBackColor = True
        '
        'SearchButton
        '
        Me.SearchButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SearchButton.Location = New System.Drawing.Point(4, 732)
        Me.SearchButton.Name = "SearchButton"
        Me.SearchButton.Size = New System.Drawing.Size(136, 29)
        Me.SearchButton.TabIndex = 8
        Me.SearchButton.Text = "&Buscar"
        Me.SearchButton.UseVisualStyleBackColor = True
        '
        'FechasGroupBox
        '
        Me.FechasGroupBox.Controls.Add(Me.ActivoBajaCheck)
        Me.FechasGroupBox.Controls.Add(Me.ActivoDescCheck)
        Me.FechasGroupBox.Controls.Add(Me.ActivoInstCheck)
        Me.FechasGroupBox.Controls.Add(Me.ActivoContCheck)
        Me.FechasGroupBox.Controls.Add(Me.Label16)
        Me.FechasGroupBox.Controls.Add(Me.Baja2Picker)
        Me.FechasGroupBox.Controls.Add(Me.Baja1Picker)
        Me.FechasGroupBox.Controls.Add(Me.Label15)
        Me.FechasGroupBox.Controls.Add(Me.Label12)
        Me.FechasGroupBox.Controls.Add(Me.Label11)
        Me.FechasGroupBox.Controls.Add(Me.Desc2Picker)
        Me.FechasGroupBox.Controls.Add(Me.Desc1Picker)
        Me.FechasGroupBox.Controls.Add(Me.Label10)
        Me.FechasGroupBox.Controls.Add(Me.Inst2Picker)
        Me.FechasGroupBox.Controls.Add(Me.Inst1Picker)
        Me.FechasGroupBox.Controls.Add(Me.Label9)
        Me.FechasGroupBox.Controls.Add(Me.Label8)
        Me.FechasGroupBox.Controls.Add(Me.Cont2Picker)
        Me.FechasGroupBox.Controls.Add(Me.Cont1Picker)
        Me.FechasGroupBox.Controls.Add(Me.Label7)
        Me.FechasGroupBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FechasGroupBox.Location = New System.Drawing.Point(4, 514)
        Me.FechasGroupBox.Name = "FechasGroupBox"
        Me.FechasGroupBox.Size = New System.Drawing.Size(283, 213)
        Me.FechasGroupBox.TabIndex = 7
        Me.FechasGroupBox.TabStop = False
        Me.FechasGroupBox.Text = "Fechas :"
        '
        'ActivoBajaCheck
        '
        Me.ActivoBajaCheck.AutoSize = True
        Me.ActivoBajaCheck.Location = New System.Drawing.Point(9, 169)
        Me.ActivoBajaCheck.Name = "ActivoBajaCheck"
        Me.ActivoBajaCheck.Size = New System.Drawing.Size(15, 14)
        Me.ActivoBajaCheck.TabIndex = 22
        Me.ActivoBajaCheck.UseVisualStyleBackColor = True
        '
        'ActivoDescCheck
        '
        Me.ActivoDescCheck.AutoSize = True
        Me.ActivoDescCheck.Location = New System.Drawing.Point(9, 124)
        Me.ActivoDescCheck.Name = "ActivoDescCheck"
        Me.ActivoDescCheck.Size = New System.Drawing.Size(15, 14)
        Me.ActivoDescCheck.TabIndex = 21
        Me.ActivoDescCheck.UseVisualStyleBackColor = True
        '
        'ActivoInstCheck
        '
        Me.ActivoInstCheck.AutoSize = True
        Me.ActivoInstCheck.Location = New System.Drawing.Point(9, 82)
        Me.ActivoInstCheck.Name = "ActivoInstCheck"
        Me.ActivoInstCheck.Size = New System.Drawing.Size(15, 14)
        Me.ActivoInstCheck.TabIndex = 20
        Me.ActivoInstCheck.UseVisualStyleBackColor = True
        '
        'ActivoContCheck
        '
        Me.ActivoContCheck.AutoSize = True
        Me.ActivoContCheck.Location = New System.Drawing.Point(9, 38)
        Me.ActivoContCheck.Name = "ActivoContCheck"
        Me.ActivoContCheck.Size = New System.Drawing.Size(15, 14)
        Me.ActivoContCheck.TabIndex = 19
        Me.ActivoContCheck.UseVisualStyleBackColor = True
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(132, 191)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(21, 16)
        Me.Label16.TabIndex = 18
        Me.Label16.Text = "al"
        '
        'Baja2Picker
        '
        Me.Baja2Picker.CustomFormat = "dd/MM/yyyy"
        Me.Baja2Picker.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Baja2Picker.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.Baja2Picker.Location = New System.Drawing.Point(158, 185)
        Me.Baja2Picker.Name = "Baja2Picker"
        Me.Baja2Picker.Size = New System.Drawing.Size(118, 22)
        Me.Baja2Picker.TabIndex = 17
        '
        'Baja1Picker
        '
        Me.Baja1Picker.CustomFormat = "dd/MM/yyyy"
        Me.Baja1Picker.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Baja1Picker.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.Baja1Picker.Location = New System.Drawing.Point(8, 185)
        Me.Baja1Picker.Name = "Baja1Picker"
        Me.Baja1Picker.Size = New System.Drawing.Size(118, 22)
        Me.Baja1Picker.TabIndex = 16
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(22, 168)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(95, 16)
        Me.Label15.TabIndex = 15
        Me.Label15.Text = "Fecha Baja :"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(133, 147)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(21, 16)
        Me.Label12.TabIndex = 13
        Me.Label12.Text = "al"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(133, 104)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(21, 16)
        Me.Label11.TabIndex = 12
        Me.Label11.Text = "al"
        '
        'Desc2Picker
        '
        Me.Desc2Picker.CalendarFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Desc2Picker.CustomFormat = "dd/MM/yyyy"
        Me.Desc2Picker.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Desc2Picker.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.Desc2Picker.Location = New System.Drawing.Point(158, 141)
        Me.Desc2Picker.Name = "Desc2Picker"
        Me.Desc2Picker.Size = New System.Drawing.Size(119, 22)
        Me.Desc2Picker.TabIndex = 11
        '
        'Desc1Picker
        '
        Me.Desc1Picker.CalendarFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Desc1Picker.CustomFormat = "dd/MM/yyyy"
        Me.Desc1Picker.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Desc1Picker.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.Desc1Picker.Location = New System.Drawing.Point(7, 141)
        Me.Desc1Picker.Name = "Desc1Picker"
        Me.Desc1Picker.Size = New System.Drawing.Size(120, 22)
        Me.Desc1Picker.TabIndex = 10
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(22, 124)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(153, 16)
        Me.Label10.TabIndex = 6
        Me.Label10.Text = "Fecha Desconexión :"
        '
        'Inst2Picker
        '
        Me.Inst2Picker.CustomFormat = "dd/MM/yyyy"
        Me.Inst2Picker.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Inst2Picker.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.Inst2Picker.Location = New System.Drawing.Point(158, 98)
        Me.Inst2Picker.Name = "Inst2Picker"
        Me.Inst2Picker.Size = New System.Drawing.Size(120, 22)
        Me.Inst2Picker.TabIndex = 9
        '
        'Inst1Picker
        '
        Me.Inst1Picker.CustomFormat = "dd/MM/yyyy"
        Me.Inst1Picker.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Inst1Picker.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.Inst1Picker.Location = New System.Drawing.Point(7, 98)
        Me.Inst1Picker.Name = "Inst1Picker"
        Me.Inst1Picker.Size = New System.Drawing.Size(120, 22)
        Me.Inst1Picker.TabIndex = 8
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(22, 82)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(138, 16)
        Me.Label9.TabIndex = 7
        Me.Label9.Text = "Fecha Instalación :"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(133, 59)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(21, 16)
        Me.Label8.TabIndex = 6
        Me.Label8.Text = "al"
        '
        'Cont2Picker
        '
        Me.Cont2Picker.CustomFormat = "dd/MM/yyyy"
        Me.Cont2Picker.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Cont2Picker.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.Cont2Picker.Location = New System.Drawing.Point(158, 54)
        Me.Cont2Picker.Name = "Cont2Picker"
        Me.Cont2Picker.Size = New System.Drawing.Size(119, 22)
        Me.Cont2Picker.TabIndex = 2
        '
        'Cont1Picker
        '
        Me.Cont1Picker.CustomFormat = "dd/MM/yyyy"
        Me.Cont1Picker.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Cont1Picker.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.Cont1Picker.Location = New System.Drawing.Point(7, 54)
        Me.Cont1Picker.Name = "Cont1Picker"
        Me.Cont1Picker.Size = New System.Drawing.Size(120, 22)
        Me.Cont1Picker.TabIndex = 1
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(22, 36)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(150, 16)
        Me.Label7.TabIndex = 0
        Me.Label7.Text = "Fecha Contratación :"
        '
        'ServicioGroupBox
        '
        Me.ServicioGroupBox.Controls.Add(Me.DescTempCheck)
        Me.ServicioGroupBox.Controls.Add(Me.FueraAreaCheck)
        Me.ServicioGroupBox.Controls.Add(Me.SuspendidosCheck)
        Me.ServicioGroupBox.Controls.Add(Me.Label4)
        Me.ServicioGroupBox.Controls.Add(Me.Label3)
        Me.ServicioGroupBox.Controls.Add(Me.TipoCobroCombo)
        Me.ServicioGroupBox.Controls.Add(Me.ZonaCombo)
        Me.ServicioGroupBox.Controls.Add(Me.TipSerCombo)
        Me.ServicioGroupBox.Controls.Add(Me.ContratadosCheck)
        Me.ServicioGroupBox.Controls.Add(Me.InstaladosCheck)
        Me.ServicioGroupBox.Controls.Add(Me.BajaCheck)
        Me.ServicioGroupBox.Controls.Add(Me.DesconectadosCheck)
        Me.ServicioGroupBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ServicioGroupBox.Location = New System.Drawing.Point(4, 247)
        Me.ServicioGroupBox.Name = "ServicioGroupBox"
        Me.ServicioGroupBox.Size = New System.Drawing.Size(283, 251)
        Me.ServicioGroupBox.TabIndex = 6
        Me.ServicioGroupBox.TabStop = False
        Me.ServicioGroupBox.Text = "Tipo Servicio :"
        '
        'DescTempCheck
        '
        Me.DescTempCheck.AutoSize = True
        Me.DescTempCheck.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DescTempCheck.Location = New System.Drawing.Point(6, 122)
        Me.DescTempCheck.Name = "DescTempCheck"
        Me.DescTempCheck.Size = New System.Drawing.Size(162, 16)
        Me.DescTempCheck.TabIndex = 16
        Me.DescTempCheck.Text = "Desconexiones Temporales"
        Me.DescTempCheck.UseVisualStyleBackColor = True
        '
        'FueraAreaCheck
        '
        Me.FueraAreaCheck.AutoSize = True
        Me.FueraAreaCheck.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FueraAreaCheck.Location = New System.Drawing.Point(172, 100)
        Me.FueraAreaCheck.Name = "FueraAreaCheck"
        Me.FueraAreaCheck.Size = New System.Drawing.Size(97, 16)
        Me.FueraAreaCheck.TabIndex = 15
        Me.FueraAreaCheck.Text = "Fuera De Área"
        Me.FueraAreaCheck.UseVisualStyleBackColor = True
        '
        'SuspendidosCheck
        '
        Me.SuspendidosCheck.AutoSize = True
        Me.SuspendidosCheck.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SuspendidosCheck.Location = New System.Drawing.Point(6, 100)
        Me.SuspendidosCheck.Name = "SuspendidosCheck"
        Me.SuspendidosCheck.Size = New System.Drawing.Size(88, 16)
        Me.SuspendidosCheck.TabIndex = 14
        Me.SuspendidosCheck.Text = "Suspendidos"
        Me.SuspendidosCheck.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(4, 200)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(51, 16)
        Me.Label4.TabIndex = 13
        Me.Label4.Text = "Zona :"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(4, 150)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(94, 16)
        Me.Label3.TabIndex = 12
        Me.Label3.Text = "Tipo Cobro :"
        '
        'TipoCobroCombo
        '
        Me.TipoCobroCombo.DisplayMember = "Concepto"
        Me.TipoCobroCombo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TipoCobroCombo.FormattingEnabled = True
        Me.TipoCobroCombo.Location = New System.Drawing.Point(6, 169)
        Me.TipoCobroCombo.Name = "TipoCobroCombo"
        Me.TipoCobroCombo.Size = New System.Drawing.Size(267, 24)
        Me.TipoCobroCombo.TabIndex = 7
        Me.TipoCobroCombo.ValueMember = "Clv_TipoCliente"
        '
        'ZonaCombo
        '
        Me.ZonaCombo.DisplayMember = "Concepto"
        Me.ZonaCombo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ZonaCombo.FormattingEnabled = True
        Me.ZonaCombo.Location = New System.Drawing.Point(6, 219)
        Me.ZonaCombo.Name = "ZonaCombo"
        Me.ZonaCombo.Size = New System.Drawing.Size(171, 24)
        Me.ZonaCombo.TabIndex = 8
        Me.ZonaCombo.ValueMember = "Clv_Periodo"
        '
        'TipSerCombo
        '
        Me.TipSerCombo.DisplayMember = "Concepto"
        Me.TipSerCombo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TipSerCombo.FormattingEnabled = True
        Me.TipSerCombo.Location = New System.Drawing.Point(6, 44)
        Me.TipSerCombo.Name = "TipSerCombo"
        Me.TipSerCombo.Size = New System.Drawing.Size(268, 24)
        Me.TipSerCombo.TabIndex = 7
        Me.TipSerCombo.ValueMember = "Clv_TipSer"
        '
        'ContratadosCheck
        '
        Me.ContratadosCheck.AutoSize = True
        Me.ContratadosCheck.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ContratadosCheck.Location = New System.Drawing.Point(6, 78)
        Me.ContratadosCheck.Name = "ContratadosCheck"
        Me.ContratadosCheck.Size = New System.Drawing.Size(86, 16)
        Me.ContratadosCheck.TabIndex = 8
        Me.ContratadosCheck.Text = "Contratados"
        Me.ContratadosCheck.UseVisualStyleBackColor = True
        '
        'InstaladosCheck
        '
        Me.InstaladosCheck.AutoSize = True
        Me.InstaladosCheck.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.InstaladosCheck.Location = New System.Drawing.Point(95, 78)
        Me.InstaladosCheck.Name = "InstaladosCheck"
        Me.InstaladosCheck.Size = New System.Drawing.Size(77, 16)
        Me.InstaladosCheck.TabIndex = 9
        Me.InstaladosCheck.Text = "Instalados"
        Me.InstaladosCheck.UseVisualStyleBackColor = True
        '
        'BajaCheck
        '
        Me.BajaCheck.AutoSize = True
        Me.BajaCheck.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BajaCheck.Location = New System.Drawing.Point(95, 100)
        Me.BajaCheck.Name = "BajaCheck"
        Me.BajaCheck.Size = New System.Drawing.Size(46, 16)
        Me.BajaCheck.TabIndex = 10
        Me.BajaCheck.Text = "Baja"
        Me.BajaCheck.UseVisualStyleBackColor = True
        '
        'DesconectadosCheck
        '
        Me.DesconectadosCheck.AutoSize = True
        Me.DesconectadosCheck.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DesconectadosCheck.Location = New System.Drawing.Point(172, 78)
        Me.DesconectadosCheck.Name = "DesconectadosCheck"
        Me.DesconectadosCheck.Size = New System.Drawing.Size(102, 16)
        Me.DesconectadosCheck.TabIndex = 11
        Me.DesconectadosCheck.Text = "Desconectados"
        Me.DesconectadosCheck.UseVisualStyleBackColor = True
        '
        'Activo3Check
        '
        Me.Activo3Check.AutoSize = True
        Me.Activo3Check.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Activo3Check.Location = New System.Drawing.Point(220, 499)
        Me.Activo3Check.Name = "Activo3Check"
        Me.Activo3Check.Size = New System.Drawing.Size(62, 17)
        Me.Activo3Check.TabIndex = 6
        Me.Activo3Check.Text = "Activo"
        Me.Activo3Check.UseVisualStyleBackColor = True
        '
        'AddOneButton
        '
        Me.AddOneButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AddOneButton.Location = New System.Drawing.Point(526, 341)
        Me.AddOneButton.Name = "AddOneButton"
        Me.AddOneButton.Size = New System.Drawing.Size(179, 29)
        Me.AddOneButton.TabIndex = 6
        Me.AddOneButton.Text = "&Agregar Uno A La Lista"
        Me.AddOneButton.UseVisualStyleBackColor = True
        '
        'AddAllButton
        '
        Me.AddAllButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AddAllButton.Location = New System.Drawing.Point(526, 372)
        Me.AddAllButton.Name = "AddAllButton"
        Me.AddAllButton.Size = New System.Drawing.Size(179, 30)
        Me.AddAllButton.TabIndex = 7
        Me.AddAllButton.Text = "&Agregar Todos A La Lista"
        Me.AddAllButton.UseVisualStyleBackColor = True
        '
        'DelOneButton
        '
        Me.DelOneButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DelOneButton.Location = New System.Drawing.Point(711, 340)
        Me.DelOneButton.Name = "DelOneButton"
        Me.DelOneButton.Size = New System.Drawing.Size(179, 30)
        Me.DelOneButton.TabIndex = 8
        Me.DelOneButton.Text = "&Quitar Uno De La Lista"
        Me.DelOneButton.UseVisualStyleBackColor = True
        '
        'DelAllButton
        '
        Me.DelAllButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DelAllButton.Location = New System.Drawing.Point(711, 372)
        Me.DelAllButton.Name = "DelAllButton"
        Me.DelAllButton.Size = New System.Drawing.Size(179, 29)
        Me.DelAllButton.TabIndex = 9
        Me.DelAllButton.Text = "&Quitar Todos De La Lista"
        Me.DelAllButton.UseVisualStyleBackColor = True
        '
        'CambiaZonaCombo
        '
        Me.CambiaZonaCombo.DisplayMember = "Concepto"
        Me.CambiaZonaCombo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CambiaZonaCombo.FormattingEnabled = True
        Me.CambiaZonaCombo.Location = New System.Drawing.Point(301, 369)
        Me.CambiaZonaCombo.Name = "CambiaZonaCombo"
        Me.CambiaZonaCombo.Size = New System.Drawing.Size(205, 23)
        Me.CambiaZonaCombo.TabIndex = 10
        Me.CambiaZonaCombo.ValueMember = "Clv_Periodo"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(298, 348)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(208, 15)
        Me.Label13.TabIndex = 11
        Me.Label13.Text = "Seleccione la Zona a Cambiar :"
        '
        'AfectaButton
        '
        Me.AfectaButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AfectaButton.Location = New System.Drawing.Point(610, 735)
        Me.AfectaButton.Name = "AfectaButton"
        Me.AfectaButton.Size = New System.Drawing.Size(138, 29)
        Me.AfectaButton.TabIndex = 12
        Me.AfectaButton.Text = "&Afecta Clientes"
        Me.AfectaButton.UseVisualStyleBackColor = True
        '
        'ImpresionButton
        '
        Me.ImpresionButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ImpresionButton.Location = New System.Drawing.Point(153, 732)
        Me.ImpresionButton.Name = "ImpresionButton"
        Me.ImpresionButton.Size = New System.Drawing.Size(128, 28)
        Me.ImpresionButton.TabIndex = 13
        Me.ImpresionButton.Text = "&Imp Reporte"
        Me.ImpresionButton.UseVisualStyleBackColor = True
        '
        'FrmCambioZonaClientes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(902, 769)
        Me.Controls.Add(Me.AfectaButton)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.CambiaZonaCombo)
        Me.Controls.Add(Me.ExitButton)
        Me.Controls.Add(Me.DelAllButton)
        Me.Controls.Add(Me.DelOneButton)
        Me.Controls.Add(Me.AddAllButton)
        Me.Controls.Add(Me.AddOneButton)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Zonas2Grid)
        Me.Controls.Add(Me.Zonas1Grid)
        Me.Name = "FrmCambioZonaClientes"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Cambio de Zona"
        Me.DatosGroupBox.ResumeLayout(False)
        Me.DatosGroupBox.PerformLayout()
        CType(Me.Zonas1Grid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Zonas2Grid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.FechasGroupBox.ResumeLayout(False)
        Me.FechasGroupBox.PerformLayout()
        Me.ServicioGroupBox.ResumeLayout(False)
        Me.ServicioGroupBox.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DatosGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents Zonas1Grid As System.Windows.Forms.DataGridView
    Friend WithEvents Zonas2Grid As System.Windows.Forms.DataGridView
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents NombreText As System.Windows.Forms.TextBox
    Friend WithEvents ContratoText As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents ServicioGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents TipoCobroCombo As System.Windows.Forms.ComboBox
    Friend WithEvents ZonaCombo As System.Windows.Forms.ComboBox
    Friend WithEvents TipSerCombo As System.Windows.Forms.ComboBox
    Friend WithEvents ContratadosCheck As System.Windows.Forms.CheckBox
    Friend WithEvents InstaladosCheck As System.Windows.Forms.CheckBox
    Friend WithEvents BajaCheck As System.Windows.Forms.CheckBox
    Friend WithEvents DesconectadosCheck As System.Windows.Forms.CheckBox
    Friend WithEvents DescTempCheck As System.Windows.Forms.CheckBox
    Friend WithEvents FueraAreaCheck As System.Windows.Forms.CheckBox
    Friend WithEvents SuspendidosCheck As System.Windows.Forms.CheckBox
    Friend WithEvents Activo1Check As System.Windows.Forms.CheckBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents ColoniaCombo As System.Windows.Forms.ComboBox
    Friend WithEvents CalleCombo As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents ExitButton As System.Windows.Forms.Button
    Friend WithEvents SearchButton As System.Windows.Forms.Button
    Friend WithEvents FechasGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents Activo3Check As System.Windows.Forms.CheckBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Desc2Picker As System.Windows.Forms.DateTimePicker
    Friend WithEvents Desc1Picker As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Inst2Picker As System.Windows.Forms.DateTimePicker
    Friend WithEvents Inst1Picker As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Cont2Picker As System.Windows.Forms.DateTimePicker
    Friend WithEvents Cont1Picker As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Activo2Check As System.Windows.Forms.CheckBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Baja2Picker As System.Windows.Forms.DateTimePicker
    Friend WithEvents Baja1Picker As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents ActivoBajaCheck As System.Windows.Forms.CheckBox
    Friend WithEvents ActivoDescCheck As System.Windows.Forms.CheckBox
    Friend WithEvents ActivoInstCheck As System.Windows.Forms.CheckBox
    Friend WithEvents ActivoContCheck As System.Windows.Forms.CheckBox
    Friend WithEvents Contrato As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Nombre As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Clv_Periodo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AddOneButton As System.Windows.Forms.Button
    Friend WithEvents AddAllButton As System.Windows.Forms.Button
    Friend WithEvents DelOneButton As System.Windows.Forms.Button
    Friend WithEvents DelAllButton As System.Windows.Forms.Button
    Friend WithEvents CambiaZonaCombo As System.Windows.Forms.ComboBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Cont As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Nom As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Zon As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ImpresionButton As System.Windows.Forms.Button
    Friend WithEvents AfectaButton As System.Windows.Forms.Button
End Class
