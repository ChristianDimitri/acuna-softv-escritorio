﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmCargosAdministrativos
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cmbxTipSer = New System.Windows.Forms.ComboBox()
        Me.cmbxServicio = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.btnAgregar = New System.Windows.Forms.Button()
        Me.txtCosto = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.btnModificar = New System.Windows.Forms.Button()
        Me.dgvRelacionServiciosCargoAdmin = New System.Windows.Forms.DataGridView()
        Me.lblMensajeServicio = New System.Windows.Forms.Label()
        Me.lblMensajeCosto = New System.Windows.Forms.Label()
        Me.lblAyudaSelecciona = New System.Windows.Forms.Label()
        Me.lblAyuda02 = New System.Windows.Forms.Label()
        CType(Me.dgvRelacionServiciosCargoAdmin, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(268, -1)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(405, 23)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Asignación de Cargos Administrativos por Servicio"
        '
        'cmbxTipSer
        '
        Me.cmbxTipSer.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbxTipSer.FormattingEnabled = True
        Me.cmbxTipSer.Location = New System.Drawing.Point(59, 93)
        Me.cmbxTipSer.Name = "cmbxTipSer"
        Me.cmbxTipSer.Size = New System.Drawing.Size(423, 27)
        Me.cmbxTipSer.TabIndex = 1
        '
        'cmbxServicio
        '
        Me.cmbxServicio.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbxServicio.FormattingEnabled = True
        Me.cmbxServicio.Location = New System.Drawing.Point(58, 183)
        Me.cmbxServicio.Name = "cmbxServicio"
        Me.cmbxServicio.Size = New System.Drawing.Size(485, 27)
        Me.cmbxServicio.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(56, 72)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(198, 18)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Selecciona un Tipo de Servicio:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(55, 162)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(149, 18)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Selecciona un Servicio:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(56, 259)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(305, 18)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Relación de Cargos Administrativos por Servicio:"
        '
        'btnSalir
        '
        Me.btnSalir.BackColor = System.Drawing.Color.DarkOrange
        Me.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSalir.ForeColor = System.Drawing.Color.Black
        Me.btnSalir.Location = New System.Drawing.Point(765, 518)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(155, 36)
        Me.btnSalir.TabIndex = 40
        Me.btnSalir.Text = "&SALIR"
        Me.btnSalir.UseVisualStyleBackColor = False
        '
        'btnEliminar
        '
        Me.btnEliminar.BackColor = System.Drawing.Color.DarkOrange
        Me.btnEliminar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEliminar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEliminar.ForeColor = System.Drawing.Color.Black
        Me.btnEliminar.Location = New System.Drawing.Point(765, 387)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(155, 36)
        Me.btnEliminar.TabIndex = 39
        Me.btnEliminar.Text = "&ELIMINAR"
        Me.btnEliminar.UseVisualStyleBackColor = False
        '
        'btnAgregar
        '
        Me.btnAgregar.BackColor = System.Drawing.Color.DarkOrange
        Me.btnAgregar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAgregar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAgregar.ForeColor = System.Drawing.Color.Black
        Me.btnAgregar.Location = New System.Drawing.Point(765, 279)
        Me.btnAgregar.Name = "btnAgregar"
        Me.btnAgregar.Size = New System.Drawing.Size(155, 36)
        Me.btnAgregar.TabIndex = 38
        Me.btnAgregar.Text = "&AGREGAR"
        Me.btnAgregar.UseVisualStyleBackColor = False
        '
        'txtCosto
        '
        Me.txtCosto.Font = New System.Drawing.Font("Calibri", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCosto.Location = New System.Drawing.Point(639, 177)
        Me.txtCosto.Name = "txtCosto"
        Me.txtCosto.Size = New System.Drawing.Size(253, 33)
        Me.txtCosto.TabIndex = 41
        Me.txtCosto.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(636, 156)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(111, 18)
        Me.Label5.TabIndex = 42
        Me.Label5.Text = "Precio Asignado:"
        '
        'btnModificar
        '
        Me.btnModificar.BackColor = System.Drawing.Color.DarkOrange
        Me.btnModificar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnModificar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnModificar.ForeColor = System.Drawing.Color.Black
        Me.btnModificar.Location = New System.Drawing.Point(765, 332)
        Me.btnModificar.Name = "btnModificar"
        Me.btnModificar.Size = New System.Drawing.Size(155, 38)
        Me.btnModificar.TabIndex = 43
        Me.btnModificar.Text = "&MODIFICAR"
        Me.btnModificar.UseVisualStyleBackColor = False
        '
        'dgvRelacionServiciosCargoAdmin
        '
        Me.dgvRelacionServiciosCargoAdmin.AllowUserToAddRows = False
        Me.dgvRelacionServiciosCargoAdmin.AllowUserToDeleteRows = False
        Me.dgvRelacionServiciosCargoAdmin.AllowUserToResizeColumns = False
        Me.dgvRelacionServiciosCargoAdmin.AllowUserToResizeRows = False
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvRelacionServiciosCargoAdmin.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle7
        Me.dgvRelacionServiciosCargoAdmin.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvRelacionServiciosCargoAdmin.DefaultCellStyle = DataGridViewCellStyle8
        Me.dgvRelacionServiciosCargoAdmin.Location = New System.Drawing.Point(59, 280)
        Me.dgvRelacionServiciosCargoAdmin.Name = "dgvRelacionServiciosCargoAdmin"
        Me.dgvRelacionServiciosCargoAdmin.ReadOnly = True
        Me.dgvRelacionServiciosCargoAdmin.Size = New System.Drawing.Size(696, 275)
        Me.dgvRelacionServiciosCargoAdmin.TabIndex = 44
        '
        'lblMensajeServicio
        '
        Me.lblMensajeServicio.AutoSize = True
        Me.lblMensajeServicio.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMensajeServicio.Location = New System.Drawing.Point(212, 164)
        Me.lblMensajeServicio.Name = "lblMensajeServicio"
        Me.lblMensajeServicio.Size = New System.Drawing.Size(322, 15)
        Me.lblMensajeServicio.TabIndex = 45
        Me.lblMensajeServicio.Text = "( Este servicio aún no tiene costo por Cargo Administrativo )"
        '
        'lblMensajeCosto
        '
        Me.lblMensajeCosto.AutoSize = True
        Me.lblMensajeCosto.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMensajeCosto.Location = New System.Drawing.Point(636, 213)
        Me.lblMensajeCosto.Name = "lblMensajeCosto"
        Me.lblMensajeCosto.Size = New System.Drawing.Size(265, 15)
        Me.lblMensajeCosto.TabIndex = 46
        Me.lblMensajeCosto.Text = "( Coloca un precio para el Cargo Administrativo )"
        '
        'lblAyudaSelecciona
        '
        Me.lblAyudaSelecciona.AutoSize = True
        Me.lblAyudaSelecciona.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAyudaSelecciona.Location = New System.Drawing.Point(56, 558)
        Me.lblAyudaSelecciona.Name = "lblAyudaSelecciona"
        Me.lblAyudaSelecciona.Size = New System.Drawing.Size(383, 15)
        Me.lblAyudaSelecciona.TabIndex = 47
        Me.lblAyudaSelecciona.Text = "Selecciona un Concepto de la lista para modificar su tarifa o Eliminarlo."
        '
        'lblAyuda02
        '
        Me.lblAyuda02.AutoSize = True
        Me.lblAyuda02.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAyuda02.Location = New System.Drawing.Point(55, 213)
        Me.lblAyuda02.Name = "lblAyuda02"
        Me.lblAyuda02.Size = New System.Drawing.Size(488, 15)
        Me.lblAyuda02.TabIndex = 48
        Me.lblAyuda02.Text = "Selecciona un Servicio y asigna un Precio si quieres agregar un nuevo Cargo Admin" & _
            "istrativo"
        '
        'FrmCargosAdministrativos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(935, 584)
        Me.Controls.Add(Me.lblAyuda02)
        Me.Controls.Add(Me.lblAyudaSelecciona)
        Me.Controls.Add(Me.lblMensajeCosto)
        Me.Controls.Add(Me.lblMensajeServicio)
        Me.Controls.Add(Me.dgvRelacionServiciosCargoAdmin)
        Me.Controls.Add(Me.btnModificar)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.txtCosto)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnEliminar)
        Me.Controls.Add(Me.btnAgregar)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.cmbxServicio)
        Me.Controls.Add(Me.cmbxTipSer)
        Me.Controls.Add(Me.Label1)
        Me.MaximizeBox = False
        Me.Name = "FrmCargosAdministrativos"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Cargos Administrativos Por Servicio"
        CType(Me.dgvRelacionServiciosCargoAdmin, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cmbxTipSer As System.Windows.Forms.ComboBox
    Friend WithEvents cmbxServicio As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents btnAgregar As System.Windows.Forms.Button
    Friend WithEvents txtCosto As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents btnModificar As System.Windows.Forms.Button
    Friend WithEvents dgvRelacionServiciosCargoAdmin As System.Windows.Forms.DataGridView
    Friend WithEvents lblMensajeServicio As System.Windows.Forms.Label
    Friend WithEvents lblMensajeCosto As System.Windows.Forms.Label
    Friend WithEvents lblAyudaSelecciona As System.Windows.Forms.Label
    Friend WithEvents lblAyuda02 As System.Windows.Forms.Label
End Class
