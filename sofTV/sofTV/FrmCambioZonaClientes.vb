﻿Imports System.Data.SqlClient
Imports System.Text
Imports System.Collections.Generic
Imports Microsoft.VisualBasic

Public Class FrmCambioZonaClientes
    'SE DECLARAN LOS DICCIONARIOS QUE VAMOS A UTILIZAR
    Dim sel As New Dictionary(Of String, Consulta)
    Dim frm As New Dictionary(Of String, Consulta)
    Dim whe As New Dictionary(Of String, Consulta)
    Dim group As New Dictionary(Of String, Consulta)
    Public Consulta As String = Nothing

    Private Sub MuestraCallesZonas()

        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder

        strSQL.Append("EXEC MuestraCallesZonas ")

        Dim dataTable As New DataTable
        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim bindingSource As New BindingSource

        Try
            conexion.Open()
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            CalleCombo.DataSource = bindingSource.DataSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub
    Private Sub MuestraColoniasZonas(ByVal Clv_Calle As Integer)

        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder

        strSQL.Append("EXEC MuestraColoniasZonas ")
        strSQL.Append(CStr(Clv_Calle))

        Dim dataTable As New DataTable
        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim bindingSource As New BindingSource

        Try
            conexion.Open()
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            ColoniaCombo.DataSource = bindingSource.DataSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub
    Private Sub MuestraTipSerPrincipal3()

        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder

        strSQL.Append("EXEC MuestraTipSerPrincipal3 ")

        Dim dataTable As New DataTable
        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim bindingSource As New BindingSource

        Try
            conexion.Open()
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            TipSerCombo.DataSource = bindingSource.DataSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub
    Private Sub MuestraTiposClientes()

        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder

        strSQL.Append("EXEC MuestraTiposClientes ")

        Dim dataTable As New DataTable
        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim bindingSource As New BindingSource

        Try
            conexion.Open()
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            TipoCobroCombo.DataSource = bindingSource.DataSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub
    Private Sub MuestraZona(ByVal Op As Integer)

        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder

        strSQL.Append("EXEC MuestraZona ")
        strSQL.Append(CStr(Op))

        Dim dataTable As New DataTable
        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim bindingSource As New BindingSource

        Try
            conexion.Open()
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            ZonaCombo.DataSource = bindingSource.DataSource
            CambiaZonaCombo.DataSource = bindingSource.DataSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub FrmCambioZonaClientes_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        GeneraDiccionario()
        If Me.Activo1Check.Checked = False Then
            DatosGroupBox.Enabled = False
        Else
            DatosGroupBox.Enabled = True
        End If
        If Me.Activo2Check.Checked = False Then
            ServicioGroupBox.Enabled = False
        Else
            ServicioGroupBox.Enabled = True
        End If
        If Me.Activo3Check.Checked = False Then
            FechasGroupBox.Enabled = False
        Else
            FechasGroupBox.Enabled = True
        End If
    End Sub

    Private Sub Activo1Check_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Activo1Check.CheckedChanged
        If Me.Activo1Check.Checked = False Then
            DatosGroupBox.Enabled = False
        Else
            DatosGroupBox.Enabled = True
            MuestraCallesZonas()
            MuestraColoniasZonas(CalleCombo.SelectedValue)
        End If
    End Sub

    Private Sub Activo2Check_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Activo2Check.CheckedChanged
        If Me.Activo2Check.Checked = False Then
            ServicioGroupBox.Enabled = False
        Else
            ServicioGroupBox.Enabled = True
            MuestraTipSerPrincipal3()
            MuestraZona(1)
            MuestraTiposClientes()
        End If
    End Sub

    Private Sub Activo3Check_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Activo3Check.CheckedChanged
        If Me.Activo3Check.Checked = False Then
            FechasGroupBox.Enabled = False
        Else
            FechasGroupBox.Enabled = True
            Me.Desc1Picker.Enabled = False
            Me.Desc2Picker.Enabled = False
            Me.Inst1Picker.Enabled = False
            Me.Inst2Picker.Enabled = False
            Me.Cont1Picker.Enabled = False
            Me.Cont2Picker.Enabled = False
            Me.Baja1Picker.Enabled = False
            Me.Baja2Picker.Enabled = False
        End If
    End Sub

    Private Sub ExitButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitButton.Click
        Me.Close()
    End Sub

    'SE CREAN LOS DICCIONARIOS A UTILIZAR DURANTE EL PROCESO DE GENERACIÓN DE LA CONSULTA
    Private Sub GeneraDiccionario()
        sel.Add("select", New Consulta("SELECT A.CONTRATO, A.NOMBRE, H.DESCRIPCION"))

        frm.Add("from", New Consulta(" FROM CLIENTES A INNER JOIN ClientesTV B ON A.CONTRATO=B.CONTRATO " +
                                        "INNER JOIN CLIENTESNET I ON A.CONTRATO =I.Contrato " +
                                        "INNER JOIN ContNet C ON C.CONTRATONET=I.CONTRATONET " +
                                        "INNER JOIN CALLES D ON D.Clv_Calle = A.Clv_Calle " +
                                        "INNER JOIN COLONIAS E ON E.clv_colonia = A.Clv_Colonia " +
                                        "INNER JOIN Servicios F ON F.Clv_Servicio = B.Clv_TipoServicioTV OR F.Clv_Servicio = C.Clv_Servicio " +
                                        "INNER JOIN Rel_Clientes_TiposClientes G ON A.CONTRATO = G.CONTRATO " +
                                        "INNER JOIN CatalogoPeriodosCorte H ON A.Clv_Periodo = H.Clv_Periodo " +
                                        "WHERE "))

        whe.Add("contrato", New Consulta("A.Contrato"))
        whe.Add("nombre", New Consulta("A.Nombre"))
        whe.Add("calle", New Consulta("D.Clv_Calle"))
        whe.Add("colonia", New Consulta("E.Clv_Colonia"))


        whe.Add("TipServ", New Consulta("F.Clv_TipServ"))
        whe.Add("status", New Consulta("Status"))
        whe.Add("TipoCobro", New Consulta("G.Clv_TipoCliente"))
        whe.Add("Zona", New Consulta("A.Clv_Periodo"))

        whe.Add("FechaCont", New Consulta("Fecha_Solicitud"))
        whe.Add("FechaInst", New Consulta("Fecha_Inst"))
        whe.Add("FechaInstalacio", New Consulta("Fecha_Instalacio"))
        whe.Add("FechaDesc", New Consulta("Fecha_Corte"))
        whe.Add("FechaSusp", New Consulta("Fecha_Suspension"))
        whe.Add("FechaCancelacio", New Consulta("Fecha_Cancelacio"))
        whe.Add("FechaBaja", New Consulta("Fecha_Baja"))

        group.Add("Group", New Consulta("GROUP BY A.CONTRATO,A.NOMBRE,H.DESCRIPCION"))
    End Sub

    Private Function GeneraConsulta() As String
        Dim sb As New StringBuilder()
        Dim sb2 As New StringBuilder()
        Dim sbSelect As New StringBuilder()
        Dim sbFrom As New StringBuilder()
        Dim sbGroup As New StringBuilder()

        sbSelect.Append(sel("select").valor)

        sbFrom.Append(frm("from").valor)

        If (Activo1Check.Checked) Then
            sb.Append(IIf(ContratoText.Text.Trim().Length > 0, (IIf(sb.ToString().Trim().Length > 0, " AND ", "")) + whe("contrato").valor + " = " + ContratoText.Text.Trim(), ""))
            sb.Append(IIf(NombreText.Text.Trim().Length > 0, (IIf(sb.ToString().Trim().Length > 0, " AND ", "")) + whe("nombre").valor + " = '" + NombreText.Text.Trim() + "'", ""))
            sb.Append(IIf(CalleCombo.SelectedValue = "0", "", (IIf(sb.ToString().Trim().Length > 0, " AND ", "")) + whe("calle").valor + " = " + CalleCombo.SelectedValue.ToString()))
            sb.Append(IIf(ColoniaCombo.SelectedValue = "0", "", (IIf(sb.ToString().Trim().Length > 0, " AND ", "")) + whe("colonia").valor + " = " + ColoniaCombo.SelectedValue.ToString()))
        End If

        If (Activo2Check.Checked) Then
            sb.Append(IIf(TipSerCombo.SelectedValue = "0", "", (IIf(sb.ToString().Trim().Length > 0, " AND ", "")) + whe("TipoCobro").valor + " = " + TipSerCombo.SelectedValue.ToString()))

            'If (TipSerCombo.SelectedValue) <> 0 Then
            sb2.Append(IIf(Not ContratadosCheck.Checked, "", (IIf(sb2.ToString().Trim().Length > 0, " , ", "")) + "'C'"))
            sb2.Append(IIf(Not InstaladosCheck.Checked, "", (IIf(sb2.ToString().Trim().Length > 0, " , ", "")) + "'I'"))
            sb2.Append(IIf(Not DesconectadosCheck.Checked, "", (IIf(sb2.ToString().Trim().Length > 0, " , ", "")) + "'D'"))
            sb2.Append(IIf(Not SuspendidosCheck.Checked, "", (IIf(sb2.ToString().Trim().Length > 0, " , ", "")) + "'S'"))
            sb2.Append(IIf(Not BajaCheck.Checked, "", (IIf(sb2.ToString().Trim().Length > 0, " , ", "")) + "'B'"))
            sb2.Append(IIf(Not FueraAreaCheck.Checked, "", (IIf(sb2.ToString().Trim().Length > 0, " , ", "")) + "'F'"))
            sb2.Append(IIf(Not DescTempCheck.Checked, "", (IIf(sb2.ToString().Trim().Length > 0, " , ", "")) + "'T'"))

            sb.Append(IIf(sb2.ToString().Length <= 0, "",
                          (IIf(sb.ToString().Trim().Length > 0, " AND ", "")) +
                          IIf(TipSerCombo.SelectedValue = 1, "B." + whe("status").valor + " IN (" + sb2.ToString() + ")",
                              IIf(TipSerCombo.SelectedValue = 2, "C." + whe("status").valor + " IN (" + sb2.ToString() + ")",
                                  "B." + whe("status").valor + " IN (" + sb2.ToString() + ")") + "AND" + "C." + whe("status").valor + " IN (" + sb2.ToString() + ")")))
            'End If

            If TipoCobroCombo.SelectedValue <> 0 Then
                sb.Append(IIf(TipoCobroCombo.SelectedValue = "0", "", (IIf(sb.ToString().Trim().Length > 0, " AND ", "")) + whe("TipoCobro").valor + " = " + TipoCobroCombo.SelectedValue.ToString()))
            End If

            sb.Append(IIf(ZonaCombo.SelectedValue = "0", "", (IIf(sb.ToString().Trim().Length > 0, " AND ", "")) + whe("Zona").valor + " = " + ZonaCombo.SelectedValue.ToString()))
        End If

        If (Activo3Check.Checked) Then
            If (ActivoContCheck.Checked) Then
                sb.Append((IIf(sb.ToString().Trim().Length > 0, " AND ", "")) +
                            IIf(TipSerCombo.SelectedValue = 1, "B." + whe("FechaCont").valor + " BETWEEN '" + Format(Cont1Picker.Value, "yyyyMMdd") + "' AND '" + Format(Cont2Picker.Value, "yyyyMMdd") + "'",
                                IIf(TipSerCombo.SelectedValue = 2, "C." + whe("FechaCont").valor + " BETWEEN '" + Format(Cont1Picker.Value, "yyyyMMdd") + "' AND '" + Format(Cont2Picker.Value, "yyyyMMdd") + "'",
                                    "B." + whe("FechaCont").valor + " BETWEEN '" + Format(Cont1Picker.Value, "yyyyMMdd") + "' AND '" + Format(Cont2Picker.Value, "yyyyMMdd") + "' AND " + "C." + whe("FechaCont").valor + " BETWEEN '" + Format(Cont1Picker.Value, "yyyyMMdd") + "' AND '" + Format(Cont2Picker.Value, "yyyyMMdd") + "'")))
            End If
            If (ActivoInstCheck.Checked) Then
                sb.Append((IIf(sb.ToString().Trim().Length > 0, " AND ", "")) +
                            IIf(TipSerCombo.SelectedValue = 1, "B." + whe("FechaInst").valor + " BETWEEN '" + Format(Inst1Picker.Value, "yyyyMMdd") + "' AND '" + Format(Inst2Picker.Value, "yyyyMMdd") + "'",
                                IIf(TipSerCombo.SelectedValue = 2, "C." + whe("FechaInstalacio").valor + " BETWEEN '" + Format(Inst1Picker.Value, "yyyyMMdd") + "' AND '" + Format(Inst2Picker.Value, "yyyyMMdd") + "'",
                                    "B." + whe("FechaInst").valor + " BETWEEN '" + Format(Inst1Picker.Value, "yyyyMMdd") + "' AND '" + Format(Inst2Picker.Value, "yyyyMMdd") + "' AND " + "C." + whe("FechaInstalacio").valor + " BETWEEN '" + Format(Inst1Picker.Value, "yyyyMMdd") + "' AND '" + Format(Inst2Picker.Value, "yyyyMMdd") + "'")))
            End If
            If (ActivoDescCheck.Checked) Then
                sb.Append((IIf(sb.ToString().Trim().Length > 0, " AND ", "")) +
                            IIf(TipSerCombo.SelectedValue = 1, "B." + whe("FechaDesc").valor + " BETWEEN '" + Format(Desc1Picker.Value, "yyyyMMdd") + "' AND '" + Format(Desc2Picker.Value, "yyyyMMdd") + "'",
                                IIf(TipSerCombo.SelectedValue = 2, "C." + whe("FechaSusp").valor + " BETWEEN '" + Format(Desc1Picker.Value, "yyyyMMdd") + "' AND '" + Format(Desc2Picker.Value, "yyyyMMdd") + "'",
                                    "B." + whe("FechaDesc").valor + " BETWEEN '" + Format(Desc1Picker.Value, "yyyyMMdd") + "' AND '" + Format(Desc2Picker.Value, "yyyyMMdd") + "' AND " + "C." + whe("FechaSusp").valor + " BETWEEN '" + Format(Desc1Picker.Value, "yyyyMMdd") + "' AND '" + Format(Desc2Picker.Value, "yyyyMMdd") + "'")))
            End If
            If (ActivoBajaCheck.Checked) Then
                sb.Append((IIf(sb.ToString().Trim().Length > 0, " AND ", "")) +
                            IIf(TipSerCombo.SelectedValue = 1, "B." + whe("FechaCancelacio").valor + " BETWEEN '" + Format(Baja1Picker.Value, "yyyyMMdd") + "' AND '" + Format(Baja2Picker.Value, "yyyyMMdd") + "'",
                                IIf(TipSerCombo.SelectedValue = 2, "C." + whe("FechaBaja").valor + " BETWEEN '" + Format(Baja1Picker.Value, "yyyyMMdd") + "' AND '" + Format(Baja2Picker.Value, "yyyyMMdd") + "'",
                                    "B." + whe("FechaCancelacio").valor + " BETWEEN '" + Format(Baja1Picker.Value, "yyyyMMdd") + "' AND '" + Format(Baja2Picker.Value, "yyyyMMdd") + "' AND " + "C." + whe("FechaBaja").valor + " BETWEEN '" + Format(Baja1Picker.Value, "yyyyMMdd") + "' AND '" + Format(Baja2Picker.Value, "yyyyMMdd") + "'")))
            End If
        End If

        sbGroup.Append(group("Group").valor)

        Consulta = sbSelect.ToString + sbFrom.ToString + sb.ToString() + sbGroup.ToString
        Return sbSelect.ToString + sbFrom.ToString + sb.ToString() + sbGroup.ToString
    End Function

    Private Sub SearchButton_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SearchButton.Click
        If Me.Activo1Check.Checked = False And Me.Activo2Check.Checked = False And Me.Activo3Check.Checked = False Then
            MsgBox("Debe Ingresar Datos Al Menos En Una Categoría Para Realizar La Búsqueda De Los Clientes", MsgBoxStyle.Exclamation)
            Exit Sub
        End If

        If Me.Activo1Check.Checked Then
            If Me.ContratoText.Text.Length = 0 And Me.NombreText.Text.Length = 0 And Me.CalleCombo.SelectedValue = 0 And Me.ColoniaCombo.SelectedValue = 0 Then
                MsgBox("Debe Utilizar Al Menos Un Tipo De Datos Del Cliente", MsgBoxStyle.Exclamation)
                Exit Sub
            End If
        End If

        If Me.Activo2Check.Checked Then
            If Me.TipSerCombo.SelectedValue = 0 And Me.TipoCobroCombo.SelectedValue = 0 And Me.ZonaCombo.SelectedValue = 0 Then
                MsgBox("Debe Utilizar Al Menos Un Tipo De Servicio, Tipo Cobro o Zona", MsgBoxStyle.Exclamation)
                Exit Sub
            End If
        End If

        If Me.Activo3Check.Checked = True And Me.ActivoContCheck.Checked = False And Me.ActivoInstCheck.Checked = False And Me.ActivoDescCheck.Checked = False And Me.ActivoBajaCheck.Checked = False Then
            MsgBox("Debe Utilizar Al Menos Un Tipo De Fecha", MsgBoxStyle.Exclamation)
            Exit Sub
        End If

        'SE EJECUTA LA CONSULTA GENERADA EN LA FUNCIÓN GENERARCONSULTA()
        Dim CON As New SqlConnection(MiConexion)
        Dim dataTable As New DataTable
        Dim dataAdapter As New SqlDataAdapter(GeneraConsulta(), CON)
        Dim bindingSource As New BindingSource

        Try
            CON.Open()
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            Zonas1Grid.DataSource = bindingSource.DataSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            CON.Close()
            CON.Dispose()
        End Try

        'GUARDO TEMPORALMENTE LA CONSULTA GENERADA EN UNA TABLA.
        GuardaZonasTemp()
        MuestraZona(0)
    End Sub

    Private Sub ActivoContCheck_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ActivoContCheck.CheckedChanged
        If Me.ActivoContCheck.Checked Then
            Me.Cont1Picker.Enabled = True
            Me.Cont2Picker.Enabled = True
        Else
            Me.Cont1Picker.Enabled = False
            Me.Cont2Picker.Enabled = False
        End If
    End Sub

    Private Sub ActivoInstCheck_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ActivoInstCheck.CheckedChanged
        If Me.ActivoInstCheck.Checked Then
            Me.Inst1Picker.Enabled = True
            Me.Inst2Picker.Enabled = True
        Else
            Me.Inst1Picker.Enabled = False
            Me.Inst2Picker.Enabled = False
        End If
    End Sub

    Private Sub ActivoDescCheck_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ActivoDescCheck.CheckedChanged
        If Me.ActivoDescCheck.Checked Then
            Me.Desc1Picker.Enabled = True
            Me.Desc2Picker.Enabled = True
        Else
            Me.Desc1Picker.Enabled = False
            Me.Desc2Picker.Enabled = False
        End If
    End Sub

    Private Sub ActivoBajaCheck_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ActivoBajaCheck.CheckedChanged
        If Me.ActivoBajaCheck.Checked Then
            Me.Baja1Picker.Enabled = True
            Me.Baja2Picker.Enabled = True
        Else
            Me.Baja1Picker.Enabled = False
            Me.Baja2Picker.Enabled = False
        End If
    End Sub
    Private Sub GuardaZonasTemp()
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("GuardaZonasTemp", CON)
        CMD.CommandType = CommandType.StoredProcedure

        Dim PRM As New SqlParameter("@CONSULTA", SqlDbType.VarChar, 8000)
        PRM.Direction = ParameterDirection.Input
        PRM.Value = Consulta
        CMD.Parameters.Add(PRM)

        Try
            CON.Open()
            CMD.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub CalleCombo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CalleCombo.SelectedIndexChanged
        MuestraColoniasZonas(CalleCombo.SelectedValue)
    End Sub

    Private Sub AgregaZonas(ByVal ZContrato As Integer, ByVal Zona As Integer, ByVal Op As Integer)
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("AgregaZonas", CON)
        CMD.CommandType = CommandType.StoredProcedure

        Dim PRM1 As New SqlParameter("@Contrato", SqlDbType.Int)
        PRM1.Direction = ParameterDirection.Input
        PRM1.Value = ZContrato
        CMD.Parameters.Add(PRM1)

        Dim PRM2 As New SqlParameter("@Zona", SqlDbType.Int)
        PRM2.Direction = ParameterDirection.Input
        PRM2.Value = Zona
        CMD.Parameters.Add(PRM2)

        Dim PRM3 As New SqlParameter("@Op", SqlDbType.Int)
        PRM3.Direction = ParameterDirection.Input
        PRM3.Value = Op
        CMD.Parameters.Add(PRM3)

        Try
            CON.Open()
            CMD.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub
    Private Sub EliminaZonas(ByVal ZContrato As Integer, ByVal Op As Integer)
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("EliminaZonas", CON)
        CMD.CommandType = CommandType.StoredProcedure

        Dim PRM1 As New SqlParameter("@Contrato", SqlDbType.Int)
        PRM1.Direction = ParameterDirection.Input
        PRM1.Value = ZContrato
        CMD.Parameters.Add(PRM1)

        Dim PRM2 As New SqlParameter("@Op", SqlDbType.Int)
        PRM2.Direction = ParameterDirection.Input
        PRM2.Value = Op
        CMD.Parameters.Add(PRM2)

        Try
            CON.Open()
            CMD.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub
    Private Sub ConsultaZonasSinAsignar()

        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder

        strSQL.Append("EXEC ConsultaZonasSinAsignar ")

        Dim dataTable As New DataTable
        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim bindingSource As New BindingSource

        Try
            conexion.Open()
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            Zonas1Grid.DataSource = bindingSource.DataSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub
    Private Sub ConsultaZonasAsignadas()

        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder

        strSQL.Append("EXEC ConsultaZonasAsignadas ")

        Dim dataTable As New DataTable
        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim bindingSource As New BindingSource

        Try
            conexion.Open()
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            Zonas2Grid.DataSource = bindingSource.DataSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub CambiaZonaCLientes()
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("CambiaZonaCLientes", CON)
        CMD.CommandType = CommandType.StoredProcedure

        Try
            CON.Open()
            CMD.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub AddOneButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AddOneButton.Click
        If Zonas1Grid.RowCount = 0 Then
            MsgBox("Debe Tener Seleccionada Al Menos Una Fila")
            Exit Sub
        End If

        AgregaZonas(Zonas1Grid.SelectedCells.Item(0).Value(), CambiaZonaCombo.SelectedValue, 1)
        ConsultaZonasSinAsignar()
        ConsultaZonasAsignadas()
    End Sub

    Private Sub AddAllButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AddAllButton.Click
        If Zonas1Grid.RowCount = 0 Then
            MsgBox("Debe Tener Seleccionada Al Menos Una Fila")
            Exit Sub
        End If
        AgregaZonas(0, CambiaZonaCombo.SelectedValue, 2)
        ConsultaZonasSinAsignar()
        ConsultaZonasAsignadas()
    End Sub

    Private Sub DelOneButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DelOneButton.Click
        If Zonas2Grid.RowCount = 0 Then
            MsgBox("Debe Tener Seleccionada Al Menos Una Fila")
            Exit Sub
        End If
        EliminaZonas(Zonas2Grid.SelectedCells.Item(0).Value(), 1)
        ConsultaZonasSinAsignar()
        ConsultaZonasAsignadas()
    End Sub

    Private Sub DelAllButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DelAllButton.Click
        If Zonas2Grid.RowCount = 0 Then
            MsgBox("Debe Tener Seleccionada Al Menos Una Fila")
            Exit Sub
        End If
        EliminaZonas(0, 2)
        ConsultaZonasSinAsignar()
        ConsultaZonasAsignadas()
    End Sub

    Private Sub AfectaButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AfectaButton.Click
        If Zonas2Grid.RowCount = 0 Then
            MsgBox("No Hay Clientes Para Afectar", MsgBoxStyle.Exclamation)
            Exit Sub
        End If
        CambiaZonaCLientes()
        ConsultaZonasSinAsignar()
        ConsultaZonasAsignadas()
    End Sub
End Class

Public Class Consulta
    Public valor As String
    Public Sub New(ByVal valor As String)
        Me.valor = valor
    End Sub
End Class

