Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient
Imports System.Text
Public Class Bwr_FacturasCancelar

    Private Sub BrwFacturas_Cancelar_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim CON As New SqlConnection(MiConexion)
        Dim op As Integer
        colorea(Me, Me.Name)
        CON.Open()
        'TODO: esta l�nea de c�digo carga datos en la tabla 'NewsoftvDataSet1.MUESTRATIPOFACTURA' Puede moverla o quitarla seg�n sea necesario.
        Me.DAMEFECHADELSERVIDORTableAdapter.Connection = CON
        Me.DAMEFECHADELSERVIDORTableAdapter.Fill(Me.NewsoftvDataSet1.DAMEFECHADELSERVIDOR, Me.FECHADateTimePicker.Text)
        CON.Close()
        'Me.MUESTRATIPOFACTURATableAdapter.Connection = CON
        'Me.MUESTRATIPOFACTURATableAdapter.Fill(Me.NewsoftvDataSet1.MUESTRATIPOFACTURA)
        If IdSistema = "LO" Or IdSistema = "YU" Then
            CMBLabel5.Text = "Tipo de Pago"
            CMBLabel1.Text = "Buscar Pago por :"
            Button2.Text = "&Cancelar Pago"
            Button6.Text = "&Reimprimir Pago"
            Button8.Text = "&Ver Pago"
            Label8.Text = "Datos del Pago"
        ElseIf IdSistema = "AG" Or IdSistema = "VA" Then
            op = 2
        Else
            op = 1
        End If
        GloTipo = Me.ComboBox4.SelectedValue
        If GloOpFacturas = 0 Then
            If IdSistema = "LO" Or IdSistema = "YU" Then
                Me.Text = "Cancelaci�n de Pagos"
            Else
                Me.Text = "Cancelaci�n de Facturas"
            End If
            Me.CMBPanel2.Visible = True
            Me.CMBPanel3.Visible = False
            Me.CMBPanel4.Visible = False
            Me.Panel5.Visible = True
            Me.Button8.TabStop = False
            Me.Button6.TabStop = False
            CON.Open()
            Me.MUESTRATIPOFACTURATableAdapter.Connection = CON
            Me.MUESTRATIPOFACTURATableAdapter.Fill(Me.DataSetLidia2.MUESTRATIPOFACTURA, 1)
            BuscaFacturas(0, "", 0, "19000101", 0, "", GloTipo)
            'Me.BUSCAFACTURASTableAdapter.Connection = CON
            'Me.BUSCAFACTURASTableAdapter.Fill(Me.NewsoftvDataSet1.BUSCAFACTURAS, 0, "", 0, "01/01/1900", 0, "", GloTipo)
            CON.Close()
        ElseIf GloOpFacturas = 1 Then
            If IdSistema = "LO" Or IdSistema = "YU" Then
                Me.Text = "Reimpresi�n de Pagos"
            Else
                Me.Text = "Reimpresi�n de Facturas"
            End If
            Me.CMBPanel2.Visible = False
            Me.CMBPanel3.Visible = True
            Me.CMBPanel4.Visible = False
            Me.Panel5.Visible = True
            CON.Open()
            Me.MUESTRATIPOFACTURATableAdapter.Connection = CON
            Me.MUESTRATIPOFACTURATableAdapter.Fill(Me.DataSetLidia2.MUESTRATIPOFACTURA, 1)
            BuscaFacturas(0, "", 0, "19000101", 0, "", GloTipo)
            'Me.BUSCAFACTURASTableAdapter.Connection = CON
            'Me.BUSCAFACTURASTableAdapter.Fill(Me.NewsoftvDataSet1.BUSCAFACTURAS, 0, "", 0, "01/01/1900", 0, "", GloTipo)
            CON.Close()
            Me.Button8.TabStop = False
            Me.Button2.TabStop = False
        ElseIf GloOpFacturas = 3 Then
            Me.Text = "Ver Historial de Pagos"

            Me.CMBPanel2.Visible = False
            Me.CMBPanel3.Visible = False
            Me.CMBPanel4.Visible = True
            Me.Panel5.Visible = False
            CON.Open()
            Me.MUESTRATIPOFACTURATableAdapter.Connection = CON
            Me.MUESTRATIPOFACTURATableAdapter.Fill(Me.DataSetLidia2.MUESTRATIPOFACTURA, op)
            BuscaFacturas(13, "", 0, "19000101", Contrato, "", GloTipo)
            'Me.BUSCAFACTURASTableAdapter.Connection = CON
            'Me.BUSCAFACTURASTableAdapter.Fill(Me.NewsoftvDataSet1.BUSCAFACTURAS, 13, "", 0, "01/01/1900", Contrato, "", GloTipo)
            CON.Close()
            Me.ComboBox4.Text = ""
            Me.Button2.TabStop = False
            Me.Button6.TabStop = False
        End If
        Detalle_Faturas_OLDB()

    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub
    Private Sub buscaNotas(ByVal opcion As Integer)
        Dim conlidia As New SqlClient.SqlConnection(MiConexion)
        Try
            conlidia.Open()
            Me.BUSCANOTASDECREDITOTableAdapter.Connection = conlidia
            If opcion = 5 Then
                Me.BUSCANOTASDECREDITOTableAdapter.Fill(Me.DataSetLidia2.BUSCANOTASDECREDITO, opcion, 0, Me.FECHATextBox.Text, Contrato, 0, "")
            ElseIf opcion = 6 Then
                Me.BUSCANOTASDECREDITOTableAdapter.Fill(Me.DataSetLidia2.BUSCANOTASDECREDITO, opcion, Me.FOLIOTextBox.Text, "01/01/1900", Contrato, 0, "")
            ElseIf opcion = 2 Then
                Me.BUSCANOTASDECREDITOTableAdapter.Fill(Me.DataSetLidia2.BUSCANOTASDECREDITO, opcion, 0, Me.FECHATextBox.Text, 0, 0, "")
            ElseIf opcion = 3 Then
                Me.BUSCANOTASDECREDITOTableAdapter.Fill(Me.DataSetLidia2.BUSCANOTASDECREDITO, opcion, 0, "01/01/1900", Me.CONTRATOTextBox.Text, 0, "")
            ElseIf opcion = 4 Then
                Me.BUSCANOTASDECREDITOTableAdapter.Fill(Me.DataSetLidia2.BUSCANOTASDECREDITO, 3, 0, "01/01/1900", Contrato, 0, "")
            End If

            conlidia.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub


    Private Sub Busca(ByVal OP As Integer)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            If GloOpFacturas = 3 Then
                If OP = 0 Then
                    BuscaFacturas(10, "", 0, "19000101", Contrato, "", Me.ComboBox4.SelectedValue)
                    'Me.BUSCAFACTURASTableAdapter.Connection = CON
                    'Me.BUSCAFACTURASTableAdapter.Fill(Me.NewsoftvDataSet1.BUSCAFACTURAS, 10, "", 0, "01/01/1900", Contrato, "", Me.ComboBox4.SelectedValue)
                ElseIf OP = 1 Then
                    If IsNumeric(Me.FOLIOTextBox.Text) = False Then Me.FOLIOTextBox.Text = 0
                    BuscaFacturas(11, Me.SERIETextBox.Text, Me.FOLIOTextBox.Text, "19000101", Contrato, "", Me.ComboBox4.SelectedValue)
                    'Me.BUSCAFACTURASTableAdapter.Connection = CON
                    'Me.BUSCAFACTURASTableAdapter.Fill(Me.NewsoftvDataSet1.BUSCAFACTURAS, 11, Me.SERIETextBox.Text, Me.FOLIOTextBox.Text, "01/01/1900", Contrato, "", Me.ComboBox4.SelectedValue)
                ElseIf OP = 2 Then
                    If IsDate(Me.FECHATextBox.Text) = False Then Me.FECHATextBox.Text = "19000101"
                    BuscaFacturas(12, "", 0, Me.FECHATextBox.Text, Contrato, "", Me.ComboBox4.SelectedValue)
                    'Me.BUSCAFACTURASTableAdapter.Connection = CON
                    'Me.BUSCAFACTURASTableAdapter.Fill(Me.NewsoftvDataSet1.BUSCAFACTURAS, 12, "", 0, Me.FECHATextBox.Text, Contrato, "", Me.ComboBox4.SelectedValue)
                End If
            Else
                If OP = 0 Then
                    BuscaFacturas(0, "", 0, "19000101", 0, "", Me.ComboBox4.SelectedValue)
                    'Me.BUSCAFACTURASTableAdapter.Connection = CON
                    'Me.BUSCAFACTURASTableAdapter.Fill(Me.NewsoftvDataSet1.BUSCAFACTURAS, 0, "", 0, "01/01/1900", 0, "", Me.ComboBox4.SelectedValue)
                ElseIf OP = 1 Then
                    If IsNumeric(Me.FOLIOTextBox.Text) = False Then Me.FOLIOTextBox.Text = 0
                    BuscaFacturas(1, Me.SERIETextBox.Text, Me.FOLIOTextBox.Text, "19000101", 0, "", Me.ComboBox4.SelectedValue)
                    'Me.BUSCAFACTURASTableAdapter.Connection = CON
                    'Me.BUSCAFACTURASTableAdapter.Fill(Me.NewsoftvDataSet1.BUSCAFACTURAS, 1, Me.SERIETextBox.Text, Me.FOLIOTextBox.Text, "01/01/1900", 0, "", Me.ComboBox4.SelectedValue)
                ElseIf OP = 2 Then
                    If IsDate(Me.FECHATextBox.Text) = False Then Me.FECHATextBox.Text = "19000101"
                    BuscaFacturas(2, "", 0, Me.FECHATextBox.Text, 0, "", Me.ComboBox4.SelectedValue)
                    'Me.BUSCAFACTURASTableAdapter.Connection = CON
                    'Me.BUSCAFACTURASTableAdapter.Fill(Me.NewsoftvDataSet1.BUSCAFACTURAS, 2, "", 0, Me.FECHATextBox.Text, 0, "", Me.ComboBox4.SelectedValue)
                ElseIf OP = 3 Then
                    If IsNumeric(Me.CONTRATOTextBox.Text) = False Then Me.CONTRATOTextBox.Text = 0
                    BuscaFacturas(3, "", 0, "19000101", Me.CONTRATOTextBox.Text, "", Me.ComboBox4.SelectedValue)
                    'Me.BUSCAFACTURASTableAdapter.Connection = CON
                    'Me.BUSCAFACTURASTableAdapter.Fill(Me.NewsoftvDataSet1.BUSCAFACTURAS, 3, "", 0, "01/01/1900", Me.CONTRATOTextBox.Text, "", Me.ComboBox4.SelectedValue)
                ElseIf OP = 4 Then
                    If Len(Trim(Me.NOMBRETextBox.Text)) > 0 Then
                        BuscaFacturas(4, "", 0, "19000101", 0, Me.NOMBRETextBox.Text, Me.ComboBox4.SelectedValue)
                        'Me.BUSCAFACTURASTableAdapter.Connection = CON
                        'Me.BUSCAFACTURASTableAdapter.Fill(Me.NewsoftvDataSet1.BUSCAFACTURAS, 4, "", 0, "01/01/1900", 0, Me.NOMBRETextBox.Text, Me.ComboBox4.SelectedValue)
                    Else
                        MsgBox("La B�squeda no se puede realizar sin Datos", MsgBoxStyle.Information)
                    End If
                End If
            End If
            Me.SERIETextBox.Clear()
            Me.FOLIOTextBox.Clear()
            Me.FECHATextBox.Clear()
            Me.CONTRATOTextBox.Clear()
            Me.NOMBRETextBox.Clear()

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()
    End Sub

    Private Sub ComboBox4_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox4.SelectedIndexChanged
        If Me.ComboBox4.SelectedValue = "N" Then
            Me.BUSCANOTASDECREDITODataGridView.Visible = True
            Me.CMBLabel1.Text = "Buscar Nota por:"
            Me.Label3.Visible = False
            Me.Panel2.Visible = True
            Me.SERIETextBox.Visible = False
            Me.Label4.Text = "Nota :"
            Me.Button8.Text = "&Ver Nota"
            buscaNotas(4)
        Else
            If IdSistema = "LO" Or IdSistema = "YU" Then
                Me.Button8.Text = "&Ver Pago"
                Me.CMBLabel1.Text = "Buscar Pago por:"
            Else
                Me.Button8.Text = "&Ver Factura"
                Me.CMBLabel1.Text = "Buscar Factura por:"
            End If

            Me.Label3.Visible = True
            Me.SERIETextBox.Visible = True
            Me.Panel2.Visible = False
            Me.Label4.Text = "Serie :"

            Me.BUSCANOTASDECREDITODataGridView.Visible = False
            Busca(0)
        End If
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        If Me.ComboBox4.SelectedValue = "N" Then
            buscaNotas(6)
        Else
            Busca(1)
        End If
    End Sub

    Private Sub FECHATextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles FECHATextBox.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If Me.ComboBox4.SelectedValue = "N" Then
                buscaNotas(5)
            Else
                Busca(2)
            End If

        End If
    End Sub


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If Me.ComboBox4.SelectedValue = "N" Then
            buscaNotas(5)
        Else
            Busca(2)
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Busca(3)
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Busca(4)
    End Sub

    Private Sub SERIETextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles SERIETextBox.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(1)
        End If
    End Sub


    Private Sub FOLIOTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles FOLIOTextBox.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If Me.ComboBox4.SelectedValue = "N" Then
                buscaNotas(6)
            Else
                Busca(1)
            End If
        End If
    End Sub


    Private Sub CONTRATOTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles CONTRATOTextBox.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(3)
        End If
    End Sub


    Private Sub NOMBRETextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles NOMBRETextBox.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(4)
        End If
    End Sub

    'Private Sub SplitContainer1_Panel1_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles SplitContainer1.Panel1.Paint

    'End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim resp As MsgBoxResult

        If IsNumeric(Me.Clv_FacturaLabel1.Text) = True Then
            If DateValue(Me.FECHADateTimePicker.Text) = DateValue(Me.FECHALabel1.Text) Then
                resp = MsgBox("Deseas Cancelar la " & Me.ComboBox4.Text & " : " & Me.SerieLabel1.Text & "-" & Me.FacturaLabel1.Text, MsgBoxStyle.OkCancel, "Cancelaci�n de Facturas")
                If resp = MsgBoxResult.Ok Then
                    CANCELAFACTURA()
                End If
            Else

                MsgBox("S�lo se puede cancelar las facturas del d�a hoy", MsgBoxStyle.Information)
            End If
        Else
            If IdSistema = "LO" Or IdSistema = "YU" Then
                MsgBox("Seleccione el Pago ", MsgBoxStyle.Information)
            Else
                MsgBox("Seleccione la Factura ", MsgBoxStyle.Information)
            End If

        End If
    End Sub

    Private Sub FacturaLabel1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub FacturaLabel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub CANCELAFACTURA()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            Dim MSG As String = Nothing
            Dim BNDERROR As Integer = 0
            Me.CANCELACIONFACTURASTableAdapter.Connection = CON
            Me.CANCELACIONFACTURASTableAdapter.Fill(Me.NewsoftvDataSet1.CANCELACIONFACTURAS, Me.Clv_FacturaLabel1.Text, 0, MSG, BNDERROR)
            MsgBox(MSG)
            If BNDERROR = 0 Then
                Busca(0)
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        If IsNumeric(Me.Clv_FacturaLabel1.Text) = True Then
            GloClv_Factura = Me.Clv_FacturaLabel1.Text
            LocGloOpRep = 0
            FrmImprimirFac.Show()
        Else
            If IdSistema = "LO" Or IdSistema = "YU" Then
                MsgBox("Seleccione el Pago ", MsgBoxStyle.Information)
            Else
                MsgBox("Seleccione la Factura ", MsgBoxStyle.Information)
            End If

        End If
    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        

        If Me.ComboBox4.SelectedValue = "N" Then
            If IsNumeric(Me.Label20.Text) = True Then
                gloClvNota = Me.Label20.Text
                LocGloOpRep = 3
                FrmImprimirFac.Show()
            Else
                MsgBox("Seleccione la Nota ", MsgBoxStyle.Information)

            End If
        Else
            If IdSistema = "LO" Or IdSistema = "YU" Then LocGloOpRep = 6

            If DataGridView1.RowCount > 0 Then
                'GloClv_Factura = Me.Clv_FacturaLabel1.Text
                GloClv_Factura = DataGridView1.SelectedCells.Item(0).Value()
                FrmImprimirFac.Show()
            Else
                If IdSistema = "LO" Or IdSistema = "YU" Then
                    MsgBox("Seleccione el Pago ", MsgBoxStyle.Information)
                Else
                    MsgBox("Seleccione la Factura ", MsgBoxStyle.Information)
                End If
            End If
        End If
    End Sub

    Private Sub Label5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub BUSCANOTASDECREDITODataGridView_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles BUSCANOTASDECREDITODataGridView.SelectionChanged
        Dim cone As New SqlConnection(MiConexion)
        If Me.Label20.Text <> "" Then
            cone.Open()
            Me.DetalleNOTASDECREDITOTableAdapter.Connection = cone
            Me.DetalleNOTASDECREDITOTableAdapter.Fill(Me.DataSetLidia2.DetalleNOTASDECREDITO, Me.Label20.Text)
            cone.Close()
        End If
    End Sub

    Private Sub BuscaFacturas(ByVal OpBF As Integer, ByVal SerieBF As String, ByVal FacturaBF As Long, ByVal FechaBF As String, ByVal ContratoBF As Long, ByVal NombreBF As String, ByVal TipoBF As String)

        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder

        strSQL.Append("EXEC BUSCAFACTURAS_OLDB ")
        strSQL.Append(CStr(OpBF) & ",'")
        strSQL.Append(SerieBF & "',")
        strSQL.Append(CStr(FacturaBF) & ",")
        strSQL.Append(CStr(FechaBF) & ",")
        strSQL.Append(CStr(ContratoBF) & ",'")
        strSQL.Append(NombreBF & "','")
        strSQL.Append(TipoBF & "'")

        Dim dataTable As New DataTable
        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim bindingSource As New BindingSource

        Try
            conexion.Open()
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            DataGridView1.DataSource = bindingSource.DataSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub Detalle_Faturas_OLDB()
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("DETALLE_FACTURAS_OLDB", CON)
        CMD.CommandType = CommandType.StoredProcedure

        Dim PRM1 As New SqlParameter("@CLV_FACTURA", SqlDbType.BigInt)
        PRM1.Direction = ParameterDirection.Input
        If DataGridView1.RowCount = 0 Then
            PRM1.Value = 0
        Else
            PRM1.Value = CLng(DataGridView1.SelectedCells.Item(0).Value())
        End If
        CMD.Parameters.Add(PRM1)

        Dim PRM2 As New SqlParameter("@SERIE", SqlDbType.VarChar, 5)
        PRM2.Direction = ParameterDirection.Output
        CMD.Parameters.Add(PRM2)

        Dim PRM3 As New SqlParameter("@FOLIO", SqlDbType.Int)
        PRM3.Direction = ParameterDirection.Output
        CMD.Parameters.Add(PRM3)

        Dim PRM4 As New SqlParameter("@FECHA", SqlDbType.DateTime)
        PRM4.Direction = ParameterDirection.Output
        CMD.Parameters.Add(PRM4)

        Dim PRM5 As New SqlParameter("@CONTRATO", SqlDbType.BigInt)
        PRM5.Direction = ParameterDirection.Output
        CMD.Parameters.Add(PRM5)

        Dim PRM6 As New SqlParameter("@NOMBRE", SqlDbType.VarChar, 250)
        PRM6.Direction = ParameterDirection.Output
        CMD.Parameters.Add(PRM6)

        Dim PRM7 As New SqlParameter("@IMPORTE", SqlDbType.Decimal, 10, 2)
        PRM7.Direction = ParameterDirection.Output
        CMD.Parameters.Add(PRM7)

        Dim PRM8 As New SqlParameter("@STATUS", SqlDbType.VarChar, 20)
        PRM8.Direction = ParameterDirection.Output
        CMD.Parameters.Add(PRM8)

        Try
            CON.Open()
            CMD.ExecuteNonQuery()
            Me.SerieLabel1.Text = PRM2.Value
            Me.FacturaLabel1.Text = PRM3.Value
            Me.FECHALabel1.Text = PRM4.Value
            Me.ClienteLabel1.Text = PRM5.Value
            Me.CMBNOMBRETextBox1.Text = PRM6.Value
            Me.ImporteLabel1.Text = PRM7.Value
            Me.Label9.Text = PRM8.Value
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub DataGridView1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridView1.Click
        Detalle_Faturas_OLDB()
    End Sub
End Class