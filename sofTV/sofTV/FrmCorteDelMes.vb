﻿Imports System.Data.SqlClient
Public Class FrmCorteDelMes

    Private Sub FrmCorteDelMes_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.BackgroundWorker1.RunWorkerAsync()
    End Sub



    Private Sub Procesa()
        If eBndCorteDelMes = 0 Then
            InsertaBitacoraCierreDelMes("N")
            ProcesoCierreDelMes()
        ElseIf eBndCorteDelMes = 1 Then
            InsertaBitacoraCierreDelMes("P")
            ProcesoCierreDelMesPensionados()
        End If
    End Sub

    Private Sub ProcesoCierreDelMes()
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ProcesoCierreDelMes", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub ProcesoCierreDelMesPensionados()
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ProcesoCierreDelMesPensionados", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub BackgroundWorker1_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork
        Procesa()
    End Sub

    Private Sub BackgroundWorker1_ProgressChanged(ByVal sender As Object, ByVal e As System.ComponentModel.ProgressChangedEventArgs) Handles BackgroundWorker1.ProgressChanged

    End Sub

    Private Sub BackgroundWorker1_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BackgroundWorker1.RunWorkerCompleted
        MsgBox("Se ha procesado con éxito.", MsgBoxStyle.Information)
        Me.Close()

    End Sub

    Private Sub InsertaBitacoraCierreDelMes(ByVal Tipo)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("InsertaBitacoraCierreDelMes", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par1 As New SqlParameter("@Tipo", SqlDbType.VarChar, 1)
        par1.Direction = ParameterDirection.Input
        par1.Value = Tipo
        comando.Parameters.Add(par1)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

End Class