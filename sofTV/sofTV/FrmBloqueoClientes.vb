﻿Imports System.Data.SqlClient
Public Class FrmBloqueoClientes

    Public contratobloq As Integer
    Dim bloqueado As Boolean

    Private Sub FrmBloqueoClientes_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Consulta_ClientesBloq(contratobloq)
    End Sub

    Private Sub Consulta_ClientesBloq(contrato As Integer)
        Dim COnar As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand()
        Try
            cmd = New SqlCommand()
            COnar.Open()
            'Consulta_No_Int (@contrato bigint)
            With cmd
                .CommandText = "Consulta_ClientesBloq"
                .Connection = COnar
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                Dim prm As New SqlParameter("@contrato", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = contrato
                .Parameters.Add(prm)

                Dim reader As SqlDataReader = .ExecuteReader()
                While reader.Read()
                    ''Me.TxtNumeroInt.Text = reader.GetValue(0).ToString
                    tbContrato.Text = reader.GetValue(0).ToString
                    tbFecha.Text = DateTime.Parse(reader.GetValue(1).ToString).ToShortDateString()
                    tbObservaciones.Text = reader.GetValue(2).ToString
                    cbBloquear.Checked = Boolean.Parse(reader.GetValue(3).ToString)
                    bloqueado = Boolean.Parse(reader.GetValue(3).ToString)
                End While

                If tbContrato.Text.Length = 0 Then
                    tbContrato.Text = contratobloq
                    DAMEFECHADELSERVIDOR_2()
                    cbBloquear.Checked = True
                    bloqueado = True
                End If

            End With
            COnar.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub DAMEFECHADELSERVIDOR_2()
        Dim COnar As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand()
        Try
            cmd = New SqlCommand()
            COnar.Open()
            'Consulta_No_Int (@contrato bigint)
            With cmd
                .CommandText = "DAMEFECHADELSERVIDOR_2"
                .Connection = COnar
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                Dim reader As SqlDataReader = .ExecuteReader()
                While reader.Read()
                    ''tbFecha.Text = DateTime.Parse(reader[0].ToString()).ToShortDateString();
                    tbFecha.Text = DateTime.Parse(reader.GetValue(0).ToString).ToShortDateString()
                End While

                If tbContrato.Text.Length = 0 Then
                    tbContrato.Text = contratobloq
                End If

            End With
            COnar.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub tsbCancelar_Click(sender As Object, e As EventArgs) Handles tsbCancelar.Click
        Cancelar()
    End Sub

    Private Sub Cancelar()
        Me.Close()
    End Sub


    Private Sub tsbGuardar_Click(sender As Object, e As EventArgs) Handles tsbGuardar.Click
        Guardar()
    End Sub

    Private Sub Guardar()
        If (cbBloquear.Checked = True And tbObservaciones.Text.Length = 0) Then
            MsgBox("Captura una observación.")
            Return
        End If

        If (bloqueado = Not (cbBloquear.Checked)) Then
            DAMEFECHADELSERVIDOR_2()
        End If

        NuevoClienteBloqueado(contratobloq, DateTime.Parse(tbFecha.Text), tbObservaciones.Text, cbBloquear.Checked)
        Me.Close()

    End Sub

    Public Sub NuevoClienteBloqueado(Contrato As Integer, Fecha As DateTime, Descripcion As String, Bloq As Boolean)
        Dim COnar As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand()
        Try
            cmd = New SqlCommand()
            COnar.Open()
            'Consulta_No_Int (@contrato bigint)
            With cmd
                .CommandText = "NuevoClienteBloqueado"
                .Connection = COnar
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                Dim prm As New SqlParameter("@Contrato", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = Contrato
                .Parameters.Add(prm)

                Dim prm1 As New SqlParameter("@Fecha", SqlDbType.DateTime)
                prm1.Direction = ParameterDirection.Input
                prm1.Value = Fecha
                .Parameters.Add(prm1)

                Dim prm2 As New SqlParameter("@Descripcion", SqlDbType.VarChar)
                prm2.Direction = ParameterDirection.Input
                prm2.Value = Descripcion
                .Parameters.Add(prm2)

                Dim prm3 As New SqlParameter("@Bloq", SqlDbType.Bit)
                prm3.Direction = ParameterDirection.Input
                prm3.Value = Bloq
                .Parameters.Add(prm3)

                Dim ia As Integer = .ExecuteNonQuery()

            End With
            COnar.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

End Class