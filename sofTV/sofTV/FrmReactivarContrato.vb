Imports System.Data.SqlClient

Public Class FrmReactivarContrato
    Private resp As MsgBoxResult = MsgBoxResult.Cancel
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub FrmReactivarContrato_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        '--- vero
        If Me.TextBox1.Text.Length = 0 Then 'VALIDAMOS QUE SE HAYA UINGRESADO UN CONTRATO
            MsgBox("Ingrese un contrato a reactivar", MsgBoxStyle.Information)
            Exit Sub
        End If

        If IsNumeric(Me.TextBox1.Text) = False Then 'VALIDAMOS QUE LOS CARACTERES TECLEADOS SEAN NUM�RICOS
            MsgBox("Ingrese un contrato v�lido", MsgBoxStyle.Information)
            Exit Sub
        End If

        If uspChecaSiContratoEnBaja(CInt(Me.TextBox1.Text)) = False Then
            MsgBox("El contrato ingresado no tiene su servicio en baja", MsgBoxStyle.Information)
            Exit Sub
        End If

        reactivarContrato(CInt(Me.TextBox1.Text)) 'MANDAMOS A REACTIVAR EL CONTRATO
    End Sub
    'Private Sub Reactivar()
    '    If IsNumeric(Me.TextBox1.Text) = True And GloClv_Servicio > 0 Then
    '        Dim CON As New SqlConnection(MiConexion)
    '        CON.Open()

    '        bitsist(GloUsuario, Me.TextBox1.Text, LocGloSistema, Me.Text, "", Me.CMBLabel1.Text, GloSucursal, LocClv_Ciudad)
    '        glocontratoreactivacion = Me.TextBox1.Text

    '        CON.Close()

    '        GloClv_Servicio = 0
    '        MsgBox(eMsg)
    '        pasa = 0
    '        Me.TextBox1.Clear()
    '        Me.Close()
    '    ElseIf GloClv_Servicio = 0 And IsNumeric(Me.TextBox1.Text) = True Then
    '        glocontratoreactivacion = TextBox1.Text
    '        Dim CON As New SqlConnection(MiConexion)
    '        CON.Open()
    '        Me.DameClientesInactivosTableAdapter.Connection = CON
    '        Me.DameClientesInactivosTableAdapter.Fill(Me.DataSetEric.DameClientesInactivos, Me.TextBox1.Text, "", "", "", "", 0)
    '        If IsNumeric(Me.TextBox2.Text) = True Then

    '            resp = MsgBox("El Contrato no Tiene Asignado un Servicio, � Desea Hacerlo ? ", MsgBoxStyle.YesNo)
    '            If resp = MsgBoxResult.Yes Then
    '                tempocontrato = Me.TextBox1.Text
    '                FrmSelPaquete_Reactiva.Show()
    '            ElseIf resp = MsgBoxResult.No Then
    '                Me.TextBox1.Clear()
    '            End If
    '        Else
    '            MsgBox("El Contrato Seleccionado Ya se Encuentra Activado")
    '        End If
    '    Else
    '        MsgBox("Teclee Un Contrato Por Favor", MsgBoxStyle.Information)
    '    End If
    'End Sub



    '---- vero
    Private Sub reactivarContrato(ByVal prmContrato As Integer)
        Dim CON As New SqlConnection(MiConexion)
        Dim seleccionaPaqueteReactivar As New FrmSelPaquete_Reactiva

        glocontratoreactivacion = Me.TextBox1.Text

        resp = MsgBox("El Contrato no Tiene Asignado un Servicio, � Desea Hacerlo ? ", MsgBoxStyle.YesNo)
        If resp = MsgBoxResult.Yes Then
            tempocontrato = Me.TextBox1.Text
            seleccionaPaqueteReactivar.locReactivarContrato = CInt(Me.TextBox1.Text)
            If seleccionaPaqueteReactivar.ShowDialog = Windows.Forms.DialogResult.OK Then
                reactivaContratoNew(CInt(Me.TextBox1.Text), seleccionaPaqueteReactivar.locReactivarClvServicio, GloClv_TipSer, seleccionaPaqueteReactivar.locReactivarCabPropio, BndReactivacionRes)
            Else
                Exit Sub
            End If
        ElseIf resp = MsgBoxResult.No Then
            Me.TextBox1.Clear()
            Exit Sub
        End If

        CON.Open()
        bitsist(GloUsuario, Me.TextBox1.Text, LocGloSistema, Me.Text, "Se manda a reactivar contrato: " & CStr(Me.TextBox1.Text) & " por el usuario: " & GloUsuario, Me.CMBLabel1.Text, GloSucursal, LocClv_Ciudad)
        CON.Close()

        MsgBox("Reactivaci�n realizada exitosamente", MsgBoxStyle.Information)
        GloClv_Servicio = 0
        pasa = 0
        Me.TextBox1.Clear()
        Me.Close()
    End Sub

    Private Sub TextBox1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBox1.KeyDown
        If e.KeyValue = 13 Then
            If Me.TextBox1.Text.Length = 0 Then 'VALIDAMOS QUE SE HAYA UINGRESADO UN CONTRATO
                MsgBox("Ingrese un contrato a reactivar", MsgBoxStyle.Information)
                Exit Sub
            End If

            If IsNumeric(Me.TextBox1.Text) = False Then 'VALIDAMOS QUE LOS CARACTERES TECLEADOS SEAN NUM�RICOS
                MsgBox("Ingrese un contrato v�lido", MsgBoxStyle.Information)
                Exit Sub
            End If

            If uspChecaSiContratoEnBaja(CInt(Me.TextBox1.Text)) = False Then
                MsgBox("El contrato ingresado no tiene su servicio en baja", MsgBoxStyle.Information)
                Exit Sub
            End If

            reactivarContrato(CInt(Me.TextBox1.Text)) 'MANDAMOS A REACTIVAR EL CONTRATO
        End If
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        '-- validacion de solo numeros

        If InStr(1, "1234567890" & Chr(8), e.KeyChar) = 0 Then 'VALIDACI�N PARA QUE S�LO ADMITA VALORES NUM�RICOS (TextBox1)
            e.KeyChar = ""
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Dim brwContratos As New BrwSelContratoInactivos
        eContratoRec = 0

        If brwContratos.ShowDialog = Windows.Forms.DialogResult.OK Then
            eContratoRec = brwContratos.contratoSeleccionado
            Me.TextBox1.Text = eContratoRec
        End If
    End Sub

    'Private Sub ReactivaContrato(ByVal Contrato As Long, ByVal clv_servicio As Integer,
    '                              ByVal Tipo As Integer, ByVal seRenta As Integer, ByVal Resp As Integer)


    '    BaseII.limpiaParametros()
    '    BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, Contrato)
    '    BaseII.CreateMyParameter("@Res", System.Data.ParameterDirection.Output, SqlDbType.Int)
    '    BaseII.CreateMyParameter("@Msg", System.Data.ParameterDirection.Output, SqlDbType.VarChar, 150)
    '    BaseII.CreateMyParameter("@clv_servicio", SqlDbType.Int, clv_servicio)
    '    BaseII.CreateMyParameter("@Tipo", SqlDbType.Int, Tipo)
    '    BaseII.CreateMyParameter("@seRenta", SqlDbType.Int, seRenta)
    '    BaseII.CreateMyParameter("@Resp", SqlDbType.Int, Resp)
    '    BaseII.ProcedimientoOutPut("ReactivaContrato")

    'eRes = 0
    '    eMsg = ""

    '    eRes = BaseII.dicoPar("@Res").ToString
    '    eMsg = BaseII.dicoPar("@Msg").ToString

    'End Sub

#Region "PROCEDIMIENTOS NUEVOS REACTIVACI�N"
    Private Function uspChecaSiContratoEnBaja(ByVal prmContrato As Integer) As Boolean
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@contratoReactivar", SqlDbType.Int, prmContrato)
            BaseII.CreateMyParameter("@bndExisteContrato", ParameterDirection.Output, SqlDbType.Bit)
            BaseII.ProcedimientoOutPut("uspChecaSiContratoEnBaja")

            uspChecaSiContratoEnBaja = CBool(BaseII.dicoPar("@bndExisteContrato").ToString)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub reactivaContratoNew(ByVal Contrato As Long, ByVal clv_servicio As Integer,
                                 ByVal Tipo As Integer, ByVal seRenta As Integer, ByVal Resp As Integer)


        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, Contrato)
        BaseII.CreateMyParameter("@Res", System.Data.ParameterDirection.Output, SqlDbType.Int)
        BaseII.CreateMyParameter("@Msg", System.Data.ParameterDirection.Output, SqlDbType.VarChar, 150)
        BaseII.CreateMyParameter("@clv_servicio", SqlDbType.Int, clv_servicio)
        BaseII.CreateMyParameter("@Tipo", SqlDbType.Int, Tipo)
        BaseII.CreateMyParameter("@seRenta", SqlDbType.Int, seRenta)
        BaseII.CreateMyParameter("@Resp", SqlDbType.Int, Resp)
        BaseII.ProcedimientoOutPut("ReactivaContrato")

        eRes = 0
        eMsg = ""

        eRes = BaseII.dicoPar("@Res").ToString
        eMsg = BaseII.dicoPar("@Msg").ToString
    End Sub
#End Region
End Class