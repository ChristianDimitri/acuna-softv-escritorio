﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmCortesiasTv
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnAgregar = New System.Windows.Forms.Button()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.TvTexbox = New System.Windows.Forms.TextBox()
        Me.CorTextbox = New System.Windows.Forms.TextBox()
        Me.dgvTvCortesias = New System.Windows.Forms.DataGridView()
        CType(Me.dgvTvCortesias, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnAgregar
        '
        Me.btnAgregar.Location = New System.Drawing.Point(589, 84)
        Me.btnAgregar.Name = "btnAgregar"
        Me.btnAgregar.Size = New System.Drawing.Size(75, 23)
        Me.btnAgregar.TabIndex = 0
        Me.btnAgregar.Text = "&Agregar"
        Me.btnAgregar.UseVisualStyleBackColor = True
        '
        'btnEliminar
        '
        Me.btnEliminar.Location = New System.Drawing.Point(777, 84)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(75, 23)
        Me.btnEliminar.TabIndex = 1
        Me.btnEliminar.Text = "&Eliminar"
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'TvTexbox
        '
        Me.TvTexbox.Location = New System.Drawing.Point(304, 84)
        Me.TvTexbox.Name = "TvTexbox"
        Me.TvTexbox.Size = New System.Drawing.Size(100, 20)
        Me.TvTexbox.TabIndex = 2
        '
        'CorTextbox
        '
        Me.CorTextbox.Location = New System.Drawing.Point(447, 84)
        Me.CorTextbox.Name = "CorTextbox"
        Me.CorTextbox.Size = New System.Drawing.Size(100, 20)
        Me.CorTextbox.TabIndex = 3
        '
        'dgvTvCortesias
        '
        Me.dgvTvCortesias.BackgroundColor = System.Drawing.SystemColors.ButtonFace
        Me.dgvTvCortesias.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvTvCortesias.Location = New System.Drawing.Point(304, 190)
        Me.dgvTvCortesias.Name = "dgvTvCortesias"
        Me.dgvTvCortesias.Size = New System.Drawing.Size(240, 150)
        Me.dgvTvCortesias.TabIndex = 4
        '
        'FrmCortesiasTv
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(934, 460)
        Me.Controls.Add(Me.dgvTvCortesias)
        Me.Controls.Add(Me.CorTextbox)
        Me.Controls.Add(Me.TvTexbox)
        Me.Controls.Add(Me.btnEliminar)
        Me.Controls.Add(Me.btnAgregar)
        Me.Name = "FrmCortesiasTv"
        Me.Text = "Cortesias TV"
        CType(Me.dgvTvCortesias, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnAgregar As System.Windows.Forms.Button
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents TvTexbox As System.Windows.Forms.TextBox
    Friend WithEvents CorTextbox As System.Windows.Forms.TextBox
    Friend WithEvents dgvTvCortesias As System.Windows.Forms.DataGridView
End Class
