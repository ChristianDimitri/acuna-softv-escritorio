﻿Imports System.Data.SqlClient
Public Class FrmCargosAdministrativos
    Dim Modificando As Boolean = False
    Private Sub FrmCargosAdministrativos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Me.Label1.ForeColor = Color.Black
        Me.Label2.ForeColor = Color.Black
        Me.Label3.ForeColor = Color.Black
        Me.Label4.ForeColor = Color.Black
        Me.Label5.ForeColor = Color.Black
        Me.lblAyudaSelecciona.ForeColor = Color.Black
        Me.lblAyuda02.ForeColor = Color.Black

        Me.lblMensajeCosto.ForeColor = Color.Black
        Me.lblMensajeServicio.ForeColor = Color.Black

        CargaDatosIniciales()
    End Sub

    'CARGAMOS GENERALES DEL PROVEEDOR
    Public Sub CargaDatosIniciales()

        Dim conn As New SqlConnection(MiConexion)
        Try

            conn.Open()
            Dim comando As New SqlClient.SqlCommand("CargaRelacionServiciosCargoAdmin", conn)
            comando.CommandType = CommandType.StoredProcedure

            Dim Adaptador As New SqlDataAdapter()
            Adaptador.SelectCommand = comando

            Dim Dataset As New DataSet
            Dim Bs As New BindingSource

            Adaptador.Fill(Dataset)

            Dataset.Tables(0).TableName = "TipServ"
            Dataset.Tables(1).TableName = "RelServiciosTipoDeServicios"
            Dataset.Tables(2).TableName = "RelServiciosCargosAdmin"

            'AGREGAMOS LA RELACIÓN ENTRE LAS DOS TABLAS 
            Dataset.Relations.Add("FK_Clv_TipSer", Dataset.Tables("TipServ").Columns("Clv_TipSer"), Dataset.Tables("RelServiciosTipoDeServicios").Columns("Clv_TipSer"))
            Dataset.Relations.Add("FK_Clv_TipSer2", Dataset.Tables("TipServ").Columns("Clv_TipSer"), Dataset.Tables("RelServiciosCargosAdmin").Columns("Clv_TipSer"))

            'CARGAMOS LAS FUNTES DE DATOS
            Dim BSTipoServicio As New BindingSource
            BSTipoServicio.DataSource = Dataset.Tables("TipServ")

            Dim BSServicios As New BindingSource(BSTipoServicio, "FK_Clv_TipSer")

            Dim BSRelServiciosCargosAdministrativos As New BindingSource(BSTipoServicio, "FK_Clv_TipSer2")

            Me.dgvRelacionServiciosCargoAdmin.DataSource = BSRelServiciosCargosAdministrativos


            If Me.dgvRelacionServiciosCargoAdmin.RowCount = 0 Then

                Me.btnModificar.Enabled = False
                Me.btnEliminar.Enabled = False

            End If

            Me.dgvRelacionServiciosCargoAdmin.Columns(0).Visible = False
            Me.dgvRelacionServiciosCargoAdmin.Columns(1).Visible = False
            Me.dgvRelacionServiciosCargoAdmin.Columns(4).Visible = False

            Me.dgvRelacionServiciosCargoAdmin.Columns(2).Width = 530
            Me.dgvRelacionServiciosCargoAdmin.Columns(3).Width = 120

            Me.dgvRelacionServiciosCargoAdmin.Columns(2).HeaderText = "Servicio"
            Me.dgvRelacionServiciosCargoAdmin.Columns(3).HeaderText = "Costo del Cargo Administrativo"

            Me.cmbxTipSer.DataSource = BSTipoServicio
            Me.cmbxTipSer.DisplayMember = "Concepto"
            Me.cmbxTipSer.ValueMember = "Clv_TipSer"

            Me.cmbxServicio.DataSource = BSServicios
            Me.cmbxServicio.DisplayMember = "Descripcion"
            Me.cmbxServicio.ValueMember = "Clv_Servicio"

        
        Catch ex As Exception

            MsgBox("Ocurrion un error al momento de cargar el Catálogo de los Proveedores.", MsgBoxStyle.Exclamation)
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)

        Finally

            conn.Dispose()
            conn.Close()

        End Try

    End Sub

   

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
        Me.Dispose()
    End Sub

   
    Private Sub btnAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregar.Click

        If Me.btnAgregar.Text = "GUARDAR" Then

                If IsNumeric(Me.txtCosto.Text) = True Then

                    Dim Resp = MsgBox("Se Modificará el Costo del Cargo Administrativo del Servicio: " & Me.cmbxServicio.Text.ToString.ToUpper & " con un costo de: $" & Me.txtCosto.Text, MsgBoxStyle.YesNoCancel, "MODIFICAR EL CARGO")
                    If Resp = MsgBoxResult.Yes Then
                    CargosAdminABC(Me.cmbxServicio.SelectedValue, Me.cmbxTipSer.SelectedValue, Me.txtCosto.Text, "C")

                    'Me.btnAgregar.Enabled = True
                    Me.btnEliminar.Enabled = True

                    Me.btnAgregar.Text = "AGREGAR"
                    Me.btnModificar.Text = "MODIFICAR"

                    Me.dgvRelacionServiciosCargoAdmin.Enabled = True
                    Me.cmbxServicio.Enabled = True

                    End If
                Else
                    MsgBox("El costo especificado no tiene un formato válido. Solo números.", MsgBoxStyle.Information, "COSTO NO VALIDO")
                End If

            
        Else

            'Agregamos el Servicio Seleccionado
            If EsRegistroExistente(Me.cmbxServicio.SelectedValue) = False Then

                If IsNumeric(Me.txtCosto.Text) = True Then

                    Dim Resp = MsgBox("Se Agregará un Cargo Administrativo para el Servicio: " & Me.cmbxServicio.Text.ToString.ToUpper & " con un costo de: $" & Me.txtCosto.Text, MsgBoxStyle.YesNoCancel, "AGREGAR NUEVO CARGO")
                    If Resp = MsgBoxResult.Yes Then
                        CargosAdminABC(Me.cmbxServicio.SelectedValue, Me.cmbxTipSer.SelectedValue, Me.txtCosto.Text, "A")
                        Me.dgvRelacionServiciosCargoAdmin.Enabled = True
                        Me.cmbxServicio.Enabled = True
                    End If
                Else
                    MsgBox("El costo especificado no tiene un formato válido. Solo números.", MsgBoxStyle.Information, "COSTO NO VALIDO")
                End If

            Else
                MsgBox("Ya existe un tarifa de Cargo Administrativo para el Servicio:" & Me.cmbxServicio.Text.ToString.ToUpper & ". Lo puedes seleccionar de la lista y dar clic en el botón 'MODIFICAR'.", MsgBoxStyle.Information, "EL CARGO ADMINISTRATIVO YA ESTA REGISTRADO")
            End If

        End If

        


    End Sub

    Private Sub btnModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModificar.Click
        If Me.btnModificar.Text = "MODIFICAR" Or Me.btnModificar.Text = "&MODIFICAR" Then
            CargaServicioAModificar()
            Me.dgvRelacionServiciosCargoAdmin.Enabled = False
            Me.cmbxServicio.Enabled = False
        Else

            'Me.btnAgregar.Enabled = True
            Me.btnEliminar.Enabled = True

            Me.dgvRelacionServiciosCargoAdmin.Enabled = True
            Me.cmbxServicio.Enabled = True

            Me.btnAgregar.Text = "AGREGAR"
            Me.btnModificar.Text = "MODIFICAR"

            CargaDatosIniciales()
        End If

        'CargosAdminABC(Me.cmbxServicio.SelectedValue, Me.cmbxTipSer.SelectedValue, Me.txtCosto.Text, "C")
    End Sub

    Private Function EsRegistroExistente(ByVal Clv_Servicio As Integer) As Boolean
        Dim YaExiste As Boolean = False
        Dim cont As Integer = 0


        For cont = 0 To Me.dgvRelacionServiciosCargoAdmin.RowCount - 1
            If Clv_Servicio = Me.dgvRelacionServiciosCargoAdmin.Rows(cont).Cells(1).Value Then
                YaExiste = True
            End If
        Next

        Return YaExiste
    End Function

    Private Sub CargosAdminABC(ByVal Clv_Servicio As Integer, ByVal Clv_TipSer As Integer, ByVal Costo As Double, ByVal Opcion As String)

        Dim conn As New SqlConnection(MiConexion)

        Try

            Dim comando As New SqlClient.SqlCommand("RelServiciosCargoAdmin_ABC", conn)
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandTimeout = 10

            comando.Parameters.Add("@Clv_servicio", SqlDbType.Int)
            comando.Parameters(0).Value = Clv_Servicio

            comando.Parameters.Add("@Clv_TipSer", SqlDbType.Int)
            comando.Parameters(1).Value = Clv_TipSer

            comando.Parameters.Add("@Costo", SqlDbType.Money)
            comando.Parameters(2).Value = Costo

            comando.Parameters.Add("@Opcion", SqlDbType.VarChar, 1)
            comando.Parameters(3).Value = Opcion

            conn.Open()
            comando.ExecuteNonQuery()
            conn.Close()
            conn.Dispose()


            CargaDatosIniciales()
            If Opcion = "A" Then
                MsgBox("Se ha agregado el Cargo correctamente.", MsgBoxStyle.Information)
            ElseIf Opcion = "B" Then
                MsgBox("Se ha eliminado el Cargo correctamente.", MsgBoxStyle.Information)
            End If


        Catch ex As Exception

            MsgBox("Ocurrió un error. No se pudo agregar la relación del servicio con el Carga Administrativo.", MsgBoxStyle.Information, "ERROR AL AGREGAR")
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            conn.Close()
            conn.Dispose()

        Finally

            conn.Close()
            conn.Dispose()

        End Try


    End Sub

    Private Sub cmbxServicio_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbxServicio.SelectedValueChanged
        Try
            CargaMontoCargoAdmin(Me.cmbxServicio.SelectedValue)

        Catch ex As Exception
        End Try

        If Me.dgvRelacionServiciosCargoAdmin.RowCount = 0 Then

            Me.btnModificar.Enabled = False
            Me.btnEliminar.Enabled = False

        End If

    End Sub


    Private Sub CargaMontoCargoAdmin(ByVal Clv_Servicio As Integer)

        Dim cont As Integer


        Me.txtCosto.Text = 0
        Me.lblMensajeCosto.Visible = True
        Me.lblMensajeServicio.Visible = True

        For cont = 0 To Me.dgvRelacionServiciosCargoAdmin.RowCount - 1
            If Me.dgvRelacionServiciosCargoAdmin.Rows(cont).Cells(1).Value = Clv_Servicio Then

                Me.txtCosto.Text = Me.dgvRelacionServiciosCargoAdmin.Rows(cont).Cells(3).Value

                Me.lblMensajeCosto.Visible = False
                Me.lblMensajeServicio.Visible = False
          
                Exit For

            End If
        Next

    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        Dim Resp = MsgBox("¿Deseas Eliminar el Cargo Administrativo para el Servicio: " & Me.dgvRelacionServiciosCargoAdmin.CurrentRow.Cells(2).Value.ToString.ToUpper & "?", MsgBoxStyle.YesNoCancel, "ELIMINAR CARGO POR SERVICIO")

        If Resp = MsgBoxResult.Yes Then
            CargosAdminABC(Me.dgvRelacionServiciosCargoAdmin.CurrentRow.Cells(1).Value, Me.cmbxTipSer.SelectedValue, Me.txtCosto.Text, "B")
        End If

    End Sub

    Private Sub cmbxTipSer_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbxTipSer.SelectedValueChanged
        Try
            CargaMontoCargoAdmin(Me.cmbxServicio.SelectedValue)

        Catch ex As Exception
        End Try

        If Me.dgvRelacionServiciosCargoAdmin.RowCount = 0 Then

            Me.btnModificar.Enabled = False
            Me.btnEliminar.Enabled = False
        Else

            Me.btnModificar.Enabled = True
            Me.btnEliminar.Enabled = True

        End If


    End Sub

    Private Sub dgvRelacionServiciosCargoAdmin_CurrentCellChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvRelacionServiciosCargoAdmin.CurrentCellChanged
        Try
            CargaMontoCargoAdmin(Me.cmbxServicio.SelectedValue)

        Catch ex As Exception
        End Try
    End Sub

    'ASIGANA SERVICIO A MODIFICAR
    Private Sub CargaServicioAModificar()

        Me.cmbxServicio.SelectedValue = Me.dgvRelacionServiciosCargoAdmin.CurrentRow.Cells(1).Value
        Me.txtCosto.Text = Me.dgvRelacionServiciosCargoAdmin.CurrentRow.Cells(3).Value

        'Me.btnAgregar.Enabled = False
        Me.btnEliminar.Enabled = False

        Me.btnAgregar.Text = "GUARDAR"
        Me.btnModificar.Text = "CANCELAR"

    End Sub

End Class