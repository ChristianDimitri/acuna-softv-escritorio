Imports System.Data.SqlClient
Imports System.Text
Public Class FrmDetCobroDesc


    Private Sub FrmDetCobroDesc_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim CON As New SqlConnection(MiConexion)
        Dim Clv_Orden As Integer = 0
        Dim Clv_Tipo As Integer = 0
        colorea(Me, Me.Name)

        'CON.Open()
        'Me.Muestra_Descr_CoDescTableAdapter.Connection = CON
        'Me.Muestra_Descr_CoDescTableAdapter.Fill(Me.DataSetEric.Muestra_Descr_CoDesc, eGloContrato, Clv_Orden, Clv_Tipo)
        'Me.TextBox1.Text = Clv_Orden.ToString
        'Me.TextBox2.Text = Clv_Tipo.ToString
        'Me.Valida_tipser_ordenTableAdapter.Connection = CON
        'Me.Valida_tipser_ordenTableAdapter.Fill(Me.DataSetEric.Valida_tipser_orden, Clv_Orden, Clv_Tipo)
        'CON.Close()
        MUESTRA_DETALLE_MATERIAL()
        If IsNumeric(Me.TextBox2.Text) = True Then
            If CInt(Me.TextBox2.Text) = 1 Then
                Me.CMBLabel1.Text = "No. de Orden que se est� cobrando:"
            ElseIf CInt(Me.TextBox2.Text) = 2 Then
                Me.CMBLabel1.Text = "No. de Queja que se est� cobrando:"
            End If
        End If
    End Sub

    Private Sub Cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel.Click
        Me.Close()
    End Sub
    Private Sub MUESTRA_DETALLE_MATERIAL()

        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC MUESTRA_DETALLE_MATERIAL ")
        strSQL.Append(CStr(eGloContrato))
        

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            conexion.Open()
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            Me.Muestra_Descr_CoDescDataGridView.DataSource = bindingSource
            Me.Muestra_Descr_CoDescDataGridView.Columns(0).Width = 390
            Me.Muestra_Descr_CoDescDataGridView.Columns(1).Width = 100
            Me.Muestra_Descr_CoDescDataGridView.Columns(2).Width = 100
            'Me.CONTRATOLabel1.Text = CStr(Folio)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub
End Class