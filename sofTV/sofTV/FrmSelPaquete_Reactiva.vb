Imports System.Data.SqlClient
Public Class FrmSelPaquete_Reactiva

    Public locReactivarClvServicio, locReactivarContrato As Integer
    Public locReactivarCabPropio As Boolean

    Private Sub FrmSelPaquete_Reactiva_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.ReactivacionTableAdapter.Connection = CON
            Me.ReactivacionTableAdapter.Fill(Me.DataSetLidia.Reactivacion, tempocontrato)
            Me.ComboBox1.Text = ""
            If Me.TextBox1.Text = 2 Then
                eContratoRec = tempocontrato
                Dim resp As MsgBoxResult = MsgBoxResult.Cancel
                GloClv_Cablemodem = 0
                'GloClv_Servicio = 0
                GloClv_TipSer = 2
                resp = MsgBox("� El Cliente cuenta con Cablemodem Propio ? ", MsgBoxStyle.YesNoCancel)
                If resp = MsgBoxResult.Yes Then
                    eCabModPropio = False
                    locReactivarCabPropio = False
                    FrmCabModPropio.Show()
                ElseIf resp = MsgBoxResult.No Then
                    eCabModPropio = True
                    locReactivarCabPropio = True
                    resp = MsgBox("� El modem es Al�mbrico ? ", MsgBoxStyle.YesNoCancel)
                    If resp = MsgBoxResult.Yes Then
                        GloTipoCablemodem = 1
                        ' "Alambrico"
                    ElseIf resp = MsgBoxResult.No Then
                        GloTipoCablemodem = 2
                        '"Inalambrico"
                    End If
                    pasa = 1
                    FrmSelServicios.Show()
                    Me.Close()
                ElseIf resp = MsgBoxResult.Cancel Then
                    Me.Close()
                    pasa = 0
                End If
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If IsNumeric(Me.ComboBox1.SelectedValue) = False Then
            MsgBox("Se Requiere que se Elija un Servicio a Reactivar", MsgBoxStyle.Information)
            Exit Sub
        End If

        If Me.ComboBox1.Text = "" Then
            MsgBox("Se Requiere que se Elija un Servicio a Reactivar", MsgBoxStyle.Information)
            Exit Sub
        End If
        '---- vero
        If uspChecaSiTieneAdeudo(locReactivarContrato) > 0 Then
            locPregunta = ""
            locPregunta = UspHaz_Pregunta(Me.TextBox1.Text, 0, 0)
            If locPregunta.ToString.Length > 0 Then
                Dim selpregunta As New FrmPregunta()
                selpregunta.TextBox1.Text = locPregunta
                selpregunta.locPreguntaContrato = locReactivarContrato
                If selpregunta.ShowDialog = Windows.Forms.DialogResult.Cancel Then
                    Exit Sub
                End If
            End If
        Else
            MsgBox("�El cliente no cuenta con adeudo pendiente!" & vbNewLine & "Se proceder� a reactivar el contrato", MsgBoxStyle.Information)
        End If

        GloClv_Servicio = ComboBox1.SelectedValue
        locReactivarClvServicio = CInt(Me.ComboBox1.SelectedValue)
        '---- fin
        'ReactivaContrato(tempocontrato, GloClv_Servicio, GloClv_TipSer, eCabModPropio, BndReactivacionRes)
        eContratoRec = tempocontrato
        GloClv_Servicio = Me.ComboBox1.SelectedValue
        GloClv_TipSer = Me.TextBox1.Text
        pasa = 1
        Me.DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim resultado As MsgBoxResult = MsgBoxResult.Cancel
        resultado = MsgBox("� No se Guardar�n los Cambios, Est� Seguro de que Desea Cancelar ?", MsgBoxStyle.YesNo)
        If resultado = MsgBoxResult.Yes Then
            eContratoRec = 0
            pasa = 2
            Me.DialogResult = Windows.Forms.DialogResult.Cancel
            Me.Close()
        End If
    End Sub

    Private Sub ReactivaContrato(ByVal Contrato As Long, ByVal clv_servicio As Integer,
                                 ByVal Tipo As Integer, ByVal seRenta As Integer, ByVal Resp As Integer)


        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, Contrato)
        BaseII.CreateMyParameter("@Res", System.Data.ParameterDirection.Output, SqlDbType.Int)
        BaseII.CreateMyParameter("@Msg", System.Data.ParameterDirection.Output, SqlDbType.VarChar, 150)
        BaseII.CreateMyParameter("@clv_servicio", SqlDbType.Int, clv_servicio)
        BaseII.CreateMyParameter("@Tipo", SqlDbType.Int, Tipo)
        BaseII.CreateMyParameter("@seRenta", SqlDbType.Int, seRenta)
        BaseII.CreateMyParameter("@Resp", SqlDbType.Int, Resp)
        BaseII.ProcedimientoOutPut("ReactivaContrato")

        eRes = 0
        eMsg = ""

        eRes = BaseII.dicoPar("@Res").ToString
        eMsg = BaseII.dicoPar("@Msg").ToString

    End Sub

#Region "PROCEDIMIENTO CHECA SI TIENE ADEUDO EL CLIENTE"
    Private Function uspChecaSiTieneAdeudo(ByVal prmContrato As Integer) As Decimal
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@contratoReactivar", SqlDbType.Int, prmContrato)
            BaseII.CreateMyParameter("@montoAdeudo", ParameterDirection.Output, SqlDbType.Money)
            BaseII.ProcedimientoOutPut("uspChecaSiTieneAdeudo")

            uspChecaSiTieneAdeudo = CDec(BaseII.dicoPar("@montoAdeudo").ToString)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
#End Region


End Class